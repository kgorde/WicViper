__author__ = 'pgrimberg'
import random
import string
import pandas as pd
import datetime


def df2sortable_table(df,table_width="100%",id=None):
    table = []
    columns = df.columns
    for idx in df.index:
        df_row = df.ix[idx]
        table_row = {}
        k=1
        for c in columns:
            value = df_row[c]
            float_value = None
            try:
                float_value = round(float(value), 2)
            except Exception:
                pass
            table_row[(c,k,1)] = str(df_row[c] if float_value is None else float_value) + ""
            k=k+1
        table.append(table_row)

    if id is None: id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))
    return dict2sortable_table(table, table_width,id=id)

# input in the format: list of rows. every row is a dictionary: [{(column name, column order, width-optional) -> value}]
def dict2sortable_table(dict_list, width_pct="100%",id=None):
    if dict_list is None: return ""
    if len(dict_list) == 0: return ""
    if id is None: id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))
    keys = sorted(dict_list[0].keys(), key=lambda x: x[1])  # sort columns by their order - code convention is right elem in tuple stands for order
    # style=\"\"
    meta = "<script type=\"text/javascript\" src=\"http://www.kryogenix.org/code/browser/sorttable/sorttable.js\"></script>" \
           + "<table id=\""+id+"\" style=\"width:"+width_pct+"; background-color:#F5F5F5 \" cellspacing=\"1\" class=\"sortable\">" \
           + "<thead>" \
           + "<tr>"
    cln_first = "<th class=\"header\">" + keys[0][0] + "<span id=\"sorttable_sortrevind\">&nbsp;</span></th>"
    cln_mid = "".join(["<th class=\"header\">" + k[0] + "</th>" for k in keys[1:-1]])
    cln_last = "<th class=\"header headerSortUp\">" + keys[-1][0] + "</th></tr></thead><tbody>"

    rows = []
    for dic in dict_list:
        rows.append("<tr>")
        for k in keys:
            if len(k) >= 3:
                rows.append("<td width=" + str(k[2])+"%>" + dic[k] + "</td>")
            else:
                rows.append("<td>" + dic[k] + "</td>")
        rows.append("</tr>")

    rows.append("</tbody></table>")

    return "{0}{1}{2}{3}{4}".format(meta, cln_first, cln_mid, cln_last, "".join(rows))

def to_expandable_table(title, section2content, id):
    table_header = "<style type=\"text/css\">\r\n" \
                    "#" + id + " { border-collapse:collapse;}\r\n" \
                    "#" + id + " h4 { margin:0px; padding:0px;}\r\n" \
                    "#" + id + " img { float:right;}\r\n" \
                    "#" + id + " ul { margin:10px 0 10px 40px; padding:0px;}\r\n" \
                    "#" + id + " th { background:#7CB8E2 url(./static/header_bkg.png) repeat-x scroll center left; color:#fff; padding:7px 15px; text-align:left;}\r\n" \
                    "#" + id + " td { background:#C7DDEE none repeat-x scroll center left; color:#000; padding:7px 15px; }\r\n" \
                    "#" + id + " tr.odd td { background:#fff url(./static/row_bkg.png) repeat-x scroll center left; cursor:pointer; }\r\n" \
                    "#" + id + " div.arrow { background:transparent url(./static/arrows.png) no-repeat scroll 0px -16px; width:16px; height:16px; display:block;}\r\n" \
                    "#" + id + " div.up { background-position:0px 0px;}\r\n" \
                    "</style>\r\n" \
                    "<script type=\"text/javascript\">\r\n" \
                    "$(document).ready(function(){\r\n" \
                    "$(\"#" + id + " tr:odd\").addClass(\"odd\");\r\n" \
                    "$(\"#" + id + " tr:not(.odd)\").hide();\r\n" \
                    "$(\"#" + id + " tr:first-child\").show();\r\n" \
                    "$(\"#" + id + " tr.odd\").click(function(){\r\n" \
                    "$(this).next(\"tr\").toggle();\r\n" \
                    "$(this).find(\".arrow\").toggleClass(\"up\");\r\n" \
                    "});\r\n" \
                    "//$(\"#" + id + "\").jExpand();\r\n" \
                    "});\r\n" \
                    "</script>\r\n"
    table = [
        "<table id=\"" + id + "\">" \
                              "<tr>" \
                              "<th>" + title + "</th>" \
                                               "<th></th>" \
                                               "<th></th>" \
                                               "<th></th>" \
                                               "<th></th>" \
                                               "</tr>"]
    for (head, body) in section2content:
        table.append(
            "<tr>" \
            "<td>" + head + "</td>" \
                            "<td></td>" \
                            "<td></td>" \
                            "<td></td>" \
                            "<td><div class=\"arrow\"></div></td>" \
                            "</tr>" \
                            "<tr>" \
                            "<td colspan=\"5\">" + body + "</td>" \
                                                          "</tr>")
    table.append("</table>")
    return table_header + "".join(table)

def df2searchable_table(df,id=None,height="540px"):
    if id is None:
        id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))
    #region javascript
    script="<script>\r\n"\
    "\t\t\t$(document).ready(function () {\r\n"\
    "\t\t\t\t$('#"+id+"').dataTable({\r\n"\
    "\t\t\t\t    \"sDom\": '<lf<t>ip>', \r\n"\
    "\t\t\t\t        \"bScrollInfinite\": false, \r\n"\
    "\t\t\t\t        \"bScrollCollapse\": false, \r\n"\
    "\t\t\t\t        \"sScrollY\": \""+height+"\" \r\n"\
    "\t\t\t});\r\n"\
    "\t\t\tvar dataTableObj = $('#"+id+"').DataTable();\r\n"\
    "\t\t\t$('#customSearch').on('keyup change', function () {\r\n"\
    "\t\t\t\t    dataTableObj.column(1).search(this.value).draw();\r\n"\
    "\t\t\t});\r\n"\
    "\t\t\tvar table = $('#"+id+"').DataTable();\r\n"\
    "\t\t\t// Event listener to the two range filtering inputs to redraw on input\r\n"\
    "\t\t\t$('#min, #max').keyup( function() {\r\n"\
    "\t\t\t\t    table.draw();\r\n"\
    "\t\t\t} );\r\n"\
    "\t\t\t});\r\n"\
    "</script>\r\n"
    #endregion

    #region html
    cols = "".join([ ("<th>"+cln+"</th> \r\n") for cln in df.columns])
    rows = "".join([("<tr>\r\n"+"".join([("<td>"+str(df.ix[idx][c])+"</td> \r\n") for c in df.columns])+"</tr>\r\n") for idx in df.index])

    table =     "<div id=\"div-"+id+"\">\r\n"\
                "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"display\" id=\""+id+"\"> \r\n"\
                "       <thead> \r\n"\
                "            <tr> \r\n"+cols+"</tr> \r\n"\
                "        </thead> \r\n"\
                "        <tbody> \r\n"+rows+"</tbody> \r\n"\
                "        <tfoot></tfoot> \r\n"\
                "    </table> \r\n"\
                "</div>"
    #endregion
    return script+table

def df2jquery_datatable(df,colnames_to_sum=[], cln2decimal_pts={},paginate=False,page_length=None,id=None,
                        up_down_arrow_cls=[],fixed_header=False,scroll_y='50%',
                        cln2tooltip = {},
                        enable_search=True,
                        show_export=True,
                        date_colnames_to_sort=[],
                        sum_table_show_export=False,
                        default_tooltips_enabled=True,
                        col_supercol_pairs=[]):

    #region default tooltips
    if default_tooltips_enabled:
        cln2tooltip["Alpha Exposure"] = "Net delta adjusted exposure of all alpha securities \r\n including the options on alpha securities\r\n "
        cln2tooltip["Hedge Exposure"] = "Net delta adjusted exposure of all hedge securities\r\n "
        cln2tooltip["Net Exposure"] = "Net delta adjudsted exposure \r\n "
        cln2tooltip["Capital"] = "Gross market value of all alpha securities and options on the alpha securities \r\n "
        cln2tooltip["Gross Exposure"] = "Gross delta adjusted exposure \r\n"
        cln2tooltip["Capital Exposure"] = "Gross market value of all alpha securities \r\n including gross market value of options on alpha securities"
        cln2tooltip["Directional Equity Risk"] = "Custom beta-adjusted net delta exposure to equities \r\n Cash only ARB deals are assigned 0 beta. \r\n ARB deals with stock component receive the relative portion of the acquirer beta (stock consideration over total deal terms) \r\n for the rest of equities, beta is set as 1, i.e., directional equity risk is the net delta exp \r\n "
        cln2tooltip["Directional Credit Risk(bps)"] = "Exposure to credit spreads defined as the bps impact on NAV for 1% change in spread \r\n (Adj CR01)"
        cln2tooltip["Directional IR Risk(bps)"] = "Exposure to interest rate movement defined as bps impact to NAV for 1bp move in rates \r\n (DV01)"
    #endregion



    if id is None: id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))

    th_tooltip = lambda cln: "" if cln not in cln2tooltip else "title=\""+cln2tooltip[cln]+"\""
    def td_data_order(colname, val):
        if colname not in date_colnames_to_sort: return ""
        if pd.isnull(val): return ""
        if val.startswith("<center>") and val.endswith("</center>"):
            val = val.split("<center>")[1].split("</center>")[0]
        if val in ["None",""]: return ""
        return "data-order=\""+str(int((pd.to_datetime(val) - datetime.datetime(1970,1,1)).total_seconds()*1000))+"\""

    cols = "<tr>"+("".join([ ("<th "+th_tooltip(cln)+" >"+cln+"</th> \r\n") for cln in df.columns]))+"</tr>"

    def builder_complex_header_part1():
        res = ""
        k=0
        n=len(col_supercol_pairs)
        while k<n:
            (col,s_col) = col_supercol_pairs[k]
            if s_col is None:
                res += "<th rowspan=\"2\">"+str(col)+"</th> \r\n"
                k+=1
                continue
            #super column, start grouping.
            colspan = 1
            while True:
                if k+1 == n: # reached end.
                    res += "<th colspan=\""+str(colspan)+"\">"+str(s_col)+"</th> \r\n"
                    return res
                k+=1
                (next_col,next_s_col) = col_supercol_pairs[k]
                if next_s_col == s_col: # keep counting
                    colspan += 1
                    continue
                #group ended
                res += "<th colspan=\""+str(colspan)+"\">"+str(s_col)+"</th> \r\n"
                break
        return res
    def builder_complex_header_part2():
        res = ""
        for (col,s_col) in col_supercol_pairs:
            if s_col is None: continue
            res += "<th>"+col+"</th> \r\n"
        return res
    def builder_complex_header():
        cols ="<tr>"
        cols += builder_complex_header_part1()
        cols += "</tr> \r\n"
        cols += "<tr> \r\n"
        cols += builder_complex_header_part2()
        cols += "</tr> \r\n"
        return cols

    if len(col_supercol_pairs) > 0:  cols = builder_complex_header()

    cols_header = "<thead>"+cols+"</thead> \r\n"
    rows = "".join([("<tr>\r\n"+"".join([("<td "+td_data_order(c,df.ix[idx][c])+">"+str(df.ix[idx][c])+"</td> \r\n") for c in df.columns])+"</tr>\r\n") for idx in df.index])

    table_html =     "<div id=\"div-"+id+"\">\r\n"\
                "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"display\" id=\""+id+"\"> \r\n" +\
                cols_header +\
                "        <tbody> \r\n"+rows+"</tbody> \r\n"\
                "        <tfoot></tfoot> \r\n"\
                "    </table> \r\n"\
                "</div>"

    sumtable_html = ""
    if len(colnames_to_sum) > 0:
        sumtable_html = "<br> \r\n"\
                        "<table id=\""+id+"_sumtable\"> \r\n"\
                        "<thead> \r\n"\
                            "<tr>\r\n"+cols+"</tr> \r\n"\
                        "</thead> \r\n"\
                        "<tbody> \r\n"\
                        "</tbody> \r\n"\
                        "</table> \r\n"

    html = table_html + sumtable_html

    paginate_txt = ("true, 'pageLength':"+str(page_length)) if paginate else 'false'
    table_formatter = ""
    if len(cln2decimal_pts) > 0:
        formatters_lst = []
        for (cln,decimal) in cln2decimal_pts.iteritems():
            if cln not in df.columns: continue
            left_pos_modifier = ""; right_pos_modifier = "";left_neg_modifier = ""; right_neg_modifier = "";
            if cln in up_down_arrow_cls: left_pos_modifier = '[&uarr;'; right_pos_modifier = ']'; left_neg_modifier = '[&darr;'; right_neg_modifier = ']';
            aTarget = list(df.columns).index(cln)
            formatters_lst.append(
                "{\t\"aTargets\": ["+str(aTarget)+"], \r\n "
                "\t\"mRender\": function(data,type,row) { \r\n "\
                     "\t\t if (isNaN(data) ) {return \"\";} \r\n "\
                    "\t\t if (data == \"\") {return \"\";} \r\n "\
                    "\t\t var frmted_num = Number(parseFloat(data).toFixed("+str(decimal)+")).toLocaleString('en'); \r\n"\
                     "\t\t if (data >= 0) { \r\n"\
                                "\t\t\treturn \"<center><font color=\\\"green\\\">"+left_pos_modifier+"\" + frmted_num +\""+right_pos_modifier+"</font></center>\"; } \r\n "\
                    "\t\t return \"<center><font color=\\\"red\\\">"+left_neg_modifier+"\" + frmted_num + \""+right_neg_modifier+"</font></center>\";}\r\n}")
        table_formatter = "\"aoColumnDefs\":[\r\n  "+",".join(formatters_lst)+"]"
        if fixed_header: table_formatter += ", " +  "\"sScrollY\": \""+scroll_y+"\", \r\n"\
                                                    "\"sScrollX\": \"100%\", \r\n"\
                                                    "\"sScrollXInner\": \"100%\",\r\n"\
                                                    "\"bScrollCollapse\": true, \r\n"\
                                                    "\"fixedHeader\": true, \r\n"
    if len(colnames_to_sum) == 0:
        buttons = '' if not show_export else "buttons: ['copy', {extend: 'csv', title: 'DataExport' }, 'excel', {extend: 'pdf', title: 'DataExport' }, 'print'],"
        frmtr = "'aaSorting': [], dom: 'Bfrtip', " + buttons + " bPaginate: "+paginate_txt+" " + ("" if len(table_formatter) == 0 else ",") + table_formatter
        js = "<script>var table_"+id+" = $(\"#"+id+"\").DataTable({"+frmtr+"});</script>"
    else:
        cols_idx_to_sum = "["+",".join([str(i) for i in range(len(df.columns)) if (df.columns[i] in colnames_to_sum)])+"]"
        sum_fn = "jQuery.fn.dataTable.Api.register( 'sum()', function ( ) {\r\n return this.flatten().reduce( function ( a, b ) {\r\n \r\n if (isNaN(b)) {return a+0;} return Number(a)+Number(b);\r\n    }, 0 );\r\n} );"

        frmtr1 = "'aaSorting': [], "
        if show_export:
            frmtr1 += " dom: 'Bfrtip', buttons: ['copy', {extend: 'csv', title: 'DataExport' }, 'excel', {extend: 'pdf', title: 'DataExport' }, 'print'],"
        frmtr1 += ("bFilter: false, " if not enable_search else "")+ " bPaginate: "+paginate_txt+" " + ("" if len(table_formatter) == 0 else ",") + table_formatter

        frmtr2 = ""
        if sum_table_show_export:
            frmtr2 += " dom: 'Bfrtip', buttons: ['copy', {extend: 'csv', title: 'DataExport' }, {extend: 'pdf', title: 'DataExport' }, 'print'], "
        frmtr2 += "bFilter: false, bInfo: false, bPaginate: "+paginate_txt+" " + ("" if len(table_formatter) == 0 else ",") + table_formatter

        js = "<script>function contains(arr, obj) {\r\n    " \
             "for (var i = 0; i < arr.length; i++) {\r\n        " \
             "  if (arr[i] === obj) {\r\n            " \
             "      return true;\r\n        }\r\n    }\r\n    " \
             "return false;\r\n" \
             "}\r\n" \
             "function reload_sumtable_"+id+"() " \
            "{\r\n" \
                "\tsum_table_"+id+".clear().draw();" \
                "\t  \r\n  num_cols = parseFloat($('#"+id+" thead th').length);    \r\n" \
              "\tvar sum_arr = [];\r\n  " \
              "var cols_to_sum_arr = "+cols_idx_to_sum+";  \r\n  " \
           "    for (var i=0; i<num_cols; i+=1) { " \
                   "\r\n  \tif (contains(cols_to_sum_arr,i)) {    " \
                                        "            \t\r\n      s = table_"+id+".column(i,{page:'current'} ).data().sum();        \r\n    " \
                                                                "\tif (isNaN(s)) { sum_arr.push('');}\r\n    " \
                                                               "\telse { sum_arr.push(s); }      " \
                                                   "\r\n    }\r\n    " \
                           "else { sum_arr.push(''); }  " \
       "\t\r\n\t}  \r\n  " \
       "$('#"+id+"_sumtable').dataTable().fnAddData(sum_arr);    \r\n}\r\n\r\n" +\
        sum_fn +\
         "var table_"+id+" = $(\"#"+id+"\").DataTable({"+frmtr1+"});\r\n" \
        "var sum_table_"+id+" = $(\"#"+id+"_sumtable\").DataTable({"+frmtr2+"});\r\n" \
        "reload_sumtable_"+id+"();\r\n\r\n" \
        "$(\"#"+id+"\").on('search.dt', function() {reload_sumtable_"+id+"();});\r\n\r\n\r\n\r\n</script>"

    return html + js

def df2jquery_datatable_with_chart(df, xaxis_cln, yaxis_cln, chart_width="740px",
                                   chart_height="600px", chart_type="column", chart_title="title",
                                   chart_subtitle="subtitle", colnames_to_sum=[], cln2decimal_pts={},
                                   paginate=False,page_length=None):
    chart_id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))

    chart_html = "<div id="+chart_id+" style=\"width: "+chart_width+"; height: "+chart_height+"; margin: 0 auto\"></div> \r\n"

    table_id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))
    cat_col_idx = str(df.columns.get_loc(xaxis_cln))
    value_col_idx = str(df.columns.get_loc(yaxis_cln))
    df['Included'] = df[xaxis_cln].apply(lambda x: "<input type=\"checkbox\" id=\""+table_id+"-checkbox-"+str(x)+"\" value=\""+x+"\" onclick= \"chartfunc_"+table_id+"()\" checked>")

    table_htmljs = df2jquery_datatable(df,colnames_to_sum,cln2decimal_pts,paginate,page_length,id=table_id)

    chart_js = "<script>\r\n"\
    "\t\t\t$(function () { \r\n" \
    "\t\t\tvar options = { \r\n" \
        "\t\t\t\tchart: { \r\n" \
        		"\t\t\t\t\trenderTo: '"+chart_id+"', \r\n" \
                "\t\t\t\t\ttype: '"+chart_type+"' \r\n" \
        "\t\t\t\t}, \r\n" \
        "\t\t\t\ttitle: { text: '"+chart_title+"' }, \r\n" \
        "\t\t\t\tsubtitle: { text: '"+chart_subtitle+"' }, \r\n" \
        "\t\t\t\txAxis: { \r\n" \
            "\t\t\t\t\tcrosshair: true, \r\n" \
          	"\t\t\t\t\tlabels: { \r\n" \
                "\t\t\t\t\t\trotation: -45, \r\n" \
                "\t\t\t\t\t\tstyle: { \r\n" \
                    "\t\t\t\t\t\t\tfontSize: '13px', \r\n" \
                    "\t\t\t\t\t\t\tfontFamily: 'Verdana, sans-serif' \r\n" \
                "\t\t\t\t\t\t} \r\n" \
            "\t\t\t\t\t} \r\n" \
        "\t\t\t\t}, \r\n" \
        "\t\t\t\tyAxis: {\r\n" \
            "\t\t\t\t\tmin: 0,\r\n" \
            "\t\t\t\t\ttitle: {\r\n" \
                "\t\t\t\t\t\ttext: 'yaxis'\r\n" \
            "\t\t\t\t\t}\r\n" \
        "\t\t\t\t},\r\n" \
        "\t\t\t\ttooltip: {\r\n" \
            "\t\t\t\t\theaderFormat: '<span style=\"font-size:10px\">{point.key}</span><table>',\r\n" \
            "\t\t\t\t\tpointFormat: '<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td>' +\r\n" \
                "\t\t\t\t\t\t'<td style=\"padding:0\"><b>{point.y:.2f} </b></td></tr>',\r\n" \
            "\t\t\t\t\tfooterFormat: '</table>',\r\n" \
            "\t\t\t\t\tshared: true,\r\n" \
            "\t\t\t\t\tuseHTML: true\r\n" \
        "\t\t\t\t},\r\n" \
        "\t\t\t\tplotOptions: {\r\n" \
            "\t\t\t\t\tcolumn: {\r\n" \
                "\t\t\t\t\t\tpointPadding: 0,\r\n" \
                "\t\t\t\t\t\tborderWidth: 5,\r\n" \
                "\t\t\t\t\t\tgroupPadding: 0,\r\n" \
                "\t\t\t\t\t\tshadow: false\r\n" \
            "\t\t\t\t\t}\r\n" \
        "\t\t\t\t},\r\n" \
        "\t\t\t\tseries: [{\r\n" \
          	"\t\t\t\t\tdataLabels: {\r\n" \
                "\t\t\t\t\t\tenabled: true,\r\n" \
                "\t\t\t\t\t\trotation: -90,\r\n" \
                "\t\t\t\t\t\tcolor: '#FFFFFF',\r\n" \
                "\t\t\t\t\t\talign: 'right',\r\n" \
                "\t\t\t\t\t\tformat: '{point.y:.2f}', // one decimal\r\n" \
                "\t\t\t\t\t\ty: 5, // 5 pixels down from the top\r\n" \
                "\t\t\t\t\t\tstyle: {\r\n" \
                    "\t\t\t\t\t\t\tfontSize: '12px',\r\n" \
                    "\t\t\t\t\t\t\tfontFamily: 'Verdana, sans-serif'\r\n" \
                "\t\t\t\t\t\t}\r\n" \
            "\t\t\t\t\t}\r\n" \
        "\t\t\t\t}]\r\n" \
    "\t\t\t\t};\r\n" \
		"\t\t\t\t\tvar chart = new Highcharts.Chart(options);\r\n" \
  	    "\t\t\t\t\tchartfunc_"+table_id+" = function()\r\n" \
		"\t\t\t\t\t{\r\n" \
    	    "\t\t\t\t\t\tvar data = [];\r\n" \
            "\t\t\t\t\t\tvar categories = [];\r\n" \
            "\t\t\t\t\t\tvar table = document.getElementById(\""+table_id+"\");\r\n" \
            "\t\t\t\t\t\tfor (var i = 1, row; row = table.rows[i]; i++) {\r\n" \
      		     "\t\t\t\t\t\t\tvar cat = row.cells["+cat_col_idx+"].innerHTML;\r\n" \
      		    "\t\t\t\t\t\t\tvar included = document.getElementById(\""+table_id+"-checkbox-\"+cat)\r\n" \
                "\t\t\t\t\t\t\tif (included.checked == true) {\r\n" \
           		    "\t\t\t\t\t\t\t\tvar val = parseFloat(row.cells["+value_col_idx+"].innerHTML);\r\n" \
                    "\t\t\t\t\t\t\t\tcategories.push(cat)\r\n" \
           		    "\t\t\t\t\t\t\t\tdata.push(val);\r\n" \
                "\t\t\t\t\t\t\t}\r\n" \
            "\t\t\t\t\t\t}\r\n" \
            "\t\t\t\t\t\tvar seriesName = '"+yaxis_cln+"';\r\n" \
            "\t\t\t\t\t\tchart.xAxis[0].setCategories(categories);\r\n" \
            "\t\t\t\t\t\tchart.yAxis[0].axisTitle.attr({text: seriesName});\r\n" \
            "\t\t\t\t\t\tchart.series[0].setData(data)\r\n" \
            "\t\t\t\t\t\tchart.series[0].update({showInLegend: false, name: seriesName}, false);\r\n" \
            "\t\t\t\t\t\tchart.redraw();\r\n" \
        "\t\t\t\t\t}\r\n" \
		"\t\t\t\t\tchartfunc_"+table_id+"();\r\n" \
    "\t\t\t});\r\n"\
    "</script>"

    return table_htmljs + chart_html + chart_js





def num2color(num, num_prefix='$',num_suffix="", decimal_pts=None):
    rounder = lambda x: x if decimal_pts is None else (round(float(x),decimal_pts) if decimal_pts > 0 else int(x))
    try:
        num = float(num)
        if num >= 0:
            return "<font color=\"green\">" + num_prefix + "{:,}".format(rounder(num)) + num_suffix + "</font>"
        return "<font color=\"red\">" + num_prefix + "{:,}".format(rounder(num)) + num_suffix + "</font>"
    except: # it's not a number
        return str(num)

def to_grid(width='32.3%',height='50%',**kwargs):
    css  = "<style type=\"text/css\">\r\n" \
    "html, body { height: 100%; padding: 0; margin: 0; } "\
    "div.grid { width: "+width+"; height: "+height+"; float: left; border: 6px solid ;}</style>\r\n"

    html  = "<body>" \
            + "".join(["<div class = \"grid\" id=\""+key+"\">"+value+"</div> " for key, value in sorted(kwargs.iteritems(), key=lambda x: x[0])])\
            + "</body> "

    return css+html

def to_grid_from_arr(width='32.3%',height='50%',content_arr=[]):
    css  = "<style type=\"text/css\">\r\n" \
    "html, body { height: 100%; padding: 0; margin: 0; } "\
    "div.grid { width: "+width+"; height: "+height+"; float: left; border: 6px solid ;}</style>\r\n"
    div_id2content = [(''.join(random.choice(string.ascii_uppercase) for _ in range(12)),c) for c in content_arr]
    html  = "<body>" \
            + "".join(["<div class = \"grid\" id=\""+key+"\">"+value+"</div> " for (key, value) in div_id2content])\
            + "</body> "

    return css+html

def button2new_window(button_label,url, width="2500",height="1030"):
    id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))
    return "<input type=\"button\" onclick=\"window.open('"+url+"', '"+id+"', 'toolbars=0,width="+width+",height="+height+",left=200,top=200,scrollbars=1,resizable=1');\" value=\""+button_label+"\">"

def edit_button2new_window(url,width="2500",height="1030"):
    id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))
    return "<img src=\"./static/edit.png\" style=\"height: 20px; width: 20px;\" id=\"editDealBtn_"+id+"\" onclick=\"window.open('"+url+"', '"+id+"', 'toolbars=0,width="+width+",height="+height+",left=200,top=200,scrollbars=1,resizable=1');\">"

def dashboard_line(html_list,height='10%'):
	h = "<div style=\"width: 100%;\"> \r\n"
	for arg in html_list:
		h += "<div style=\"width: "+arg[1]+";height: "+height+"; float: left; border:3px solid black;\"> \r\n"
		h += arg[0]
		h += "</div> \r\n"
	h += "</div>"
	return h

def tooltip(label, content):
    return "<div class=\"item\"><div class=\"subitem\">"+label+"</div></div> \r\n"\
        "<div class=\"statusRollup\">"+content+"</div>"

def redirect_dropdown(items, on_select_url_to_redirect, selected_item = None, width="100px", font_size="20px"):
    html = "\r\n".join(["<option "+("selected=\"selected\"" if item == selected_item else "")+" value=\""+on_select_url_to_redirect+str(item)+"\">"+str(item)+"</option>" for item in items])
    return "<select style=\"width:"+width+"; font-size:"+font_size+"\" onChange=\"window.document.location.href=this.options[this.selectedIndex].value;\" value=\"GO\">"+html+"</select>"

def dropdown(items, selected_item, width="100px", font_size="20px",id=None):
    if id is None: id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))
    html = "\r\n".join(["<option "+("selected=\"selected\"" if item == selected_item else "")+" value=\""+str(item)+"\">"+str(item)+"</option>" for item in items])
    return "<select id=\""+id+"\" style=\"width:"+width+"; font-size:"+font_size+"\" value=\"GO\">"+html+"</select>"


def image2dialog(img_name_in_static, dialog_text,dialog_title, width="500", height="290"):
    btn_id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))
    dialog_id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))
    html = "<div id=\""+dialog_id+"\">"+dialog_text+"</div><img style=\"height: 20px; width: 20px;\" src=\"./static/"+img_name_in_static+"\" id=\""+btn_id+"\"> \r\n"

    js = "$(\"#"+dialog_id+"\").dialog({ \r\n"\
            "autoOpen  : false, \r\n"\
            "modal     : true, \r\n"\
            "width: "+width+", \r\n"\
            "height: "+height+", \r\n"\
            "title     : \""+dialog_title+"\", \r\n"\
            "buttons   : { 'Close' : function() { $(this).dialog('close'); }} \r\n"\
    "});\r\n"\
    "$(\"#"+btn_id+"\").click(function() {\r\n"\
    "$(\"#"+dialog_id+"\").dialog(\"open\"); \r\n"\
    "});\r\n"

    return html+"<script>"+js+"</script>"

def button2dialog(button_label, dialog_text,dialog_title, width="500", height="290"):
    btn_id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))
    dialog_id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))
    html = "<div id=\""+dialog_id+"\">"+dialog_text+"</div><button id=\""+btn_id+"\">"+button_label+"</button>"

    js = "$(\"#"+dialog_id+"\").dialog({ \r\n"\
            "autoOpen  : false, \r\n"\
            "modal     : true, \r\n"\
            "width: "+width+", \r\n"\
            "height: "+height+", \r\n"\
            "title     : \""+dialog_title+"\", \r\n"\
            "buttons   : { 'Close' : function() { $(this).dialog('close'); }} \r\n"\
    "});\r\n"\
    "$(\"#"+btn_id+"\").click(function() {\r\n"\
    "$(\"#"+dialog_id+"\").dialog(\"open\"); \r\n"\
    "});\r\n"

    return html+"<script>"+js+"</script>"


def button2ajax_post(button_label, url,param2value):
    id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))
    div_id = 'div_'+id
    btn_id = 'btn_'+id
    html = "<input type=\"button\" id=\""+btn_id+"\" value=\""+button_label+"\" /> \r\n"\
            "<div id=\""+div_id+"\"></div>\r\n"

    ajax_js = "\r\n\r\n $.ajax({\r\n  " \
              "     url:'"+url+"?"+'&'.join([str(p)+"="+str(v) for (p,v) in param2value.iteritems()])+"',\r\n  " \
              "     complete: function (response) { \r\n    " \
              "           server_response = response.responseText;\r\n    " \
              "           $('#"+div_id+"').html(response.responseText); \r\n  " \
              "     },\r\n  " \
              "     error: function () { $('#"+div_id+"').html('Error'); }\r\n" \
              "}); \r\n "

    js = "<script>" \
        "$('#"+div_id+"').hide(); \r\n " \
        "$('#"+btn_id+"').click(function() { " \
            "$('#"+div_id+"').show(); " \
            "$('#"+div_id+"').html('loading...'); \r\n "\
            + ajax_js +\
        "}); " \
        "</script>"

    return html+js


def health_bar(pct, dialog_content=None):
    if dialog_content is None:
        return "<meter min=\"0\" low=\"25\" optimum=\"100\" high=\"75\" max=\"100\" value=\""+str(pct)+"\"></meter>"

    return "<meter min=\"0\" low=\"25\" optimum=\"100\" high=\"75\" max=\"100\" value=\""+str(int(pct))+"\" onclick='$(\"<p>"+dialog_content+"</p>\").dialog();' style=\"cursor: pointer;\" ></meter>"

def start_end_date_picker(id=None, start_label=None, end_label=None, date_format='mm/dd/yy',start_ref_param='start_date',end_ref_param='end_date'):
    if id is None:
        id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))

    start_id = "start_date" + id
    end_id = "end_date" + id
    if start_label is None: start_label = 'Start Date '
    if end_label is None: end_label = 'End Date '
    return "\t\t"+start_label+"  <input id=\""+start_id+"\" type=\"text\" /><br /> \r\n"\
            "\t\t\t "+end_label+"  <input id=\""+end_id+"\" type=\"text\" /><br /> \r\n"\
            "\t\t\t<input id=\"thesubmit\" type=\"button\" value=\"Submit\" /> \r\n"\
            "\t\t\t<script> \r\n"\
            "\t\t\t$('#"+start_id+"').datepicker({ \r\n"\
            "\t\t\t\t  dateFormat: '"+date_format+"',  \r\n"\
            "\t\t\t}); \r\n"\
            "\t\t\t$('#"+end_id+"').datepicker({ \r\n"\
              "\t\t\t\tdateFormat: '"+date_format+"', \r\n "\
            "\t\t\t}); \r\n"\
            "\t\t\t$('#thesubmit').click(function() { \r\n"\
                "\t\t\t\t var start_date = $('#"+start_id+"').datepicker().val(); \r\n"\
                "\t\t\t\t var end_date = $('#"+end_id+"').datepicker().val(); \r\n"\
                "\t\t\t\t window.location.href = \"?"+start_ref_param+"=\"+start_date+\"&"+end_ref_param+"=\"+end_date; \r\n"\
            "\t\t\t}); \r\n"\
            "\t\t\t</script>"

def date_picker(id=None, label=None, date_format='mm/dd/yy', placeholder='',param_ref_name='date'):
    if id is None:
        id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))
    if label is None: label = ''
    start_id = "start_date" + id
    return "\t\t"+label+" <input id=\""+start_id+"\" type=\"text\" placeholder=\""+placeholder+"\" /><br /> \r\n"\
            "\t\t\t<input id=\"thesubmit\" type=\"button\" value=\"Submit\" /> \r\n"\
            "\t\t\t<script> \r\n"\
            "\t\t\t$('#"+start_id+"').datepicker({ \r\n"\
            "\t\t\t\t  dateFormat: '"+date_format+"',  \r\n"\
            "\t\t\t}); \r\n"\
            "\t\t\t$('#thesubmit').click(function() { \r\n"\
                "\t\t\t\t var start_date = $('#"+start_id+"').datepicker().val(); \r\n"\
                "\t\t\t\t window.location.href = \"?"+param_ref_name+"=\"+start_date\r\n"\
            "\t\t\t}); \r\n"\
            "\t\t\t</script>"

def tabs_page(tab2body_tuple_list, id=None):
    if tab2body_tuple_list is None:
        return ""
    if len(tab2body_tuple_list) == 0:
        return ""
    if id is None:
        id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))
    tabs = [t[0] for t in tab2body_tuple_list]
    bodies = [t[1] for t in tab2body_tuple_list]
    tabids = [''.join(random.choice(string.ascii_uppercase) for _ in range(12)) for k in range(len(tabs))]
    html = "<div id=\""+id+"\"> \r\n<ul> \r\n" + "".join(["<li><a href=\"#tabs-"+str(j)+"\">"+str(t)+"</a></li> \r\n" for (j,t) in zip(tabids,tabs)]) + "</ul> \r\n" + ("".join(["<div id=\"tabs-"+str(j)+"\"> \r\n""<p>"+str(b)+"</p> \r\n""</div> \r\n" for (j,b) in zip(tabids,bodies)])) + "</div> \r\n"

    js = "<script>$(function() {$(\"#"+id+"\").tabs();});</script>"
    return html+js

def table2auto_complete(table_id, placeholder = "search...", width="250px",height="50px",font_size="24px"):
    search_tool_id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))
    html = "<input type=\"text\" style=\"width:"+width+"; height:"+height+"; font-size: "+font_size+";\" id=\""+search_tool_id+"\" placeholder=\""+placeholder+"\"></input>"

    js = "<script>\r\n "\
            "\t\t\t$(\"#"+table_id+"\").hide(); \r\n"\
            "\t\t\t$(\"#"+search_tool_id +"\").on(\"keyup\", function() {\r\n"\
            "\t\t\t\tvar value = $(this).val();\r\n"\
            "\t\t\t\t$(\"#"+table_id+"\").hide();\r\n"\
                "\t\t\t\tif (value == \"\") { return; }\r\n"\
            "\t\t\t\t$(\"table#"+table_id+" tr\").each(function(index) {\r\n"\
                "\t\t\t\t\tif (index !== 0) {\r\n"\
                    "\t\t\t\t\t\t$row = $(this);\r\n"\
                    "\t\t\t\t\t\tvar id = $row.find(\"td:first\").text();\r\n"\
                    "\t\t\t\t\t\tif (id.toLowerCase().indexOf(value.toLowerCase()) !== 0) {\r\n"\
                        "\t\t\t\t\t\t\t$row.hide();\r\n"\
                    "\t\t\t\t\t\t}\r\n"\
                    "\t\t\t\t\telse {\r\n"\
                        "\t\t\t\t\t\t$(\"#"+table_id+"\").show();\r\n"\
                      "\t\t\t\t\t\t$row.show();\r\n"\
                    "\t\t\t\t\t}\r\n"\
                "\t\t\t\t}\r\n"\
            "\t\t\t});\r\n"\
        "\t\t});</script>\r\n"

    return html,js

def wrap_with_div(html, width='25%'):
    return "<div style=\"text-align: center;\">"\
    "<div style=\"width: "+width+"; margin: 0 auto;\">"+html+"</div>"\
    "</div>"

def upload_file_box(label, url_expected_by_server_in_post,params_to_pass_to_server = {},
                    path_to_upload_to="\\\\static\\\\ess_images\\\\", global_array_variable_name_to_store_img_ids=None):
    id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))
    file_container_id = 'FileContainer'+id
    result_div_id = 'ResultDiv'+id
    uploaded_files_list_div = 'uploadedFilesListDiv'+id

    save_img_code = "" if global_array_variable_name_to_store_img_ids is None else (global_array_variable_name_to_store_img_ids + ".push(img_id); \r\n")


    html = \
    "<label for=\""+file_container_id+"\">"+label+"</label> \r\n"\
    "<input id=\""+file_container_id+"\" type=\"file\"> \r\n"\
    "<div id=\""+uploaded_files_list_div+"\"></div> \r\n"\
    "<div id=\""+result_div_id+"\"></div> \r\n"\
    "<br> \r\n"

    js = \
      "$('input#"+file_container_id+"').on('change', function () { \r\n"\
		"$( \"#"+result_div_id+"\" ).html('starting'); \r\n"\
        "var file = document.getElementById('"+file_container_id+"').files[0]; \r\n"\
        "var reader = new FileReader(); \r\n"\
        "reader.readAsText(file, 'UTF-8'); \r\n"\
        "reader.onload = shipOff_"+id+"; \r\n"\
        "reader.onloadstart = function(){$( \"#"+result_div_id+"\" ).html('starting');} \r\n"\
        "reader.onprogress = function(){$( \"#"+result_div_id+"\" ).html('uploading...');} \r\n"\
        "reader.onabort = function(){$( \"#"+result_div_id+"\" ).html('aborted');} \r\n"\
        "reader.onerror = function(){$( \"#"+result_div_id+"\" ).html('error');} \r\n"\
    "}); \r\n"\
    "function shipOff_"+id+"(event) { \r\n"\
        "var result = event.target.result; \r\n"\
        "var file = document.getElementById('"+file_container_id+"').files[0]; \r\n"\
        "var formData = new FormData(); \r\n"\
        "var img_id = Math.random().toString(36).substring(7) \r\n" + \
        save_img_code + \
        "var static_path = '"+path_to_upload_to+"' + img_id + '___'  + file.name \r\n"\
        "formData.append('file', file); \r\n"\
        "formData.append('name',file.name); \r\n"\
        "formData.append('img_id',img_id) \r\n"

    for (name,val) in params_to_pass_to_server.iteritems():
        js += "formData.append('"+name+"','"+val+"')\r\n"

    js += "formData.append('folder','"+path_to_upload_to+"')\r\n"

    js += "$.ajax({ \r\n"\
           "url : '/"+url_expected_by_server_in_post+"', \r\n"\
           "type : 'POST', \r\n"\
           "data : formData, \r\n"\
           "processData: false, \r\n"\
           "contentType: false , \r\n"\
           "success: function (data) { $(\"#"+result_div_id+"\").html(\"Upload successful\"); $(\"#"+uploaded_files_list_div+"\").append('uploaded '+file.name+'<br>' ) }, \r\n"\
           "error: function (data) { $(\"#"+result_div_id+"\").html(\"Upload failed\"); } \r\n"\
    "}).done(function() { \r\n"\
        "$(\"#"+result_div_id+"\").empty(); \r\n"\
        "var filename = file.name ; \r\n"\
        "var extension = (filename.split('.'))[1] \r\n"\
        "var is_pic = $.inArray(extension.toLowerCase(), [\"jpg\",\"jpeg\",\"bmp\",\"png\",\"gif\",\"tif\"]) >= 0 \r\n"\
        "if (is_pic == false) { return; } \r\n"\
        "var img = $('<img />').attr({ \r\n"\
            "'id': 'uploadedImgId', \r\n"\
            "'src': static_path, \r\n"\
            "'alt': 'image', \r\n"\
            "'title': 'title', \r\n"\
            "'width': 750 \r\n"\
        "}).appendTo('#"+uploaded_files_list_div+"'); \r\n"\
        "$('#"+uploaded_files_list_div+"').append('<br>'); \r\n"\
    "}); \r\n"\
    "} \r\n"

    return html, "<script>"+js+"</script>"
