import random
import string
import datetime



def df2json(df,decimal_pts=1):
    try:
        data = [[((datetime.datetime.strptime(str(df.ix[idx][df.columns[0]]) + " 00:00:00", '%Y-%m-%d %H:%M:%S')) - datetime.datetime(1970, 1,1)).total_seconds() * 1000, round(float(df.ix[idx][df.columns[1]]),decimal_pts)] for idx in df.index]
    except:
        try:
            data = [[((datetime.datetime.strptime(str(df.ix[idx][df.columns[0]]) + " 00:00:00", '%m-%d-%Y %H:%M:%S')) - datetime.datetime(1970, 1,1)).total_seconds() * 1000, round(float(df.ix[idx][df.columns[1]]),decimal_pts)] for idx in df.index]
        except:
            try:
                data = [[((datetime.datetime.strptime(str(df.ix[idx][df.columns[0]]), '%Y-%m-%d %H:%M:%S')) - datetime.datetime(1970, 1,1)).total_seconds() * 1000, round(float(df.ix[idx][df.columns[1]]),decimal_pts)] for idx in df.index]
            except:
                data=""
    return data

def stacked_bar_chart(title,xlabel, data, labels_enabled=False, embed_links=False, source=None, disable_ylabel=False, url="", param1="",width="1200px",height="600px"):
    """data is dict of dicts. It's a mapping between stack component (color) to category to numerical value. data is x2y2value"""
    if len(data.values()) == 0:
        return "<h9>NO DATA</h9>"
    if not isinstance(data.values()[0],dict): data = {" ":data}
    min_y = data[data.keys()[0]].values()[0]
    for c in data:
        if min_y > min(data[c].values()):
            min_y = min(data[c].values())
    categories = []
    for x in data:
        for y in data[x]: categories.append(y)
    categories = list(set(categories))
    categories.sort()
    link_code = "location.href = '"+url+"?cat=' + this.category + '&xlabel='+ this.series.name + '&source=" + source + "' + '&param1=" + str(param1) + "' ;" if embed_links else ""
    labels_code = "dataLabels: {enabled: true}, " if labels_enabled else " "
    id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))
    return \
    "<div id=\""+id+"\" style=\"min-width: 0px; max-width: "+width+"; height: "+height+"; margin: 0 auto\"></div> \r\n"\
    "<script>\r\n "\
    "\t$(function () { \r\n"\
    "\t$('#"+id+"').highcharts({ \r\n"\
    "\t\t    chart: { \r\n"\
    "\t\t\t        type: 'bar' \r\n"\
    "\t\t    }, \r\n"\
    "\t\t    title: { \r\n"\
    "\t\t\t            text: '"+title+"' \r\n"\
    "\t\t        }, \r\n"\
    "\t\t        xAxis: { \r\n"\
    "\t\t\t            "+("labels: { enabled: false }, " if disable_ylabel else "")+"\r\n"\
    "\t\t\t            categories: [" + ",".join(["'"+c+"'" for c in categories ]) + "] \r\n"\
    "\t\t        }, \r\n"\
    "\t\t        yAxis: { \r\n"\
    "\t\t            min: "+str(min_y)+", \r\n"\
    "\t\t            title: { \r\n"\
    " \t\t\t               text: '"+xlabel+"' \r\n"\
    "\t\t            } \r\n"\
    "\t        }, \r\n"\
    "\t        legend: { \r\n"\
    "\t\t            reversed: true \r\n"\
    "\t       }, \r\n"\
    "\t        plotOptions: { \r\n"\
    "\t            series: { \r\n"\
    "\t\t               stacking: 'normal', \r\n"\
    "\t\t               "+labels_code+"\r\n"\
    "\t\t               cursor: 'pointer', \r\n"\
    "\t\t               point: { \r\n"\
    "\t\t\t                     events: { \r\n"\
    "\t\t\t\t                       click: function () { \r\n"\
    "\t\t\t\t\t                            "+link_code+" \r\n "\
    "\t\t\t\t                    } \r\n"\
    "\t\t\t                   } \r\n"\
    "\t\t\t                   } \r\n"\
    "\t\t            } \r\n"\
    "\t        }, \r\n"\
    "\t        series: [" + ",".join([("{ name: '"+str(x)+"',\r\n" + " data: " + "[" + ",".join([str(val[1]) for val in sorted(data[x].items(),key=lambda x:x[0])]) + "]}") for x in sorted(data.keys())])  + "]" \
    "\t    }); \r\n"\
    "}); \r\n"\
    "</script>"

def stacked_bar_chart_negative(title, ylabel, data, labels_enabled=False, embed_links=False,dec_rounding=0, source=None,url="", param1="",width="1200px",height="600px"):
    """data is dict of dicts. It's a mapping between stack component (color) to category to numerical value.
    supports negative values """
    if len(data.values()) ==0:
        return "<b>NO DATA</b>"
    if not isinstance(data.values()[0],dict): data = {k:{"":data[k]} for k in data}
    categories = []
    for k in data:
        for cat in data[k]: categories.append(cat)
    categories = list(set(categories)) # categories are funds.
    categories.sort()
    link_code = "location.href = '"+url+"?cat=' + this.category + '&xlabel='+ this.series.name + '&source=" + source + "' + '&param1=" + str(param1) + "' ;" if embed_links else ""
    labels_code = "\t\t               dataLabels: {enabled: true}, " if labels_enabled else " "
    id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))
    return \
            "<div id=\""+id+"\" style=\"min-width: 0px; max-width: "+width+"; height: "+height+"; margin: 0 auto\"></div> \r\n"\
            "<script>\r\n "\
            "\t$(function () { \r\n"\
            "\t$('#"+id+"').highcharts({ \r\n"\
            "\t\t    chart: { \r\n"\
            "\t\t\t        type: 'column' \r\n"\
            "\t\t    }, \r\n"\
            "\t\t    title: { \r\n"\
            "\t\t\t            text: '"+title+"' \r\n"\
            "\t\t        }, \r\n"\
            "\t\t        xAxis: { \r\n"\
            "\t\t\t            categories: [" + ",".join(["'"+str(c)+"'" for c in categories ]) + "] \r\n"\
            "\t\t        }, \r\n"\
            "\t\t        yAxis: { \r\n"\
            "\t\t            title: { \r\n"\
            " \t\t\t               text: '"+ylabel+"' \r\n"\
            "\t\t\t            } \r\n"\
            "\t\t            }, \r\n"\
            "\t        plotOptions: { \r\n"\
            "\t            series: { \r\n"\
            "\t\t               "+labels_code+"\r\n"\
            "\t\t               cursor: 'pointer', \r\n"\
            "\t\t               point: { \r\n"\
            "\t\t\t                     events: { \r\n"\
            "\t\t\t\t                       click: function () { \r\n"\
            "\t\t\t\t\t                            "+link_code+" \r\n "\
            "\t\t\t\t                    } \r\n"\
            "\t\t\t                   } \r\n"\
            "\t\t\t                   } \r\n"\
            "\t\t            } \r\n"\
            "\t        }, \r\n"\
            "\t\t        credits: { \r\n"\
            "\t\t\t            enabled: false \r\n"\
            "\t\t        }, \r\n"\
            "\t        series: [" + ",".join([("{ name: '"+str(c)+"',\r\n" + " data: " + "[" + ",".join([str(round(data[c][a],dec_rounding)) for a in sorted(data[c]) ]) + "]}") for c in data])  + "]" \
            "\t    }); \r\n"\
            "}); \r\n"\
            "</script>"

#"\t        series: [" + ",".join([("{ name: '"+str(c)+"',\r\n" + " data: " + "[" + ",".join([str(round(a,dec_rounding)) for a in data[c].values()]) + "]}") for c in data])  + "]" \
def donut_chart(ctg2subctg2data,title="title",subtitle="subtitle",inner_series_name="inner_series",outter_series_name="outer_series",width="1200px",height="600px"):
    id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))
    categories = ctg2subctg2data.keys()
    ctg2color = {ctg: str(i) for (ctg,i) in zip(ctg2subctg2data,range(len(ctg2subctg2data)))}
    return \
        "<div id=\""+id+"\" style=\"width: "+width+"; height: "+height+";\"></div>\r\n"\
        "<script>\r\n"\
        "\t\t$(function () { \r\n"\
        "\t\t\tvar colors = Highcharts.getOptions().colors, \r\n"\
        "\t\t\tcategories = [ " + ",".join(["'"+c+"'" for c in categories]) + "], \r\n"\
        "\t\t\tdata = [\r\n" + "\t\t\t\t,\r\n".join([
            "\t\t\t\t{y: " + str(sum(ctg2subctg2data[ctg].values())) + ", \r\n" \
            "\t\t\t\tcolor: colors["+ctg2color[ctg]+"], \r\n" \
            "\t\t\t\tdrilldown: { \r\n"\
                "\t\t\t\t\tname: 'z" + ctg2color[ctg] +"q', \r\n"\
                "\t\t\t\t\tcategories: [" + ",".join([ "'" + subctg + "'" for subctg in ctg2subctg2data[ctg].keys()]) + "], \r\n" \
                "\t\t\t\t\tdata: ["+ ",".join([str(d) for d in  ctg2subctg2data[ctg].values()]) +"], \r\n"\
                "\t\t\t\t\tcolor: colors["+ctg2color[ctg]+"] \r\n"\
                "\t\t\t\t\t}\r\n" \
        "\t\t\t\t}\r\n" for ctg in ctg2subctg2data]) + "\t\t\t\t], \r\n"\
        "\t\t\t        browserData = [], \r\n"\
        "\t\t\t        versionsData = [], \r\n"\
        "\t\t\t        i, \r\n"\
        "\t\t\t        j, \r\n"\
        "\t\t\t        dataLen = data.length, \r\n"\
        "\t\t\t        drillDataLen, \r\n"\
        "\t\t\t        brightness; \r\n"\
        "\t\t\t \r\n"\
        "\t\t\t \r\n"\
        "\t\t\t    for (i = 0; i < dataLen; i += 1) { \r\n"\
        "\t\t\t \r\n"\
        "\t\t\t        browserData.push({ \r\n"\
        "\t\t\t            name: categories[i], \r\n"\
        "\t\t\t            y: data[i].y, \r\n"\
        "\t\t\t            color: data[i].color \r\n"\
        "\t\t\t        }); \r\n"\
        "\t\t\t \r\n"\
        "\t\t\t        drillDataLen = data[i].drilldown.data.length; \r\n"\
        "\t\t\t        for (j = 0; j < drillDataLen; j += 1) { \r\n"\
        "\t\t\t            brightness = 0.2 - (j / drillDataLen) / 5; \r\n"\
        "\t\t\t            versionsData.push({ \r\n"\
        "\t\t\t                name: data[i].drilldown.categories[j], \r\n"\
        "\t\t\t                y: data[i].drilldown.data[j], \r\n"\
        "\t\t\t                color: Highcharts.Color(data[i].color).brighten(brightness).get() \r\n"\
        "\t\t\t            }); \r\n"\
        "\t\t\t        } \r\n"\
        "\t\t\t    } \r\n"\
        "\t\t\t \r\n"\
        "\t\t\t    $('#"+id+"').highcharts({ \r\n"\
        "\t\t\t        chart: { \r\n"\
        "\t\t\t            type: 'pie' \r\n"\
        "\t\t\t        }, \r\n"\
        "\t\t\t        title: { \r\n"\
        "\t\t\t            text: '"+title+"' \r\n"\
        "\t\t\t        }, \r\n"\
        "\t\t\t        subtitle: { \r\n"\
        "\t\t\t            text: '"+subtitle+"' \r\n"\
        "\t\t\t        }, \r\n"\
        "\t\t\t        yAxis: { \r\n"\
        "\t\t\t            title: { \r\n"\
        "\t\t\t                text: 'yAxis' \r\n"\
        "\t\t\t            } \r\n"\
        "\t\t\t        }, \r\n"\
        "\t\t\t        plotOptions: { \r\n"\
        "\t\t\t            pie: { \r\n"\
        "\t\t\t                shadow: false, \r\n"\
        "\t\t\t                center: ['50%', '50%'] \r\n"\
        "\t\t\t            } \r\n"\
        "\t\t\t        }, \r\n"\
        "\t\t\t        tooltip: { \r\n"\
        "\t\t\t            valueSuffix: '%' \r\n"\
        "\t\t\t        }, \r\n"\
        "\t\t\t        series: [{ \r\n"\
        "\t\t\t            name: '"+inner_series_name+"', \r\n"\
        "\t\t\t            data: browserData, \r\n"\
        "\t\t\t            size: '60%', \r\n"\
        "\t\t\t            dataLabels: { \r\n"\
        "\t\t\t                formatter: function () { \r\n"\
        "\t\t\t                    return this.y > 5 ? this.point.name : null; \r\n"\
        "\t\t\t                }, \r\n"\
        "\t\t\t                color: '#ffffff', \r\n"\
        "\t\t\t                distance: -30 \r\n"\
        "\t\t\t            } \r\n"\
        "\t\t\t        }, { \r\n"\
        "\t\t\t            name: '"+outter_series_name+"', \r\n"\
        "\t\t\t            data: versionsData, \r\n"\
        "\t\t\t            size: '80%', \r\n"\
        "\t\t\t            innerSize: '60%', \r\n"\
        "\t\t\t            dataLabels: { \r\n"\
        "\t\t\t                formatter: function () { \r\n"\
        "\t\t\t                    return '<b>' + this.point.name + ':</b> ' + this.y + '%'\r\n"\
        "\t\t\t                } \r\n"\
        "\t\t\t            } \r\n"\
        "\t\t\t        }] \r\n"\
        "\t\t\t    }); \r\n"\
        "}); "\
        "\r\n</script>"

def pie_drilldown_chart(id,ctg2subctg2wgt,title="",series_name="", width="1200px", height="600px"):
    return "<div id=\""+id+"\" style=\"width: "+width+"; height: "+height+"; margin: 0 auto\"></div>\r\n"\
            "<script>\r\n"\
            "$(function () {                                                                                                                                  \r\n"\
            "$('#"+id+"').highcharts({                                                                                                                        \r\n"\
                "\tchart: {                                                                                                                                   \r\n"\
                    "\t\ttype: 'pie'                                                                                                                          \r\n"\
                "\t},                                                                                                                                         \r\n"\
                "\ttitle: {                                                                                                                                   \r\n"\
                    "\t\ttext: '"+title+"'                                                                                              \r\n"\
                "\t},                                                                                                                                     \r\n"\
                "\tsubtitle: {                                                                                                                         \r\n"\
                    "\t\ttext: 'Click the slices to drill-down.'                                                                                   \r\n"\
                "\t},                                                                                                                                      \r\n"\
                "\tplotOptions: {                                                                                                                              \r\n"\
                    "\t\tseries: {                                                                                                                      \r\n"\
                        "\t\t\tdataLabels: {                                                                                                                            \r\n"\
                            "\t\t\t\tenabled: true,                                                                                                               \r\n"\
                            "\t\t\t\tformat: '{point.name}: {point.y:.1f}%'                                                                                 \r\n"\
                        "\t\t\t}                                                                                                                             \r\n"\
                    "\t\t}                                                                                                                                    \r\n"\
                "\t},                                                                                                                                        \r\n"\
                "\ttooltip: {                                                                                                                                    \r\n"\
                    "\t\theaderFormat: '<span style=\"font-size:11px\">{series.name}</span><br>',                                                                            \r\n"\
                    "\t\tpointFormat: '<span style=\"color:{point.color}\">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'                                                                            \r\n"\
                "\t},                                                                                                                                         \r\n"\
                "\tseries: [{                                                                                                                                     \r\n"\
                    "\t\tname: '"+series_name+"',                                                                                                                           \r\n"\
                    "\t\tcolorByPoint: true,    " \
                    "\t\t data: ["+",".join([("{ name: '" + ctg + "', y: " + str(sum(ctg2subctg2wgt[ctg].values())) + ", drilldown: '" + ctg + "'}") for ctg in ctg2subctg2wgt.keys()])+"] \r\n" \
                "\t}],                                                                            \r\n"\
                "\tdrilldown: {     " \
                "\t\t series: ["+",".join([("{ name: '" + ctg + "', id: '"+ctg+"', data: ["+",".join([("['"+subctg + "'," + str(ctg2subctg2wgt[ctg][subctg]) + "]") for subctg in ctg2subctg2wgt[ctg]])+"]" + "}") for ctg in ctg2subctg2wgt.keys()])+"] \r\n" \
                "\t}                                                                                                                                              \r\n"\
            "});                                                                                                                                                           \r\n"\
        "}); \r\n"\
        "</script>"

def simple_pie_chart(ctg2wgt,id=None,title="",series_name="",width="1200px",height="600px"):
    if len(ctg2wgt) == 0:
        return "<h9>NO DATA</h9>"
    if id is None:
        id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))
    return "<div id=\""+id+"\" style=\"width: "+width+"; height: "+height+"; margin: 0 auto\"></div>\r\n"\
    "<script>\r\n"\
    "$(function () {\r\n"\
    "$('#"+id+"').highcharts({\r\n"\
        "\tchart: {\r\n"\
            "\t\tplotBackgroundColor: null,\r\n"\
            "\t\tplotBorderWidth: null,\r\n"\
            "\t\tplotShadow: false,\r\n"\
            "\t\ttype: 'pie'\r\n"\
        "\t},\r\n"\
        "\ttitle: {\r\n"\
            "\t\ttext: '"+title+"'\r\n"\
        "\t},\r\n"\
        "\ttooltip: {\r\n"\
            "\t\tpointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>'\r\n"\
        "\t},\r\n"\
        "\tplotOptions: {\r\n"\
            "\t\tpie: {\r\n"\
                "\t\t\tallowPointSelect: true,\r\n"\
                "\t\t\tcursor: 'pointer',\r\n"\
                "\t\t\tdataLabels: {\r\n"\
                    "\t\t\t\tenabled: true,\r\n"\
                    "\t\t\t\tformat: '<b>{point.name}</b>: {point.percentage:.2f} %',\r\n"\
                    "\t\t\t\tstyle: {\r\n"\
                        "\t\t\t\t\tcolor: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'\r\n"\
                    "\t\t\t}\r\n"\
                "\t\t\t}\r\n"\
            "\t\t}\r\n"\
        "\t},\r\n"\
        "\tseries: [{\r\n"\
            "\t\tname: '"+series_name+"',\r\n"\
            "\t\tcolorByPoint: true,\r\n"\
            "\t\tdata: [" + ",".join([("{name: '"+x+"', y: "+str(ctg2wgt[x])+"}") for x in ctg2wgt]) + "]\r\n"\
        "\t}]\r\n"\
    "\t});\r\n"\
"});\r\n"\
"</script>\r\n"

def to_chart(charts, chart_id,unit="$", seperate_y_axis = False):

    if seperate_y_axis:
        yaxis = "\t\t\t\tyAxis: [" + ",".join([("{title: {text: '"+charts[k].ylabel+"'}" + ("" if k ==0 else ",opposite: true") + "}") for  k in range(len(charts))]) + "], \r\n"
    else:
        yaxis = "\t\t\t\tyAxis: {" \
                "\t\t\t\t\ttitle: {text: '" + \
                charts[0].ylabel + "'}\r\n" \
                "\t\t\t\t},\r\n" \

    return "\r\n<div id=\"" + chart_id + "\" style=\"min-width: 1500px; height: 400px; margin: 0 auto\"></div>\r\n" \
         "<script>\r\n" \
         "\t$(function () {\r\n" \
         "\t$('#" + chart_id + "').highcharts({\r\n" \
                               "\t\t\t\ttitle: {\r\n" \
                               "\t\t\t\t\ttext: '" + charts[0].title + "',\r\n" \
                               "\t\t\t\t\tx: -20 //center\r\n" \
                               "\t\t\t\t},\r\n" \
                               "\t\t\t\txAxis: {\r\n" \
                               "\t\t\t\t\ttype: 'datetime'\r\n" \
                               "\t\t\t\t},\r\n" \
                               + yaxis +\
                              "\t\t\t\ttooltip: {\r\n" \
                              "\t\t\t\t\tshared: true," \
                              "\t\t\t\t\tvaluePrefix: '\\"+unit+"'\r\n" \
                              "\t\t\t\t},\r\n" \
                              "\t\t\t\tcredit: { enabled: false },\r\n" \
                              "\t\t\t\tlegend: {\r\n" \
                              "\t\t\t\t\tlayout: 'vertical',\r\n" \
                              "\t\t\t\t\talign: 'right',\r\n" \
                              "\t\t\t\t\tverticalAlign: 'middle',\r\n" \
                              "\t\t\t\t\tborderWidth: 0\r\n" \
                              "\t\t\t\t},\r\n" \
                              "\t\t\t\tseries: [" + ",".join(["{type: '" + charts[k].type + "', name: '" + charts[k].name + "',"
                                                            + ("yAxis: " + str(k) + "," if k > 0 and seperate_y_axis else "")
                                                            +"data: " + str(charts[k].to_epoc_json_ts()) + "}" for k in range(len(charts))]) + "]\r\n" \
                                                           "\t\t\t});\r\n" \
                                                           "\t\t});\r\n" \
                                                           "\t</script>\r\n"

def zoomable_time_series_chart(dfs, id=None, title="title",subtitle="subtitle", chart_type="line", width="740px",height="600px",decimal_pt_rounding=1, shared_tooltip=False, hide_legend=False, show_ylabel=True, transparent_background=False,show_export_btn=True, hidden_cols=[], visible_cols=[], ylabel=None):
    if id is None:
        id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))
    if not isinstance(dfs,list): dfs = [dfs]

    tooltip = "\t\t\ttooltip: { shared: true }, \r\n" if shared_tooltip else ""
    background = "\t\t\t\tbackgroundColor: null, \r\n" if transparent_background else ""
    return \
    "<div id="+id+" style=\"width: "+width+"; height: "+height+"; margin: 0 auto\"></div> \r\n"\
    "<script>\r\n"\
    "\t$(function () { \r\n"\
        "\t\t$('#"+id+"').highcharts({\r\n"\
            "\t\t\tchart: {\r\n"\
                + background +\
                "\t\t\t\tzoomType: 'x' \r\n"\
            "\t\t\t},\r\n"\
            "\t\t\ttitle: {\r\n"\
                "\t\t\t\ttext: '"+title+"'\r\n"\
            "\t\t\t},\r\n"\
            "\t\t\tsubtitle: {\r\n"\
                "\t\t\t\ttext: '"+subtitle+"' \r\n"\
            "\t\t\t},\r\n"\
            "\t\t\txAxis: {\r\n"\
                "\t\t\t\ttype: 'datetime'\r\n"\
            "\t\t\t},\r\n"\
            "\t\t\tyAxis: {\r\n"\
                "\t\t\t\ttitle: {\r\n"\
                    "\t\t\t\t\ttext: '"+( (dfs[0].columns[1] if ylabel is None else ylabel) if show_ylabel else '')+"'\r\n"\
                "\t\t\t\t}\r\n"\
            "\t\t\t},\r\n"\
            "\t\t\tlegend: {\r\n"\
                "\t\t\t\tenabled: "+('true' if not hide_legend else 'false')+" \r\n"\
            "\t\t\t},\r\n"\
            "\t\t\t\texporting: { enabled: "+('true' if show_export_btn else 'false')+" },\r\n"\
            + tooltip +\
            "\t\t\tplotOptions: {\r\n"\
                "\t\t\t\tarea: {\r\n"\
                    "\t\t\t\t\tfillColor: {\r\n"\
                        "\t\t\t\t\t\tlinearGradient: {\r\n"\
                            "\t\t\t\t\t\t\tx1: 0,\r\n"\
                            "\t\t\t\t\t\t\ty1: 0,\r\n"\
                            "\t\t\t\t\t\t\tx2: 0,\r\n"\
                            "\t\t\t\t\t\t\ty2: 1\r\n"\
                        "\t\t\t\t\t\t},\r\n"\
                        "\t\t\t\t\t\tstops: [\r\n"\
                            "\t\t\t\t\t\t\t[0, Highcharts.getOptions().colors[0]],\r\n"\
                            "\t\t\t\t\t\t\t[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]\r\n"\
                        "\t\t\t\t\t\t]\r\n"\
                    "\t\t\t\t\t},\r\n"\
                    "\t\t\t\t\tmarker: {\r\n"\
                        "\t\t\t\t\t\tradius: 2\r\n"\
                    "\t\t\t\t\t},\r\n"\
                    "\t\t\t\t\tlineWidth: 1,\r\n"\
                    "\t\t\t\t\tstates: {\r\n"\
                        "\t\t\t\t\t\thover: {\r\n"\
                            "\t\t\t\t\t\t\tlineWidth: 1\r\n"\
                        "\t\t\t\t\t\t}\r\n"\
                    "\t\t\t\t\t},\r\n"\
                    "\t\t\t\t\tthreshold: null\r\n"\
                "\t\t\t\t}\r\n"\
            "\t\t\t},\r\n"\
            "\t\t\tseries: ["+ \
                ",".join([("{\r\n"\
                "\t\t\t\ttype: '"+chart_type+"',\r\n"\
                "\t\t\t\tvisible: "+('false' if df.columns[1] in hidden_cols else ('true' if (len(visible_cols) == 0 or df.columns[1] in visible_cols) else 'false'))+",\r\n"\
                "\t\t\t\tname: '"+df.columns[1]+"',\r\n"\
                "\t\t\t\tdata: "+str(df2json(df,decimal_pt_rounding))+"\r\n"\
            "\t\t\t}") for df in dfs]) +\
          "]\r\n"\
    "\t});\r\n"\
"\t});\r\n"\
"</script>\r\n"

def zoomable_time_series_chart_double_yaxis(left_dfs, right_dfs, id=None, title="title",subtitle="subtitle",
                                            chart_type="line", width="740px",height="600px",decimal_pt_rounding=1,
                                            shared_tooltip=False, hide_legend=False,
                                            show_left_ylabel=True, show_right_ylabel=True,
                                            transparent_background=False,show_export_btn=True, hidden_cols=[],
                                            left_ylabel=None,right_ylabel=None):

    if id is None: id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))
    if not isinstance(left_dfs,list): left_dfs = [left_dfs]
    if not isinstance(right_dfs,list): right_dfs = [right_dfs]

    tooltip = "\t\t\ttooltip: { shared: true }, \r\n" if shared_tooltip else ""
    background = "\t\t\t\tbackgroundColor: null, \r\n" if transparent_background else ""
    return \
    "<div id="+id+" style=\"width: "+width+"; height: "+height+"; margin: 0 auto\"></div> \r\n"\
    "<script>\r\n"\
        "\t\t$('#"+id+"').highcharts({\r\n"\
            "\t\t\tchart: {\r\n"\
                + background +\
                "\t\t\t\tzoomType: 'x' \r\n"\
            "\t\t\t},\r\n"\
            "\t\t\ttitle: {\r\n"\
                "\t\t\t\ttext: '"+title+"'\r\n"\
            "\t\t\t},\r\n"\
            "\t\t\tsubtitle: {\r\n"\
                "\t\t\t\ttext: '"+subtitle+"' \r\n"\
            "\t\t\t},\r\n"\
            "\t\t\txAxis: {\r\n"\
                "\t\t\t\ttype: 'datetime'\r\n"\
            "\t\t\t},\r\n"\
            "\t\t\tyAxis:  [{\r\n" \
                "\t\t\t\ttitle: {\r\n"\
                    "\t\t\t\t\ttext: '"+( (left_dfs[0].columns[1] if left_ylabel is None else left_ylabel) if show_left_ylabel else '')+"'\r\n"\
                "\t\t\t\t}\r\n"\
            "\t\t\t}, \r\n "\
            "\t\t\t\t{title: {\r\n"\
                    "\t\t\t\t\ttext: '"+( (right_dfs[0].columns[1] if right_ylabel is None else right_ylabel) if show_right_ylabel else '')+"'\r\n"\
                "\t\t\t\t}, \r\n"\
                "\t\t\t\topposite: true \r\n"\
            "\t\t\t}], \r\n "\
            "\t\t\tlegend: {\r\n"\
                "\t\t\t\tenabled: "+('true' if not hide_legend else 'false')+" \r\n"\
            "\t\t\t},\r\n"\
            "\t\t\t\texporting: { enabled: "+('true' if show_export_btn else 'false')+" },\r\n"\
            + tooltip +\
            "\t\t\tplotOptions: {\r\n"\
                "\t\t\t\tarea: {\r\n"\
                    "\t\t\t\t\tfillColor: {\r\n"\
                        "\t\t\t\t\t\tlinearGradient: {\r\n"\
                            "\t\t\t\t\t\t\tx1: 0,\r\n"\
                            "\t\t\t\t\t\t\ty1: 0,\r\n"\
                            "\t\t\t\t\t\t\tx2: 0,\r\n"\
                            "\t\t\t\t\t\t\ty2: 1\r\n"\
                        "\t\t\t\t\t\t},\r\n"\
                        "\t\t\t\t\t\tstops: [\r\n"\
                            "\t\t\t\t\t\t\t[0, Highcharts.getOptions().colors[0]],\r\n"\
                            "\t\t\t\t\t\t\t[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]\r\n"\
                        "\t\t\t\t\t\t]\r\n"\
                    "\t\t\t\t\t},\r\n"\
                    "\t\t\t\t\tmarker: {\r\n"\
                        "\t\t\t\t\t\tradius: 2\r\n"\
                    "\t\t\t\t\t},\r\n"\
                    "\t\t\t\t\tlineWidth: 1,\r\n"\
                    "\t\t\t\t\tstates: {\r\n"\
                        "\t\t\t\t\t\thover: {\r\n"\
                            "\t\t\t\t\t\t\tlineWidth: 1\r\n"\
                        "\t\t\t\t\t\t}\r\n"\
                    "\t\t\t\t\t},\r\n"\
                    "\t\t\t\t\tthreshold: null\r\n"\
                "\t\t\t\t}\r\n"\
            "\t\t\t},\r\n"\
            "\t\t\tseries: ["+ \
                ",".join([("{\r\n"\
                "\t\t\t\ttype: '"+chart_type+"',\r\n"\
                "\t\t\t\tvisible: "+('false' if df.columns[1] in hidden_cols else 'true')+",\r\n"\
                "\t\t\t\tname: '"+df.columns[1]+"',\r\n"\
                "\t\t\t\tdata: "+str(df2json(df,decimal_pt_rounding))+"\r\n"\
            "\t\t\t}") for df in left_dfs]) + "," +\
        ",".join([("{\r\n"\
                "\t\t\t\ttype: '"+chart_type+"',\r\n"\
                "\t\t\t\tyAxis: 1,\r\n"\
                "\t\t\t\tvisible: "+('false' if df.columns[1] in hidden_cols else 'true')+",\r\n"\
                "\t\t\t\tname: '"+df.columns[1]+"',\r\n"\
                "\t\t\t\tdata: "+str(df2json(df,decimal_pt_rounding))+"\r\n"\
            "\t\t\t}") for df in right_dfs]) +\
          "]\r\n"\
    "\t});\r\n"\
"</script>\r\n"



#subcategories should sum up to 100%
# cat2subcat - list of tuples (cat,subcat)
# subcat2values = dict of subcat to ordered list of values
def bar_drilldown(title, subtitle, ylabel, cat_value_pairs,cat2subcat_values_pairs, lvl1_suffix="%",lvl2_suffix="%",decimal_pts=1,width="1200px",height="600px", url=None, lvl1_param_name=None, lvl2_param_name=None, additional_params_pairs=[]):
    id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))
    highcharts_js = "$(function () {    \r\n    $('#"+id+"').highcharts({\r\n        chart: {\r\n            type: 'column'\r\n        },\r\n        title: {\r\n            text: '"+title+"'\r\n        },\r\n        subtitle: {\r\n            text: '"+subtitle+"'\r\n        },\r\n        xAxis: {\r\n            type: 'category'\r\n        },\r\n        yAxis: {\r\n            title: {\r\n                text: '"+ylabel+"'\r\n            }\r\n\r\n        },\r\n        legend: {\r\n            enabled: false\r\n        },\r\n        plotOptions: {\r\n            series: {\r\n                borderWidth: 0,\r\n                dataLabels: {\r\n                    enabled: true,\r\n                    format: '{point.y:."+str(decimal_pts)+"f}"+lvl1_suffix+"'\r\n                }\r\n            }\r\n        },\r\n\r\n        tooltip: {\r\n            headerFormat: '',\r\n            pointFormat: 'click for drilldown'\r\n        },\r\n"
    data = "series: [{ data: [" + ",".join(["{ name: '"+str(cat)+"',drilldown: '"+str(cat)+"',y:"+str(cat_val)+" }" for (cat,cat_val) in cat_value_pairs ]) + "]}]"



    drilldown = "drilldown: { series: [" + ",".join(["{ "+("" if (url is None) else  "point: { events: { click: function () { window.open('"+url+"?"+lvl1_param_name+"="+str(cat)+"&"+lvl2_param_name+"='+this.name+'&"+("&".join([str(p)+"="+str(v) for (p,v) in additional_params_pairs]))+"', '"+id+"', 'toolbars=0, width=2500,height=1030,left=200,top=200,scrollbars=1,resizeable=1'); } } }, \r\n ")+" dataLabels: { format: '{point.y:."+str(decimal_pts)+"f}"+lvl2_suffix+"' }, name: '"+str(cat)+"',id: '"+str(cat)+"',data: ["+",".join(["['"+str(subcat)+"',"+str(subcata_val)+"]" for (subcat,subcata_val) in cat2subcat_values_pairs[cat] ])+"]}" for (cat,cat_val) in cat_value_pairs]) + "]}"

    js = highcharts_js + data + "," + drilldown + "});});"

    return "<div id=\""+id+"\" style=\"min-width: 0px; max-width: "+width+"; height: "+height+"; margin: 0 auto\"></div> \r\n"\
            "<script>"+js+"</script>"

def hist(y_vec, x_vec,title,subtitle,ylabel,xlabel,width="740px",height="600px", x_axis_rounding=2,y_axis_rounding=2, is_pure_column_chart=False):
    id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))
    #categories =  [str(x) for x in x_vec] if is_pure_column_chart else [('['+str(round(a,x_axis_rounding))+','+str(round(b,x_axis_rounding))+']') for (a,b) in zip(x_vec[:-1],x_vec[1:])]
    categories =  [str(x) for x in x_vec] if is_pure_column_chart else [str(round(x,x_axis_rounding)) for x in x_vec[1:]]
    js = "<script>$(function () { \r\n"\
            "$('#"+id+"').highcharts({\r\n"\
                "chart: {\r\n"\
                    "type: 'column'\r\n"\
                "},\r\n"\
                "title: {\r\n"\
                    "text: '"+title+"'\r\n"\
                "},\r\n"\
                "subtitle: {\r\n"\
                    "text: '"+subtitle+"'\r\n"\
                "},\r\n"\
                "xAxis: {\r\n"\
                    "categories: ["+",".join([("'"+cat+"'") for cat in categories])+"], \r\n"\
                    "crosshair: true\r\n"\
                "},\r\n"\
                "yAxis: {\r\n"\
                    "min: 0,\r\n"\
                    "title: {\r\n"\
                        "text: '"+ylabel+"'\r\n"\
                    "}\r\n"\
                "},\r\n"\
                "tooltip: {\r\n"\
                    "headerFormat: '<span style=\"font-size:10px\">Pr[{series.name} in {point.key}]={point.y:.1f}%</span><table>',\r\n"\
                    "pointFormat: '', \r\n"\
                    "footerFormat: '</table>',\r\n"\
                    "shared: true,\r\n"\
                    "useHTML: true\r\n"\
                "},\r\n"\
                "plotOptions: {\r\n"\
                  "column: {\r\n"\
                    "pointPadding: 0,\r\n"\
                    "borderWidth: 0,\r\n"\
                    "groupPadding: 0,\r\n"\
                    "shadow: false\r\n"\
                  "}\r\n"\
                "},\r\n"\
                "series: [{\r\n"\
                    "name: '"+xlabel+"',\r\n"\
                    "data: ["+",".join([str(round(y,y_axis_rounding)) for y in y_vec])+"]\r\n"\
                "}]\r\n"\
            "});\r\n"\
        "});\r\n</script>"

    html = "<div id=\""+id+"\" style=\"min-width: "+width+"; height: "+height+"; margin: 0 auto\"></div>"

    return html,js

def sparkline_table(df):
    if len(df.columns) == 0: return "",""
    div_id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))
    table_id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))
    tbody_id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))

    def generate_row(row):
        row_html = "\t\t<tr>\r\n"\
                "\t\t\t<th>"+str(row[0])+"</th>\r\n"

        for j in range(1,len(row)):
            is_iter = True
            try: iterator = iter(row[j])
            except TypeError: is_iter = False
            if is_iter: row_html += "<td data-sparkline=\""+",".join([' '+str(x)for x in iterator])+"\"/>\r\n"
            else: row_html += "\t\t\t<td>"+str(row[j])+"</td>\r\n"

        row_html += "\t\t</tr>\r\n"
        return row_html

    html = "<div id=\""+div_id+"\"></div> \r\n" \
           "<table id=\""+table_id+"\"> \r\n"\
                "\t<thead> \r\n"\
                    "\t\t<tr> \r\n" +\
                        "\r\n".join(["\t\t\t<th>"+str(cln)+"</th>" for cln in df.columns]) +\
                    "\t\t</tr> \r\n"\
                "\t</thead> \r\n"\
                "\t<tbody id=\"tbody-"+tbody_id+"\"> \r\n" +\
                    "\r\n".join([generate_row(df.ix[idx]) for idx in df.index]) +\
                "\t</tbody> \r\n"\
            "</table> \r\n"

    js = "<script>$(function () {\r\n    /**\r\n     * Create a constructor for sparklines that takes some sensible defaults and merges in the individual\r\n     * chart options. This function is also available from the jQuery plugin as $(element).highcharts('SparkLine').\r\n     */\r\n    Highcharts.SparkLine = function (a, b, c) {\r\n        var hasRenderToArg = typeof a === 'string' || a.nodeName,\r\n            options = arguments[hasRenderToArg ? 1 : 0],\r\n            defaultOptions = {\r\n                chart: {\r\n                    renderTo: (options.chart && options.chart.renderTo) || this,\r\n                    backgroundColor: null,\r\n                    borderWidth: 0,\r\n                    type: 'area',\r\n                    margin: [2, 0, 2, 0],\r\n                    width: 120,\r\n                    height: 20,\r\n                    style: {\r\n                        overflow: 'visible'\r\n                    },\r\n                    skipClone: true\r\n                },\r\n                title: {\r\n                    text: ''\r\n                },\r\n                credits: {\r\n                    enabled: false\r\n                },\r\n                xAxis: {\r\n                    labels: {\r\n                        enabled: false\r\n                    },\r\n                    title: {\r\n                        text: null\r\n                    },\r\n                    startOnTick: false,\r\n                    endOnTick: false,\r\n                    tickPositions: []\r\n                },\r\n                yAxis: {\r\n                    endOnTick: false,\r\n                    startOnTick: false,\r\n                    labels: {\r\n                        enabled: false\r\n                    },\r\n                    title: {\r\n                        text: null\r\n                    },\r\n                    tickPositions: [0]\r\n                },\r\n                legend: {\r\n                    enabled: false\r\n                },\r\n                tooltip: {\r\n                    backgroundColor: null,\r\n                    borderWidth: 0,\r\n                    shadow: false,\r\n                    useHTML: true,\r\n                    hideDelay: 0,\r\n                    shared: true,\r\n                    padding: 0,\r\n                    positioner: function (w, h, point) {\r\n                        return { x: point.plotX - w / 2, y: point.plotY - h };\r\n                    }\r\n                },\r\n         exporting: {enabled: false}\r\n,       plotOptions: {\r\n \r\n                    series: {\r\n                        animation: false,\r\n                        lineWidth: 1,\r\n                        shadow: false,\r\n                        states: {\r\n                            hover: {\r\n                                lineWidth: 1\r\n                            }\r\n                        },\r\n                        marker: {\r\n                            radius: 1,\r\n                            states: {\r\n                                hover: {\r\n                                    radius: 2\r\n                                }\r\n                            }\r\n                        },\r\n                        fillOpacity: 0.25\r\n                    },\r\n                    column: {\r\n                        negativeColor: '#910000',\r\n                        borderColor: 'silver'\r\n                    }\r\n                }\r\n            };\r\n\r\n        options = Highcharts.merge(defaultOptions, options);\r\n\r\n        return hasRenderToArg ?\r\n            new Highcharts.Chart(a, options, c) :\r\n            new Highcharts.Chart(options, b);\r\n    };\r\n\r\n    var start = +new Date(),\r\n        $tds = $('td[data-sparkline]'),\r\n        fullLen = $tds.length,\r\n        n = 0;\r\n\r\n    // Creating 153 sparkline charts is quite fast in modern browsers, but IE8 and mobile\r\n    // can take some seconds, so we split the input into chunks and apply them in timeouts\r\n    // in order avoid locking up the browser process and allow interaction.\r\n    function doChunk() {\r\n        var time = +new Date(),\r\n            i,\r\n            len = $tds.length,\r\n            $td,\r\n            stringdata,\r\n            arr,\r\n            data,\r\n            chart;\r\n\r\n        for (i = 0; i < len; i += 1) {\r\n            $td = $($tds[i]);\r\n            stringdata = $td.data('sparkline');\r\n            arr = stringdata.split('; ');\r\n            data = $.map(arr[0].split(', '), parseFloat);\r\n            chart = {};\r\n\r\n            if (arr[1]) {\r\n                chart.type = arr[1];\r\n            }\r\n            $td.highcharts('SparkLine', {\r\n                series: [{\r\n                    data: data,\r\n                    pointStart: 1\r\n                }],\r\n                tooltip: {\r\n                    headerFormat: '<span style=\"font-size: 10px\">' + $td.parent().find('th').html() + ', P{point.x}:</span><br/>',\r\n                    pointFormat: '<b>{point.y}.000</b> USD'\r\n                },\r\n                chart: chart\r\n            });\r\n\r\n            n += 1;\r\n\r\n            // If the process takes too much time, run a timeout to allow interaction with the browser\r\n            if (new Date() - time > 500) {\r\n                $tds.splice(0, i + 1);\r\n                setTimeout(doChunk, 0);\r\n                break;\r\n            }\r\n\r\n            // Print a feedback on the performance\r\n            if (n === fullLen) {\r\n                $('#"+div_id+"').html('Generated ' + fullLen + ' sparklines in ' + (new Date() - start) + ' ms');\r\n            }\r\n        }\r\n    }\r\n    doChunk();\r\n\r\n});</script>"

    css = "<style type=\"text/css\"> \r\n" \
            "#"+div_id+" {  \r\n" \
                "\t text-align: right; \r\n" \
                "\t color: gray; \r\n" \
                "\t min-height: 2em; \r\n" \
            "} \r\n" \
            "#"+table_id+" { \r\n" \
                "\t margin: 0 auto; \r\n" \
                "\t border-collapse: collapse; \r\n" \
            "} \r\n" \
            "th { \r\n" \
                "\t font-weight: bold; \r\n" \
                "\t text-align: left; \r\n" \
            "} \r\n" \
            "td, th { \r\n" \
                "\t padding: 5px; \r\n" \
                "\t border-bottom: 1px solid silver; \r\n" \
                "\t height: 20px; \r\n" \
            "} \r\n" \
            "thead th { \r\n" \
                "\t border-top: 2px solid gray; \r\n" \
                "\t border-bottom: 2px solid gray; \r\n" \
            "} \r\n" \
            ".highcharts-tooltip>span { \r\n" \
                "\t background: white; \r\n" \
                "\t border: 1px solid silver; \r\n" \
                "\t border-radius: 3px; \r\n" \
                "\t box-shadow: 1px 1px 2px #888; \r\n" \
                "\t padding: 8px; \r\n" \
            "} \r\n" \
            "</style>"

    return css+html, js

def graph(dfs, id=None, title="title",subtitle="subtitle", width="740px",height="600px",
          decimal_pt_rounding_x=1,decimal_pt_rounding_y=1, shared_tooltip=True, ylabel='ylabel', xlabel='xlabel',series_names=[]):

    if id is None: id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))
    shared_tooltip = "" if not shared_tooltip else "tooltip: { shared: true }, \r\n"
    html = "<div id="+id+" style=\"width: "+width+"; height: "+height+"; margin: 0 auto\"></div> \r\n"
    if len(series_names) == 0:
        series_names = [df.columns[1] for df in dfs]
    def df2series_str(df):
        df = df.sort(df.columns[0])
        return ",".join([("["+str(round(x,decimal_pt_rounding_x))+","+str(round(y,decimal_pt_rounding_y))+"]") for (x,y) in zip(df[df.columns[0]],df[df.columns[1]])])

    data = ",".join([("{ name: '"+series_name+"', type: 'spline', data: [%s]}" % df2series_str(df)) for (df,series_name) in zip(dfs,series_names)])
    data = "series: ["+data+"]"
    js = "$(function () { \r\n"\
        "$('#"+id+"').highcharts({ \r\n"\
                "title: { text: '"+title+"' }, \r\n"\
                "subtitle: { text: '"+subtitle+"' }, \r\n"\
                "xAxis: { title: { text: '"+xlabel+"' } }, \r\n" +\
                "yAxis: { title: { text: '"+ylabel+"' } }, \r\n" +\
                shared_tooltip +\
                data + \
            "}); \r\n"\
        "}); \r\n"

    return html + "<script>"+js+"</script>"

def scatter(dfs, title="title",subtitle="subtitle", width="740px",height="600px",
            decimal_pt_rounding_x=1,decimal_pt_rounding_y=1,
            shared_tooltip=True,
            ylabel='ylabel', xlabel='xlabel',
            series_names=[],
            show_regression_line=[]):

    id = ''.join(random.choice(string.ascii_uppercase) for _ in range(12))

    shared_tooltip = "" if not shared_tooltip else "tooltip: { shared: true }, \r\n"
    html = "<div id="+id+" style=\"width: "+width+"; height: "+height+"; margin: 0 auto\"></div> \r\n"
    if len(series_names) == 0:
        series_names = [df.columns[1] for df in dfs]
    if len(show_regression_line) == 0:
        show_regression_line = [False]*len(dfs)
    def df2series_str(df):
        df = df.sort(df.columns[0])
        return ",".join([("["+str(round(x,decimal_pt_rounding_x))+","+str(round(y,decimal_pt_rounding_y))+"]") for (x,y) in zip(df[df.columns[0]],df[df.columns[1]])])

    reg_str = lambda show_reg: "" if not show_reg else "regression: true , regressionSettings: { type: 'linear' }, "

    data = ",".join([("{ "+reg_str(show_reg)+"  name: '"+series_name+"', type: 'scatter', data: [%s]}" % df2series_str(df)) for (df,series_name,show_reg) in zip(dfs,series_names,show_regression_line)])
    data = "series: ["+data+"]"
    js = "<script src=\"//rawgithub.com/phpepe/highcharts-regression/master/highcharts-regression.js\"> </script> \r\n"
    js += "<script> \r\n"
    js += "$(function () { \r\n"\
        "$('#"+id+"').highcharts({ \r\n"\
                "title: { text: '"+title+"' }, \r\n"\
                "subtitle: { text: '"+subtitle+"' }, \r\n"\
                "legend: { \r\n "\
                    "layout: 'vertical', \r\n "\
                    "align: 'left', \r\n "\
                    "verticalAlign: 'top', \r\n "\
                    "x: 100, \r\n "\
                    "y: 70, \r\n "\
                    "floating: true, \r\n "\
                    "backgroundColor: '#FFFFFF', \r\n "\
                    "borderWidth: 1 \r\n "\
                "}, \r\n "\
                "xAxis: { title: { text: '"+xlabel+"' } }, \r\n" +\
                "yAxis: { title: { text: '"+ylabel+"' } }, \r\n" +\
                shared_tooltip +\
                data + \
            "}); \r\n"\
        "}); \r\n"
    js += "</script>"

    return html + js






