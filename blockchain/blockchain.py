import hashlib

class Block(object):
	def __init__(self, data):
		self.prev_block = None
		self.prev_hash = None
		self.data = data

	def link(self, block):
		self.prev_block = block
		self.prev_hash = hashlib.sha256(block.data)

class BlockChain(object):
	def __init__(self):
		self.tail = Block('root')

	def append(self, data):
		block = Block(data)
		block.link(self.tail)
		self.tail = block

	def print_chain(self):
		block = self.tail
		res = ''
		while block is not None:
			res = (str(block.data) + '<--' + res)
			block = block.prev_block
		print(res)

	def verify(self):
		block = self.tail
		while block.prev_block is not None:
			if hashlib.sha256(block.prev_block.data).digest() != block.prev_hash.digest():
				return False
			block = block.prev_block
		return True


bc = BlockChain()
bc.append('paz')
bc.append('tal')
bc.append('paz3')

print(bc.verify())
