from viper_indexes import index_builder


import datetime
import time
import sys


HOUR_TO_START = 3
MINUTE_TO_START = 01

def run_in_loop(arg):
    def wait_till_tmrw():
        x=datetime.datetime.today()
        tmrw = (datetime.datetime.now() + datetime.timedelta(days=1))
        tmrw_3_am = datetime.datetime(tmrw.year, tmrw.month, tmrw.day, HOUR_TO_START,MINUTE_TO_START)
        secs = (tmrw_3_am-x).seconds
        print('waiting ' + str(secs) + " second(s) to perform daily tasks (tmrw)")
        time.sleep(secs)
    def wait_till_next_run_time():
        now=datetime.datetime.now()
        run_time = datetime.datetime(now.year, now.month, now.day, HOUR_TO_START,MINUTE_TO_START)
        if now < run_time:
            secs = (run_time-now).seconds
            print('waiting ' + str(secs) + " second(s) to perform daily tasks (today)")
            time.sleep(secs)
        else:
            wait_till_tmrw()
    if arg != 'now':
        wait_till_next_run_time()
    while True:
        now = datetime.datetime.now()
        if now.weekday() in [5,6]:
            print('['+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+'] Not running IndexBuilder on weekends')
            wait_till_tmrw()
            continue

        print('['+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+'] Running IndexBuilder')
        IndexBuilder = index_builder.IndexBuilder()
        IndexBuilder.build_all_indices()

        print('['+datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+'] IndexBuilder Running finished')
        wait_till_tmrw()

arg = sys.argv[1] if len(sys.argv) > 1 else None
run_in_loop(arg)