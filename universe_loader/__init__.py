__author__ = 'pgrimberg'
import datetime
from bbg_api import bwrapper
from viper_enums import LogEnum
import pandas as pd
import logger


class UniverseLoader(object):
    def __init__(self):
        pass

    def load_us_stocks(self, date_yyyymmdd=None):

        bbg = bwrapper()
        us1 = bbg.get_beqs('US_250M-1B', as_of_yyyymmdd=date_yyyymmdd)
        us2 = bbg.get_beqs('US_1B-10B', as_of_yyyymmdd=date_yyyymmdd)
        us3 = bbg.get_beqs('US_10B-1T', as_of_yyyymmdd=date_yyyymmdd)
        universe = []

        for (sec_id, fld2str_val) in us1.iteritems():
            if 'Ticker' in fld2str_val: universe.append(fld2str_val['Ticker'])
        for (sec_id, fld2str_val) in us2.iteritems():
            if 'Ticker' in fld2str_val: universe.append(fld2str_val['Ticker'])
        for (sec_id, fld2str_val) in us3.iteritems():
            if 'Ticker' in fld2str_val: universe.append(fld2str_val['Ticker'])

        print('Universe Size Is:')

        with open("universe_set.txt", "w") as output:
            output.write(str(set(list(universe))))

        return list(set(list(universe)))


    def search_mna_in_stocks(self, tickers):
        cols2return = ['Ticker', 'ActionID', 'Definiteness']
        bbg = bwrapper()
        # logger.log_to_UI('looking up M&As in ' + str(len(tickers)) + ' US stocks ',LogEnum.SYSTEM,'','',is_fatal=0)
        ticker2fld2value = bbg.get_refdata_fields(tickers, ['MERGERS_AND_ACQUISITIONS'], {})

        with open("ticker_2_field_value.txt", "w") as output:
            output.write(str(ticker2fld2value))
        rows = []
        for (t, f2v) in ticker2fld2value.iteritems():
            if 'ERROR' in f2v: continue
            deals = f2v['MERGERS_AND_ACQUISITIONS']
            for deal in deals:
                action_id = deal['Action Id']
                try:
                    if deal['Deal Type'].lower() != 'm&a': continue
                    if deal['Deal Status'].lower() == 'proposed':
                        rows.append([t, action_id + ' Action', 'proposed'])
                        continue
                    if deal['Deal Status'].lower() == 'pending' and deal['Payment Type'].lower() != 'undisclosed':
                        rows.append([t, action_id + ' Action', 'definite'])
                except:
                    print('failed parsing MERGERS_AND_ACQUISITIONS data for action id ' + action_id)
        df = pd.DataFrame(columns=cols2return, data=rows)
        # logger.log_to_UI('loaded ' + str(len(df[df['Definiteness']=='definite'])) + ' definite deals',LogEnum.SYSTEM,'','',is_fatal=0)
        # logger.log_to_UI('loaded ' + str(len(df[df['Definiteness']=='proposed'])) + ' proposed deals',LogEnum.SYSTEM,'','',is_fatal=0)
        #df.to_csv('ticker2FieldValue.csv')
        return df  # ['Ticker','ActionID','Definiteness']


    def query_mna_data(self, aid2definiteness):
        bbg = bwrapper()
        fld2code = {
            # region fld2code
            'CA052': 'Target Ticker',
            'CA054': 'Acquirer Ticker',
            'CA057': 'Announced Date',
            'CA058': 'Completion/Termination Date',
            'CA061': 'Deal Status',
            'CA060': 'Announced Total Value',
            'CA063': 'Announced Premium',
            # 'CA064':'Amendment Date',
            'CA065': 'Percent Owned',
            'CA066': 'Percent Sought',
            'CA067': 'Current Premium',
            'CA069': 'Cash Value',
            'CA071': 'Payment Type',
            'CA072': 'Cash Terms',
            'CA073': 'Stock Terms',
            # 'CA075':'Nature of Bid',
            # 'CA830':'Target Country',
            # 'CA831':'Acquirer Country',
            # 'CA834':'Transaction Type',
            'CA835': 'Expected Completion Date',
            'CA848': 'Deal Currency',
            'CA849': 'Gross Spread',
            # 'Contingency Payments':'CA855',
            # 'CA857':'Exchange',
            # 'CA883':'Pending Approvals',
            # 'CA884':'Expired Approvals',
            # 'CA885':'Approved Approvals',
            # 'CA886':'Extended Approvals',
            # 'CA887':'Blocked Approvals',
            # 'CA888':'Early Termination Approvals',
            # 'CA917':'Current Premium 1 Day Change',
            # 'CA918':'Current Premium 5 Day Change',
            # 'CA919':'Current Premium 10 Day Change',
            # 'Contingency Payments Amount':'CA924',
            # 'CA932':'Mergers Agreement Date',
            'CA944': 'Proposed Date',
            # 'CA956':'Acquirer Ownership in New Company Percent',
            # endregion
        }
        ## querying action ids
        api_calls = len(fld2code.keys()) * len(aid2definiteness)

        logger.log_to_UI('querying ' + str(len(aid2definiteness)) + ' pending deals for ' + str(
            len(fld2code)) + ' deal parameters [' + str(api_calls) + ' api calls]', LogEnum.SYSTEM, '', '', is_fatal=0)
        action_ids = aid2definiteness.keys()
        aid2fld2value = bbg.get_refdata_fields(action_ids, list(fld2code.keys()), {})
        aid2fld2value = {d: {fld2code[c]: v for (c, v) in f2v.iteritems() if c != 'ERROR'} for (d, f2v) in
                         aid2fld2value.iteritems()}

        with open("aid2fld2value.txt","w") as output:
            output.write(str(aid2fld2value))

        data = []
        for (d, f2v) in aid2fld2value.iteritems():
            if pd.isnull(f2v['Target Ticker']): continue
            if aid2definiteness[d] == 'definite' and (
                    pd.isnull(f2v['Payment Type']) or pd.isnull(f2v['Announced Date'])): continue
            if aid2definiteness[d] == 'proposed' and pd.isnull(['Proposed Date']): continue

            data.append([
                d,
                f2v['Announced Date'],
                f2v['Proposed Date'],
                f2v['Deal Status'],
                f2v['Target Ticker'],
                f2v['Acquirer Ticker'],
                f2v['Cash Terms'],
                f2v['Stock Terms'],
                f2v['Cash Value'],
                f2v['Current Premium'],
                f2v['Gross Spread'],
                f2v['Announced Premium'],
                f2v['Percent Owned'],
                f2v['Percent Sought'],
                f2v['Completion/Termination Date'],
                f2v['Expected Completion Date'],
                f2v['Payment Type'],
                f2v['Announced Total Value'],
                f2v['Deal Currency']
            ])
        deal_df = pd.DataFrame(columns=['Action Id', 'Announced Date', 'Proposed Date', 'Deal Status',
                                        'Target Ticker', 'Acquirer Ticker', 'Cash Terms',
                                        'Stock Terms', 'Cash Value', 'Current Premium', 'Gross Spread',
                                        'Announced Premium', 'Percent Owned', 'Percent Sought',
                                        'Completion/Termination Date', 'Expected Completion Date',
                                        'Payment Type', 'Announced Total Value', 'Deal Currency'], data=data)
        deal_df.to_csv('aid2fldvalue.csv')
        return deal_df


    def filter_for_public_targets(self, deal_df):
        bbg = bwrapper()
        tgt_tickers = list(set([t + ' Equity' for t in deal_df['Target Ticker']]))
        api_calls = len(tgt_tickers)
        logger.log_to_UI('querying ' + str(api_calls) + ' (filtered) target tickers for their public status [' + str(
            api_calls) + ' api calls]'
                         , LogEnum.SYSTEM, '', '', is_fatal=0)
        ticker2fld2value = bbg.get_refdata_fields(tgt_tickers, ['COMPANY_IS_PRIVATE'], {})
        deal_df['IsTargetPublic'] = deal_df['Target Ticker'].apply(
            lambda x: ticker2fld2value[x + ' Equity']['COMPANY_IS_PRIVATE'] == 'N')
        deal_df = deal_df[deal_df['IsTargetPublic']].copy()
        return deal_df


    def query_acquirer_currency(self, deal_df):
        bbg = bwrapper()
        if 'Stock/sh' not in deal_df.columns: raise ("must be called after parse_deal_terms()")
        acq_tickers = [(t + ' Equity') for t in list(deal_df[~pd.isnull(deal_df['Stock/sh'])]['Acquirer Ticker'].unique())]
        api_calls = len(acq_tickers)
        logger.log_to_UI('querying ' + str(len(acq_tickers)) + ' (filtered) acquirer tickers for their ccy [' + str(
            api_calls) + ' api calls]', LogEnum.SYSTEM, '', '', is_fatal=0)
        ticker2fld2value = bbg.get_refdata_fields(acq_tickers, ['CRNCY'], {})

        def parse_ccy(acq_ticker):
            try:
                return ticker2fld2value[acq_ticker + ' Equity']['CRNCY']
            except:
                return None

        deal_df['AcquirerCCY'] = deal_df['Acquirer Ticker'].apply(lambda x: parse_ccy(x))
        return deal_df


    def query_ISINs(self, deal_df):
        bbg = bwrapper()
        acq_tickers = [(t + ' Equity') for t in
                       list(deal_df[~pd.isnull(deal_df['Acquirer Ticker'])]['Acquirer Ticker'].unique()) if
                       len(t.split(' ')) == 2]
        tgt_tickers = [(t + ' Equity') for t in deal_df['Target Ticker'].unique()]
        tickers = list(set(acq_tickers + tgt_tickers))
        api_calls = len(tickers)
        logger.log_to_UI('querying ' + str(len(tickers)) + ' tickers for their ISIN [' + str(api_calls) + ' api calls]',
                         LogEnum.SYSTEM, '', '', is_fatal=0)
        ticker2fld2value = bbg.get_refdata_fields(tickers, ['ID_ISIN'], {})

        def parse_ISIN(ticker):
            try:
                return ticker2fld2value[ticker + ' Equity']['ID_ISIN']
            except:
                return None

        deal_df['Target ISIN'] = deal_df['Target Ticker'].apply(lambda x: parse_ISIN(x))
        deal_df['Acquirer ISIN'] = deal_df['Acquirer Ticker'].apply(lambda x: parse_ISIN(x))
        return deal_df


    def query_tgtmktcap(self, deal_df):
        bbg = bwrapper()
        tgt_tickers = [x + ' Equity' for x in deal_df['Target Ticker'].unique()]
        logger.log_to_UI('querying ' + str(len(tgt_tickers)) + ' target tickers for CRNCY_ADJ_MKT_CAP', LogEnum.SYSTEM, '',
                         '', is_fatal=0)

        tkr2fld2value = bbg.get_refdata_fields(tgt_tickers, ["CRNCY_ADJ_MKT_CAP"], {'EQY_FUND_CRNCY': 'USD'})
        deal_df['TgtMktCap($)'] = deal_df['Target Ticker'].apply(
            lambda x: tkr2fld2value[x + ' Equity']['CRNCY_ADJ_MKT_CAP']).astype(float)

        return deal_df


    def query_termfees(self, deal_df):
        bbg = bwrapper()
        action_ids = list(deal_df['Action Id'])

        logger.log_to_UI('querying ' + str(len(action_ids)) + ' action ids for CA866 (T2A TermFee) and CA867 (A2T TermFee)',
                         LogEnum.SYSTEM, '', '', is_fatal=0)
        actionid2fld2value = bbg.get_refdata_fields(action_ids, ["CA866", "CA867"], {})
        # 'CA866', 'Target to Acquirer Termination Fees'
        # 'CA867', 'Acquirer to Target Termination Fees'

        deal_df['T2A Term Fee($mln)'] = deal_df['Action Id'].apply(lambda x: actionid2fld2value[x]['CA866']).astype(
            float)
        deal_df['A2T Term Fee($mln)'] = deal_df['Action Id'].apply(lambda x: actionid2fld2value[x]['CA867']).astype(
            float)
        deal_df['Term Fee as % of ATV'] = 1e2 * (
                (deal_df['T2A Term Fee($mln)'] + deal_df['A2T Term Fee($mln)']) / deal_df['Announced Total Value'].astype(
            float))

        return deal_df


    def parse_deal_terms(self, deal_df):
        def parse_cash_terms(cash_terms, stock_terms):
            try:
                if pd.isnull(cash_terms):
                    if pd.isnull(stock_terms): return None
                    if stock_terms.startswith('USD'): return float((stock_terms.split('/Tgt sh.')[0]).split(' ')[1])
                    return None
                if '/sh.' in cash_terms:
                    cash_per_share = float(cash_terms.split('/sh.')[0])
                    if not pd.isnull(stock_terms) and stock_terms.startswith('USD'):
                        cash_per_share += float((stock_terms.split('/Tgt sh.')[0]).split(' ')[1])
                    return cash_per_share
            except:
                return None

        def parse_stock_terms(stock_terms):
            try:
                if stock_terms is None: return None
                if 'Aqr sh./Tgt sh.' in stock_terms:
                    stock_per_share = float(stock_terms.split(' Aqr sh./Tgt sh.')[0])
                    return stock_per_share
            except:
                return None

        deal_df['Cash/sh'] = [parse_cash_terms(c, s) for (c, s) in zip(deal_df['Cash Terms'], deal_df['Stock Terms'])]
        deal_df['Stock/sh'] = deal_df['Stock Terms'].apply(lambda x: parse_stock_terms(x))
        return deal_df


    def load_mna_universe_from_tickers(self, tickers):
        df = self.search_mna_in_stocks(tickers)  # ['Ticker','ActionID','Definiteness']
        aid2definiteness = {row['ActionID']: row['Definiteness'] for (idx, row) in df.iterrows()}
        deal_df = self.query_mna_data(aid2definiteness)
        deal_df = self.filter_for_public_targets(deal_df)  # adds 'IsTargetPublic'
        deal_df = self.parse_deal_terms(deal_df)  # adds ['Cash/sh','Stock/sh']
        deal_df = self.query_acquirer_currency(deal_df)  # adds 'AcquirerCCY'
        deal_df = self.query_ISINs(deal_df)  # adds ['Target ISIN','Acquirer ISIN']
        deal_df = self.query_tgtmktcap(deal_df)  # adds TgtMktCap($), todo: we have it from BEQS, maybe save apis?
        deal_df = self.query_termfees(deal_df)  # adds ['T2A Term Fee($mln)','A2T Term Fee($mln)','Term Fee as % of ATV']

        # deal_df =
        # ['Action Id','Announced Date','Proposed Date', 'Deal Status',
        # 'Target Ticker', 'Acquirer Ticker', 'Cash Terms',
        # 'Stock Terms','Cash Value', 'Current Premium', 'Gross Spread',
        # 'Announced Premium', 'Percent Owned','Percent Sought',
        # 'Completion/Termination Date', 'Expected Completion Date',
        # 'Payment Type', 'Announced Total Value','Deal Currency',
        # 'IsTargetPublic', 'Cash/sh','Stock/sh', 'AcquirerCCY',
        # 'Target ISIN','Acquirer ISIN',
        # 'TgtMktCap($)', 'T2A Term Fee($mln)','A2T Term Fee($mln)','Term Fee as % of ATV'
        return deal_df


    def load_mna_universe_US(self, date_yyyymmdd):

        tickers = self.load_us_stocks(date_yyyymmdd)
        tickers = [t + " Equity" for t in tickers]
        mna_US_df = self.load_mna_universe_from_tickers(tickers)
        return mna_US_df


    def load_mna_universe(self, date_yyyymmdd=None):
        # dbutils.wic.delete_table('portfolio_universe')
        now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        us_mna_df = self.load_mna_universe_US(date_yyyymmdd)
        us_mna_df['DateLoaded'] = now

        def calc_pct_proforma(pct_owned, pct_sought):
            try:
                if pd.isnull(pct_owned): pct_owned = 0
                if pd.isnull(pct_sought): pct_sought = 0
                return float(pct_owned) + float(pct_sought)
            except:
                return 0

        us_mna_df['PercentProForma'] = [calc_pct_proforma(o, s) for (o, s) in
                                        zip(us_mna_df['Percent Owned'], us_mna_df['Percent Sought'])]

        univ_cols = ['DateLoaded', 'Action Id', 'Announced Date', 'Proposed Date', 'Deal Status',
                     'Target Ticker', 'Acquirer Ticker', 'Cash Terms',
                     'Stock Terms', 'Cash Value', 'Current Premium', 'Gross Spread',
                     'Announced Premium',
                     'Percent Owned', 'Percent Sought', 'PercentProForma',
                     'Completion/Termination Date', 'Expected Completion Date',
                     'Payment Type', 'Announced Total Value', 'Deal Currency',
                     'IsTargetPublic', 'Cash/sh', 'Stock/sh', 'AcquirerCCY',
                     'Target ISIN', 'Acquirer ISIN',
                     'TgtMktCap($)', 'T2A Term Fee($mln)', 'A2T Term Fee($mln)', 'Term Fee as % of ATV']
        float_cols = ['Cash Value', 'Current Premium', 'Gross Spread', 'Announced Premium', 'Percent Owned',
                      'Percent Sought',
                      'Announced Total Value', 'Cash/sh', 'Stock/sh', 'TgtMktCap($)', 'T2A Term Fee($mln)',
                      'A2T Term Fee($mln)', 'Term Fee as % of ATV']

        univ_df = us_mna_df[univ_cols].copy()
        univ_df[float_cols] = univ_df[float_cols].astype(float)
        univ_df.to_csv("c:\\1\\us_univ_df.csv")
        us_mna_df.to_csv("c:\\1\us_mna_df.csv")
        # if store_to_db:
        # 	viper_dbutils.wic.bulk_insert(univ_df, 'viper_universe')
        logger.log_to_UI('universe loaded  ', LogEnum.SYSTEM, '', '', is_fatal=0)
        print(us_mna_df)
        return us_mna_df, univ_df
