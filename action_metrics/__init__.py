__author__ = 'pgrimberg'
import pandas as pd
import datetime
import logger
from bbg_api import bwrapper
from statsmodels.formula.api import OLS
from viper_enums import LogEnum
from universe_loader import UniverseLoader
import pytz
import viper_dbutils

from scipy import stats
class ActionMetrics(object):
    def __init__(self):
        pass

    @staticmethod
    def query_tickers_histdata_df(tickers, mnemonics, start_dt_yyyymmdd, end_dt_yyyymmdd, ccy=None):
        bbg = bwrapper()
        tkr2fld2ts = bbg.get_histdata_fields(sec_ids=tickers,
                                             fields=mnemonics, overrides_dict={},
                                             start_date_yyyymmdd=start_dt_yyyymmdd,
                                             end_date_yyyymmdd=end_dt_yyyymmdd,
                                             currency=ccy,
                                             float_fields=mnemonics, periodicitySelection='DAILY',
                                             non_trading_day_fill_option='NON_TRADING_WEEKDAYS',
                                             non_trading_day_fill_method='PREVIOUS_VALUE')
        rowz = []
        for tkr in tickers:
            for mn in mnemonics:
                ts = tkr2fld2ts[tkr][mn]
                if len(ts) == 0: continue
                for (idx, row) in ts.reset_index().iterrows():
                    rowz.append((row['index'], tkr, mn, None if pd.isnull(row[0]) else row[0]))
        df = pd.DataFrame(columns=['Date', 'Ticker', 'Mnemonic', 'Value'], data=rowz)
        return df

    @staticmethod
    def query_tickers_refdata_df(full_tickers, mnemonics):
        bbg = bwrapper()
        tkr2fld2value = bbg.get_refdata_fields(list(set(full_tickers)), mnemonics, {})
        return pd.DataFrame(columns=['Ticker'] + mnemonics,
                            data=[([d] + [f2v[fname] for fname in mnemonics]) for (d, f2v) in
                                  tkr2fld2value.iteritems()])

    @staticmethod
    def query_deal_data(action_ids, fcode2name):
        action_ids = list(set(action_ids))
        bbg = bwrapper()

        ## querying action ids

        aid2fld2value = bbg.get_refdata_fields(action_ids, list(fcode2name.keys()), {})
        aid2fld2value = {d: {fcode2name[c]: v for (c, v) in f2v.iteritems() if c != 'ERROR'} for (d, f2v) in
                         aid2fld2value.iteritems()}

        fld_names = fcode2name.values()
        deal_df_cols = ['ActionID'] + fld_names
        deal_df_rows = [([d] + [f2v[fname] for fname in fld_names]) for (d, f2v) in aid2fld2value.iteritems()]
        deal_df = pd.DataFrame(columns=deal_df_cols, data=deal_df_rows)

        return deal_df

    @staticmethod
    def query_deal_status(action_ids):
        action_ids = list(set(action_ids))
        fcode2name = {'CA061': 'Deal Status'}
        logger.log_to_UI('querying ' + str(len(action_ids)) + ' deals for their deal status', LogEnum.BBG_API, '', '',
                         is_fatal=0)
        deal_df = ActionMetrics.query_deal_data(action_ids, fcode2name)
        deal_df['Deal Status'] = deal_df['Deal Status'].str.lower()
        deal_df = deal_df.rename(columns={'Deal Status': 'Curr Deal Status'})
        return deal_df[['ActionID', 'Curr Deal Status']].copy()

    @staticmethod
    def query_deal_consideration(action_ids):
        action_ids = list(set(action_ids))
        fcode2name = {'CA072': 'Cash Terms', 'CA073': 'Stock Terms'}
        logger.log_to_UI('querying ' + str(len(action_ids)) + ' deals for their cash terms and stock terms',
                         LogEnum.BBG_API, '', '', is_fatal=0)
        deal_df = ActionMetrics.query_deal_data(action_ids, fcode2name)

        ul = UniverseLoader()
        deal_df = ul.parse_deal_terms(deal_df)  # adds ['Cash/sh','Stock/sh']
        deal_df = deal_df.rename(columns={'Cash/sh': 'Curr Cash/sh', 'Stock/sh': 'Curr Stock/sh'})
        deal_df[['Curr Cash/sh', 'Curr Stock/sh']] = deal_df[['Curr Cash/sh', 'Curr Stock/sh']].astype(float)
        return deal_df[['ActionID', 'Curr Cash/sh', 'Curr Stock/sh']].copy()

    @staticmethod
    def calc_volume_spike(dt, full_ticker):
        bbg = bwrapper()
        gmt = pytz.timezone('GMT')
        eastern = pytz.timezone('US/Eastern')

        start_dt_gmt = (eastern.localize(dt - datetime.timedelta(minutes=120))).astimezone(gmt)
        end_dt_gmt = (eastern.localize(dt)).astimezone(gmt)
        mean_dt_gmt = start_dt_gmt + (end_dt_gmt - start_dt_gmt) / 2

        start_dt = start_dt_gmt.replace(tzinfo=None)
        mean_dt = mean_dt_gmt.replace(tzinfo=None)
        end_dt = end_dt_gmt.replace(tzinfo=None)

        trades = bbg.get_refdata_intraday_tick(full_ticker, start_dt_gmt, end_dt_gmt)

        volume_first_half = sum([t[1] for t in trades if t[0] >= start_dt and t[0] <= mean_dt])
        volume_second_half = sum([t[1] for t in trades if t[0] >= mean_dt and t[0] <= end_dt])
        if volume_first_half == 0: return 0  # if nothing traded in the past hour, don't buy this.
        vol_spike_pct = 100.0 * ((volume_second_half / float(volume_first_half)) - 1.0)

        return vol_spike_pct

    # todo: warning, EQY_WEIGHTED_AVG_PX might work for past 3 days, but doesnt work further than this - check this
    # ADDS ACQ_PREV_3D_VWAP, TGT_PREV_3D_VWAP
    @staticmethod
    def calc_last_3d_vwap(univ_df):  # ADDS ACQ_PREV_3D_VWAP, TGT_PREV_3D_VWAP
        tgt_tickers = [x + ' Equity' for x in univ_df['Target Ticker'].unique()]
        acq_tickers = [x + ' Equity' for x in univ_df[~pd.isnull(univ_df['Stock/sh'])]['Acquirer Ticker'].unique()]
        tickers = list(set(tgt_tickers + acq_tickers))
        now = datetime.datetime.now()
        end_yyyymmdd = (now - datetime.timedelta(days=1)).strftime('%Y%m%d')
        start_yyyymmdd = (now - datetime.timedelta(days=4)).strftime('%Y%m%d')

        bbg = bwrapper()
        tkr2vwap = bbg.get_refdata_fields(tickers, ['EQY_WEIGHTED_AVG_PX'],
                                          {'VWAP_START_DT': start_yyyymmdd, 'VWAP_END_DT': end_yyyymmdd})

        def parse_vwap(ticker):
            try:
                res = float(tkr2vwap[ticker + ' Equity']['EQY_WEIGHTED_AVG_PX'])
                return res
            except:
                return None

        res = univ_df[['Action Id', 'Target Ticker', 'Acquirer Ticker']].copy()
        res['ACQ_PREV_3D_VWAP'] = res['Acquirer Ticker'].apply(lambda x: parse_vwap(x))
        res['TGT_PREV_3D_VWAP'] = res['Target Ticker'].apply(lambda x: parse_vwap(x))
        return res[['Action Id', 'ACQ_PREV_3D_VWAP', 'TGT_PREV_3D_VWAP']].copy()

    @staticmethod
    def calc_mktcap(full_ticker):
        bbg = bwrapper()
        tkr2fld2value = bbg.get_refdata_fields([full_ticker], ["CRNCY_ADJ_MKT_CAP"], {'EQY_FUND_CRNCY': 'USD'})
        res = float(tkr2fld2value[full_ticker]['CRNCY_ADJ_MKT_CAP'])
        return res

    @staticmethod
    def calc_ISIN(full_ticker):
        bbg = bwrapper()
        tkr2fld2value = bbg.get_refdata_fields([full_ticker], ['ID_ISIN'], {})
        res = tkr2fld2value[full_ticker]['ID_ISIN']
        return res

    @staticmethod
    def calc_acq_mktcap(univ_df):
        bbg = bwrapper()

        acq_tickers = [x + ' Equity' for x in univ_df[~pd.isnull(univ_df['Stock/sh'])]['Acquirer Ticker'].unique()]
        acq_tickers = list(set(acq_tickers))

        tkr2fld2value = bbg.get_refdata_fields(acq_tickers, ["CRNCY_ADJ_MKT_CAP"], {'EQY_FUND_CRNCY': 'USD'})

        def parse_mktcap(ticker):
            try:
                res = float(tkr2fld2value[ticker + ' Equity']['CRNCY_ADJ_MKT_CAP'])
                return res
            except:
                return None

        res = univ_df[['Action Id', 'Acquirer Ticker']].copy()
        res['ACQ_MKTCAP($)'] = res['Acquirer Ticker'].apply(lambda x: parse_mktcap(x))
        return res[['Action Id', 'ACQ_MKTCAP($)']].copy()

    @staticmethod
    def get_index_overnight_chg_df(idx_ticker='SPX Index'):
        bbg = bwrapper()
        membs = bbg.get_refdata_fields([idx_ticker], ['INDX_MWEIGHT_HIST'], {})[idx_ticker][
            'INDX_MWEIGHT_HIST']  # {"END_DATE_OVERRIDE":"20000101"}
        tickers = [x['Index Member'].split(' ')[0] + ' US Equity' for x in membs]
        now = datetime.datetime.now()
        mnemonics = ['PX_CLOSE_1D', 'PX_OPEN', 'PX_LAST', 'PX_CLOSE_DT', 'CUR_MKT_CAP', 'ID_ISIN']
        tkr2mnemonics = bbg.get_refdata_fields(tickers, mnemonics, {})
        rows = [([t] + [tkr2mnemonics[t][m] for m in mnemonics]) for t in tickers]
        df = pd.DataFrame(columns=['Ticker'] + mnemonics, data=rows)
        float_cols = ['PX_CLOSE_1D', 'PX_OPEN', 'PX_LAST', 'CUR_MKT_CAP']
        df[float_cols] = df[float_cols].astype(float)
        df['PX_CLOSE_DT'] = df['PX_CLOSE_DT'].apply(lambda x: pd.to_datetime(x))
        df['DaysSinceLastClose'] = df['PX_CLOSE_DT'].apply(lambda x: (now - x).days)
        df['CUR_MKT_CAP($bln)'] = df['CUR_MKT_CAP'].apply(lambda x: x / 1e9)
        df['OvernightReturn(%)'] = 100.0 * ((df['PX_OPEN'] / df['PX_CLOSE_1D']) - 1.0)
        return df[['Ticker', 'ID_ISIN', 'PX_CLOSE_1D', 'PX_OPEN', 'PX_LAST', 'DaysSinceLastClose', 'CUR_MKT_CAP($bln)',
                   'OvernightReturn(%)']].copy()

    @staticmethod
    def calc_sector_index_ticker(full_ticker_or_ISIN):
        if pd.isnull(full_ticker_or_ISIN): return None
        gsn2index = {
            'Banks': 'S5BANKX Index',
            'Retailing': 'S5RETL Index',
            'Media': 'S5MEDA Index',
            'Software & Services': 'S5SFTW Index',
            'Automobiles & Components': 'S5AUCO Index',
            'Technology Hardware & Equipment': 'S5TECH Index',
            'Capital Goods': 'S5CPGS Index',
            'Insurance': 'S5INSU Index',
            'Real Estate': 'S5REAL Index',
            'Transportation': 'S5TRAN Index',
            'Energy': 'S5ENRSX Index',
            'Consumer Durables & Apparel': 'S5CODU Index',
            'Food Beverage & Tobacco': 'S5FDBT Index',
            'Telecommunication Services': 'S5TELSX Index',
            'Consumer Services': 'S5HOTR Index',
            'Food & Staples Retailing': 'S5FDSR Index',
            'Household & Personal Products': 'S5HOUS Index',
            'Diversified Financials': 'S5DIVF Index',
            'Utilities': 'S5UTILX Index',
            'Pharmaceuticals, Biotechnology': 'S5PHRM Index',
            'Health Care Equipment & Services': 'S5HCES Index',
            'Commercial Professional Services': 'S5COMS Index',
            'Materials': 'S5MATRX Index',
            'Semiconductors & Semiconductor':'S5SSEQX Index',
            'Health Care Equipment & Servic':'S5HCES Index',
            'Commercial & Professional Serv': 'S5COMS Index',
            'Technology Hardware & Equipmen':'S5TECH Index',
            'Food, Beverage & Tobacco':'S5FDBT Index',

        }
        gics_sector_name = \
        ActionMetrics.query_tickers_refdata_df([full_ticker_or_ISIN], ['GICS_INDUSTRY_GROUP_NAME'])['GICS_INDUSTRY_GROUP_NAME'].iloc[0] #todo GICS Industry name

        if gics_sector_name not in gsn2index:
            print('Not found in Cal_Sector_Index_ticker: ' + str(gics_sector_name))
            if full_ticker_or_ISIN == 'BMG9319H1025 Equity': #VR US Equity
                return gsn2index['Insurance']

            elif full_ticker_or_ISIN == 'US2035071081':
                return gsn2index['Banks']
            print(full_ticker_or_ISIN)
            return None

        return gsn2index[gics_sector_name]

    @staticmethod
    def calc_return(full_ticker_or_ISIN, start_yyyymmdd, end_yyyymmdd):
        px_df = ActionMetrics.query_tickers_histdata_df([full_ticker_or_ISIN], ['PX_LAST'], start_yyyymmdd,
                                                        end_yyyymmdd)
        if len(px_df) == 0: return None
        #res = 100.0 * ((px_df['Value'].iloc[-1] / px_df['Value'].iloc[0]) - 1.0)
        res = 100* ((px_df['Value'].iloc[-1]/px_df['Value'].iloc[:60].mean()) - 1.0)
        return res

    @staticmethod
    def calc_betas(full_ISIN_or_ticker, start_yyyymmdd, end_yyyymmdd, spx_vix_weekly_df):
        if None in [full_ISIN_or_ticker, start_yyyymmdd, end_yyyymmdd]: return None, None, None
        bbg = bwrapper()
        ISIN2px_last = bbg.get_histdata_fields(sec_ids=[full_ISIN_or_ticker],
                                               fields=['PX_LAST'], overrides_dict={},
                                               start_date_yyyymmdd=start_yyyymmdd,
                                               end_date_yyyymmdd=end_yyyymmdd,
                                               float_fields=['PX_LAST'], periodicitySelection='WEEKLY',
                                               non_trading_day_fill_option='NON_TRADING_WEEKDAYS',
                                               non_trading_day_fill_method='PREVIOUS_VALUE'
                                               )

        ISIN_df = (ISIN2px_last[full_ISIN_or_ticker]['PX_LAST']).reset_index().rename(
            columns={'index': 'Date', 0: 'PX_LAST'}).sort_values(by='Date')
        ISIN_df['RET_WK(%)'] = 100.0 * ((ISIN_df['PX_LAST'] / ISIN_df['PX_LAST'].shift(1)) - 1.0)

        ols_df = pd.merge(spx_vix_weekly_df[['Date', 'SPX_RET_WK(%)']], ISIN_df[['Date', 'RET_WK(%)']], how='inner',
                          on='Date').dropna()


        beta_to_sp500=None
            #,intercept,r_value,p_value,std_err = stats.linregress(ols_df['SPX_RET_WK(%)'],ols_df['RET_WK(%)']) if len(ols_df) >= 15 else None

        stress_sp500_rets = spx_vix_weekly_df[spx_vix_weekly_df['VIX_RET_WK(%)'] > 15][['Date', 'SPX_RET_WK(%)']]
        ols_df = pd.merge(stress_sp500_rets, ISIN_df[['Date', 'RET_WK(%)']], how='inner', on='Date').dropna()

        stress_beta_to_sp500 = None
            #,int_,r_vl,p_vl,std_e = stats.linregress(ols_df['SPX_RET_WK(%)'],ols_df['RET_WK(%)']) if len(ols_df) >= 15 else None

        industry_beta = None

        # industry_index_ticker = bbg.get_refdata_fields([full_ISIN_or_ticker],['INDUSTRY_GROUP_INDEX'],{})[full_ISIN_or_ticker]['INDUSTRY_GROUP_INDEX']
        sector_index_tkr = ActionMetrics.calc_sector_index_ticker(full_ISIN_or_ticker)
        if not pd.isnull(sector_index_tkr):
            ind_idx2px_last = bbg.get_histdata_fields(sec_ids=[sector_index_tkr],
                                                      fields=['PX_LAST'], overrides_dict={},
                                                      start_date_yyyymmdd=start_yyyymmdd,
                                                      end_date_yyyymmdd=end_yyyymmdd,
                                                      float_fields=['PX_LAST'], periodicitySelection='WEEKLY',
                                                      non_trading_day_fill_option='NON_TRADING_WEEKDAYS',
                                                      non_trading_day_fill_method='PREVIOUS_VALUE')
            industry_df = (ind_idx2px_last[sector_index_tkr]['PX_LAST']).reset_index().rename(
                columns={'index': 'Date', 0: 'PX_LAST'}).sort_values(by='Date')
            industry_df['INDUSTRY_RET_WK(%)'] = 100.0 * (
                        (industry_df['PX_LAST'] / industry_df['PX_LAST'].shift(1)) - 1.0)
            ols_df = pd.merge(industry_df[['Date', 'INDUSTRY_RET_WK(%)']], ISIN_df[['Date', 'RET_WK(%)']], how='inner',
                              on='Date').dropna()
            industry_beta=None
                #,v,x,e,w = stats.linregress(ols_df['INDUSTRY_RET_WK(%)'],ols_df['RET_WK(%)']) if len(                ols_df) >= 15 else None

        return industry_beta, beta_to_sp500, stress_beta_to_sp500

    @staticmethod
    def get_spx_vix_df(start_yyyymmdd, end_yyyymmdd):
        bbg = bwrapper()
        tkr2px_last = bbg.get_histdata_fields(
            sec_ids=['SPXT Index', 'VIX Index'],
            fields=['PX_LAST'],
            overrides_dict={},
            start_date_yyyymmdd=start_yyyymmdd,
            end_date_yyyymmdd=end_yyyymmdd,
            float_fields=['PX_LAST'],
            periodicitySelection='WEEKLY',
            non_trading_day_fill_option='NON_TRADING_WEEKDAYS',
            non_trading_day_fill_method='PREVIOUS_VALUE'
        )
        spx_df = (tkr2px_last['SPXT Index']['PX_LAST']).reset_index().rename(
            columns={'index': 'Date', 0: 'SPX_PX'}).sort_values(by='Date')
        spx_df['SPX_RET_WK(%)'] = 100.0 * ((spx_df['SPX_PX'] / spx_df['SPX_PX'].shift(1)) - 1.0)
        vix_df = (tkr2px_last['VIX Index']['PX_LAST']).reset_index().rename(
            columns={'index': 'Date', 0: 'VIX_PX'}).sort_values(by='Date')
        vix_df['VIX_RET_WK(%)'] = 100.0 * ((vix_df['VIX_PX'] / vix_df['VIX_PX'].shift(1)) - 1.0)
        df = pd.merge(spx_df.dropna(), vix_df, how='inner',
                      on='Date')  # 'Date', SPX_PX, 'SPX_RET_WK(%)', 'VIX_PX', 'VIX_RET_WK(%)
        return df  # 'Date', SPX_PX, 'SPX_RET_WK(%)', 'VIX_PX', 'VIX_RET_WK(%)

    # this loads SOD metrics for univ_df, and also updates the database table viper_universe_sod_metrics
    # to hold the latest calculation of SOD metrics.
    @staticmethod
    def load_and_store_universe_SOD_metrics(univ_df, real_time):
        if not real_time:
            return pd.read_csv("c:\\1\\univ_metrics_df.csv")

        logger.log_to_UI('loading ACQ and TGT last 3D VWAP', LogEnum.SYSTEM, '', '', is_fatal=0)
        # 'Action Id','ACQ_PREV_3D_VWAP','TGT_PREV_3D_VWAP'
        vwap_df = ActionMetrics.calc_last_3d_vwap(univ_df)

        logger.log_to_UI('loading TGT_BETA, TGT_STRESS_BETA, TGT_DOWNSIDE, ACQ_BETA, ACQ_STRESS_BETA, ACQ_DOWNSIDE',
                         LogEnum.SYSTEM, '', '', is_fatal=0)
        # 'Action Id','TGT_BETA','TGT_STRESS_BETA','TGT_DOWNSIDE','ACQ_BETA','ACQ_STRESS_BETA','ACQ_DOWNSIDE'

        ####TODO: calc this
        beta_df = univ_df[['Action Id', 'Target Ticker', 'Acquirer Ticker']].copy()
        beta_df['TGT_BETA'] = None
        beta_df['TGT_STRESS_BETA'] = None
        beta_df['TGT_DOWNSIDE'] = None
        beta_df['ACQ_BETA'] = None
        beta_df['ACQ_STRESS_BETA'] = None
        beta_df['ACQ_DOWNSIDE'] = None
        beta_df = beta_df[['Action Id', 'TGT_BETA', 'TGT_STRESS_BETA', 'TGT_DOWNSIDE', 'ACQ_BETA', 'ACQ_STRESS_BETA',
                           'ACQ_DOWNSIDE']].copy()
        #####

        logger.log_to_UI('loading ACQ_MKTCAP($)', LogEnum.SYSTEM, '', '', is_fatal=0)
        # 'Action Id`','ACQ_MKTCAP($)'
        acq_mktcap_df = ActionMetrics.calc_acq_mktcap(univ_df)

        df = pd.merge(univ_df, vwap_df, how='left', on='Action Id')  # adds 'ACQ_PREV_3D_VWAP','TGT_PREV_3D_VWAP'
        df = pd.merge(df, beta_df, how='left',
                      on='Action Id')  # adds 'TGT_BETA','TGT_STRESS_BETA','TGT_DOWNSIDE','ACQ_BETA','ACQ_STRESS_BETA','ACQ_DOWNSIDE'
        df = pd.merge(df, acq_mktcap_df, how='left', on='Action Id')  # adds ACQ_MKTCAP($)

        today = datetime.datetime.now().strftime('%Y-%m-%d')
        df['Day Loaded'] = today
        viper_dbutils.ViperDB.delete_sod_metrics(today)
        viper_dbutils.ViperDB.bulk_insert(
            df[['Action Id', 'Day Loaded', 'ACQ_PREV_3D_VWAP', 'TGT_PREV_3D_VWAP', 'TGT_BETA', 'TGT_STRESS_BETA']],
            'viper_universe_sod_metrics')
        logger.log_to_UI('Finished loading Start Of Day Metrics(SODM).', LogEnum.SYSTEM, '', '', is_fatal=0)

        res = df[['Action Id', 'ACQ_PREV_3D_VWAP', 'TGT_PREV_3D_VWAP', 'TGT_BETA', 'TGT_STRESS_BETA',
                  'ACQ_MKTCAP($)']].copy()
        res.to_csv("c:\\1\\univ_metrics_df.csv")

        return res

    @staticmethod
    def yield_curve():
        pass  # bbg.get_refdata_fields(['YCGT0025 Index'],["CURVE_TENOR_RATES"],{'CURVE_DATE':'20170714'})
