__author__ = 'pgrimberg'
import datetime
import viper_dbutils
from viper_enums import LogEnum
def log(msg):
	print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+' '+str(msg))

def log_to_UI(msg, log_enum, definiteness, action_id ,is_fatal=0):
	#print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')+' '+str(msg))
	category = LogEnum.to_str(log_enum)
	viper_dbutils.ViperDB.log(msg,category,definiteness, action_id,is_fatal)
