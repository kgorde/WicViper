__author__ = 'pgrimberg'
__author__ = 'pgrimberg'
import web
from web import form
import blpapi
import thread
import pytz
from optparse import OptionParser
import time
import datetime
import pandas as pd
import logger
from viper_enums import LogEnum
import viper_dbutils

DATA = blpapi.Name("data")
SECURITY_DATA = blpapi.Name("securityData")
SECURITY_ERROR = blpapi.Name("securityError")
SECURITY = blpapi.Name("security")
FIELD_DATA = blpapi.Name("fieldData")
FIELD_EXCEPTIONS = blpapi.Name("fieldExceptions")
FIELD_DISPLAY_UNITS = blpapi.Name("fieldDisplayUnits")
FIELD_ID = blpapi.Name("fieldId")
ERROR_INFO = blpapi.Name("errorInfo")
TICK_DATA = blpapi.Name("tickData")
RESPONSE_ERROR = blpapi.Name("responseError")
TIME = blpapi.Name("time")
TYPE = blpapi.Name("type")
VALUE = blpapi.Name("value")
TICK_SIZE = blpapi.Name("size")
COND_CODE = blpapi.Name("conditionCodes")
TRACK_API_COUNT = True

class bwrapper(object):
	def __init__(self):
		pass

	def open_blpapi_session(self):
	    # Create session options
	    parser = OptionParser(description="Retrieve reference data.")
	    parser.add_option("-a", "--ip", dest="host", help="server name or IP (default: %default)", metavar="ipAddress", default="localhost")
	    parser.add_option("-p", dest="port", type="int", help="server port (default: %default)", metavar="tcpPort", default=8194)
	    (options, args) = parser.parse_args()

	    # Fill session with the options
	    session_options = blpapi.SessionOptions()
	    session_options.setServerHost(options.host)
	    session_options.setServerPort(options.port)

	    # Create a Session
	    session = blpapi.Session(session_options)

	    # Start tbe Session
	    if not session.start():
	            return None

	    return session

	def open_service(self, session, svc):
	    if session is None:
	        return None
	    if not session.openService(svc):
	        session.stop()
	        return None

	    return session.getService(svc)

	def process_refdata_response(self, msg, fields):
	    sec2fld2value = {}
	    if not msg.hasElement(SECURITY_DATA): return {}
	    sec_data_arr = msg.getElement(SECURITY_DATA)
	    for sec_data in sec_data_arr .values():
	        sec_name = sec_data.getElementAsString(SECURITY)
	        sec2fld2value[sec_name] = {}
	        for field in sec_data.getElement(FIELD_DATA).elements():
	            field_name = str(field.name())
	            if not field.isValid:
	                sec2fld2value[sec_name][field_name] = "invalid field"
	                continue

	            sec2fld2value[sec_name][field_name] = field.getValueAsString() if not field.isArray() else \
	                [{str(elem.name()):(elem.getValueAsString() if not elem.isNull() else None) for elem in row.elements()} for i,row in enumerate(field.values())]

	        # handle errors
	        err2info = {}
	        for field_exception in sec_data.getElement(FIELD_EXCEPTIONS).values():
	            error_info = field_exception.getElement(ERROR_INFO)
	            category = error_info.getElementAsString("category")
	            err_value = field_exception.getElementAsString(FIELD_ID)
	            err2info[category] = err_value

	        if len(err2info) > 0:
	            sec2fld2value[sec_name]['ERROR'] = err2info

	        for fld in fields:
	            if fld not in sec2fld2value[sec_name]: sec2fld2value[sec_name][fld] = None

	    return sec2fld2value

	def log_api_usage(self,req_type, api_calls):
	    now = datetime.datetime.now()
	    now_utc = (now-datetime.datetime(1970,1,1)).total_seconds()*1000
	    now_str = now.strftime('%Y-%m-%d %H:%M:%S.%f')
	    viper_dbutils.ViperDB.store_api_call(now_str,now_utc,req_type,api_calls)

	def get_refdata_fields(self, sec_ids, fields, overrides_dict):
	    session = self.open_blpapi_session()
	    if session is None:
	        logger.log_to_UI("failed to open bloomberg session",LogEnum.BBG_API,'','',is_fatal=1)
	        return False
	    # open service
	    svc = "//blp/refdata"
	    service = self.open_service(session, svc)
	    if service is None:
	        logger.log_to_UI("failed to open bloomberg service",LogEnum.BBG_API,'','',is_fatal=1)
	        return False
	    bbg_request = service.createRequest("ReferenceDataRequest")

	    for sid in sec_ids:
	        bbg_request.append("securities", sid)

	    # append fields to request
	    for fld in fields:
	        bbg_request.append("fields",fld)

	    # add overrides
	    overrides = bbg_request.getElement("overrides")
	    for key in overrides_dict:
	        fieldId = key
	        value = overrides_dict[key]
	        ovrd = overrides.appendElement()
	        ovrd.setElement("fieldId", fieldId)
	        ovrd.setElement("value", value)

	    cid = session.sendRequest(bbg_request)
	    if TRACK_API_COUNT: self.log_api_usage('ReferenceDataRequest',len(sec_ids)*len(fields))
	    sec2fld2value = {}
	    try:
	        # Process received events
	        while True:
	            # We provide timeout to give the chance to Ctrl+C handling:
	            event = session.nextEvent(0)
	            for msg in event:
	                if cid in msg.correlationIds():
	                    sec2fld2value.update(self.process_refdata_response(msg,fields))

	            # Response completely received, so we could exit
	            if event.eventType() == blpapi.Event.RESPONSE:
	                break
	    except Exception as e:
	        print(e.message)

	    finally:
	        # Stop the session
	        session.stop()
	        return sec2fld2value

	def get_beqs(self, screen_name, screen_type='PRIVATE', folder='GENERAL', as_of_yyyymmdd=None):
		session = self.open_blpapi_session()
		if session is None:
			logger.log_to_UI("failed to open bloomberg session",LogEnum.BBG_API,'','',is_fatal=1)
			return False
		#open service
		svc = "//blp/refdata"
		service = self.open_service(session, svc)
		if service is None:
			logger.log_to_UI("failed to open bloomberg service " + svc,LogEnum.BBG_API,'','',is_fatal=1)
			return False
		beqs_request = service.createRequest("BeqsRequest")
		beqs_request.set("screenName",screen_name)
		beqs_request.set("screenType",screen_type)
		beqs_request.set("Group",folder)
		if not as_of_yyyymmdd is None:
		    overrides = beqs_request.getElement("overrides")
		    override1 = overrides.appendElement()
		    override1.setElement("fieldId","PiTDate")
		    override1.setElement("value",as_of_yyyymmdd)

		cid = session.sendRequest(beqs_request)
		if TRACK_API_COUNT: self.log_api_usage('BeqsRequest',1) # verify that it's really 1 api call
		sec2fld2str_value = {}
		try:
		    # Process received events
		    while True:
		        # We provide timeout to give the chance to Ctrl+C handling:
		        event = session.nextEvent(0)
		        for msg in event:
		            if cid in msg.correlationIds():
		                sec2fld2str_value.update(self.process_beqs_response(msg))
		        # Response completely received, so we could exit
		        if event.eventType() == blpapi.Event.RESPONSE:
		            break
		except:
		    print('BEQS error for ' + screen_name)
		finally:
		    # Stop the session
		    session.stop()
		    return sec2fld2str_value

	def get_histdata_fields(self, sec_ids, fields, overrides_dict,
	                        start_date_yyyymmdd,end_date_yyyymmdd,
	                        float_fields=None,
	                        periodicityAdjustment=None,
	                        periodicitySelection=None,
	                        currency=None,
	                        override_option=None,
	                        pricing_option=None,
	                        non_trading_day_fill_option=None,
	                        non_trading_day_fill_method=None,
	                        max_data_points=None,
	                        return_eids=None,
	                        return_relative_date=None,
	                        adjustment_normal=None,
	                        adjustment_abnormal=None,
	                        adjustment_split=None,
	                        adjustment_follow_DPDF=None,
	                        calendar_code_override=None,
	                        calendar_code_overrides=None,
	                        cdr_opt=None,
	                        ):
	    """
	    :param sec_ids:
	    :param fields:
	    :param overrides_dict:
	    :param start_date_yyyymmdd:
	    :param end_date_yyyymmdd:
	    :param periodicityAdjustment:  ACTUAL, CALENDAR or FISCAL
	    :param periodicitySelection: DAILY, WEEKLY, MONTHLY, QUARTERLY, SEMI_ANNUALLY, YEARLY
	    :param currency: USD,GBP, etc..
	    :param override_option: OVERRIDE_OPTION_CLOSE, OVERRIDE_OPTION_GPA (whether to use exch reported close price or avg price as close price)
	    :param pricing_option: PRICING_OPTION_PRICE, PRICING_OPTION_YIELD (show in terms of price or yield)
	    :param non_trading_day_fill_option: NON_TRADING_WEEKDAYS (include only weekdays), ALL_CALENDAR_DAYS (include all), ACTIVE_DAYS_ONLY (include only active days)
	    :param non_trading_day_fill_method: PREVIOUS_VALUE (search back up to 1 mo and retrieve the previous value available), NIL_VALUE
	    :param max_data_points: response will contain up to X data points. if original response > X, returns the last X points.
	    :param return_eids: TRUE/FALSE - setting to TRUE will populate fieldData with extra element containing a name and value for EID date
	    :param return_relative_date: TRUE/FALSE - Setting this to true will populate fieldData with an extra element containing a name and value for the relative date. For example RELATIVE_DATE = 2002 Q2
	    :param adjustment_normal: TRUE/FALSE - Adjust historical pricing to reflect: Regular Cash, Interim, 1st Interim, 2nd Interim, 3rd Interim, 4th Interim, 5th Interim, Income, Estimated, Partnership Distribution, Final, Interest on Capital, Distribution, Prorated.
	    :param adjustment_abnormal: TRUE/FALSE - Adjust historical pricing to reflect: Special Cash, Liquidation, Capital Gains, Long-Term Capital Gains, Short-Term Capital Gains, Memorial, Return of Capital, Rights Redemption, Miscellaneous, Return Premium, Preferred Rights Redemption, Proceeds/Rights, Proceeds/Shares, Proceeds/ Warrants.
	    :param adjustment_split: TRUE/FALSE - Adjust historical pricing and/or volume to reflect: Spin-Offs, Stock Splits/Consolidations, Stock Dividend/Bonus, Rights Offerings/ Entitlement.
	    :param adjustment_follow_DPDF: TRUE/FALSE - Setting to true will follow the DPDF<GO> BLOOMBERG PROFESSIONAL service function. True is the default setting for this option.
	    :param calendar_code_override: TRUE/FALSE - Returns the data based on the calendar of the specified country, exchange, or religion from CDR<GO>. Taking a two character calendar code null terminated string. This will cause the data to be aligned according to the calendar and including calendar holidays. Only applies only to DAILY requests.
	    :param calendar_code_overrides: list of calendar names
	    :param cdr_opt: if calendar_code_overrides is specified, then cdr_opt must either be CDR_AND or CDR_OR
	    :return:
	    """
	    if float_fields is None: float_fields = []
	    session = self.open_blpapi_session()

	    if session is None:
		    logger.log_to_UI("failed to open bloomberg session",LogEnum.BBG_API,'','',is_fatal=1)
		    return False
	    # open service
	    svc = "//blp/refdata"
	    service = self.open_service(session, svc)
	    if service is None:
		    logger.log_to_UI("failed to open bloomberg service " + svc,LogEnum.BBG_API,'','',is_fatal=1)
		    return False
	    bbg_request = service.createRequest("HistoricalDataRequest")
	    # append securities to request
	    for sid in sec_ids:
		    bbg_request.append("securities", sid)

		# append fields to request

	    for fld in fields:
		    bbg_request.append("fields",fld)

	    # add dates
	    bbg_request.set("startDate", start_date_yyyymmdd)
	    bbg_request.set("endDate", end_date_yyyymmdd)

	    #region ##### OPTIONAL  ######
	    if not periodicityAdjustment is None: bbg_request.set("periodicityAdjustment",periodicityAdjustment)
	    if not periodicitySelection is None: bbg_request.set("periodicitySelection",periodicitySelection)
	    if not currency is None: bbg_request.set("currency",currency)
	    if not override_option is None: bbg_request.set("overrideOption",override_option)
	    if not pricing_option is None: bbg_request.set("pricingOption",pricing_option)
	    if not non_trading_day_fill_option is None: bbg_request.set("nonTradingDayFillOption",non_trading_day_fill_option)
	    if not non_trading_day_fill_method is None: bbg_request.set("nonTradingDayFillMethod",non_trading_day_fill_method)
	    if not max_data_points is None: bbg_request.set("maxDataPoints",max_data_points)
	    if not return_eids is None: bbg_request.set("returnEIDs",return_eids)
	    if not return_relative_date is None: bbg_request.set("returnRelativeDate",return_relative_date)
	    if not adjustment_normal is None: bbg_request.set("adjustmentNormal",adjustment_normal)
	    if not adjustment_abnormal is None: bbg_request.set("adjustmentAbnormal",adjustment_abnormal)
	    if not adjustment_split is None: bbg_request.set("adjustmentSplit",adjustment_split)
	    if not adjustment_follow_DPDF is None: bbg_request.set("adjustmentFollowDPDF",adjustment_follow_DPDF)
	    if not calendar_code_override is None: bbg_request.set("calendarCodeOverride",calendar_code_override)
	    if not calendar_code_overrides is None:
		    cdr_overrides_info = bbg_request.getElement("calendarOverridesInfo")
		    cdr_overrides = cdr_overrides_info.getElement("calendarOverrides")
		    for cdr in calendar_code_overrides: cdr_overrides.appendValue(cdr)
		    cdr_overrides_info.setElement("calendarOverridesOperation", cdr_opt)
		# add overrides
	    overrides = bbg_request.getElement("overrides")
	    for key in overrides_dict:
		    fieldId = key
		    value = overrides_dict[key]
		    ovrd = overrides.appendElement()
		    ovrd.setElement("fieldId", fieldId)
		    ovrd.setElement("value", value)
		#endregion
	    cid = session.sendRequest(bbg_request)
	    if TRACK_API_COUNT: self.log_api_usage('HistoricalDataRequest',len(sec_ids)*len(fields))
	    sec2fld2value = {sid:None for sid in sec_ids}
	    try:
		    # Process received events
	        while True:
			    event = session.nextEvent(0) # We provide timeout to give the chance to Ctrl+C handling:
			    for msg in event:
			        if cid in msg.correlationIds():
				        sec2fld2value.update(self.process_histdata_response(msg,fields,float_fields))
			    # Response completely received, so we could exit
			    if event.eventType() == blpapi.Event.RESPONSE:
				    break
	    except Exception as e:
		    print(e.message)
	    finally:
		    # Stop the session
		    session.stop()
		    return sec2fld2value

	def process_beqs_response(self, msg):
	    if msg.hasElement(RESPONSE_ERROR):
	        err = msg.getElement(RESPONSE_ERROR)
	        err_msg = err.getElementAsString('message')
	        err_category = err.getElementAsString('category')
	        err_src = err.getElementAsString('source')
	        print({'ERROR':{'message':err_msg, 'category':err_category, 'source':err_src}})
	        return {}

	    data_elem = msg.getElement(DATA)
	    sec2fld2str_value = {}
	    if data_elem.hasElement(SECURITY_DATA):
	        sec_data_arr = data_elem.getElement(SECURITY_DATA)
	        for sec_data in sec_data_arr.values():
	            if not sec_data.hasElement(SECURITY): continue
	            sec_id = sec_data.getElementAsString(SECURITY)
	            if sec_data.hasElement(FIELD_EXCEPTIONS):
	                if (sec_data.getElement(FIELD_EXCEPTIONS)).numValues() > 0: continue # exception in sec
	            if not sec_data.hasElement(FIELD_DATA): continue
	            fld_data_elems = sec_data.getElement(FIELD_DATA).elements()
	            fld2str_value = {str(fld.name()):fld.getValueAsString() for fld in fld_data_elems}

	            sec2fld2str_value[sec_id] = fld2str_value

	    # flds_supported = []
	    # if data_elem.hasElement(FIELD_DISPLAY_UNITS):
	    #     fld_disp_unit_arr = data_elem.getElement(FIELD_DISPLAY_UNITS)
	    #     flds_supported = [str(disp_unit.name()) for disp_unit in fld_disp_unit_arr.elements()]

	    return sec2fld2str_value


	# times are in GMT
	# gmt = pytz.timezone('GMT')
	# eastern = pytz.timezone('US/Eastern')
	# start_dt_gmt = (eastern.localize(start_dt_et)).astimezone(gmt)
	# end_dt_gmt = (eastern.localize(end_dt_et)).astimezone(gmt)
	# returns a sorted list of tuples: (dt,size,px,type,cc)
	def get_refdata_intraday_tick(self,sec_id,start_dt_gmt,end_dt_gmt,debug=False):
	    # open session
	    session = self.open_blpapi_session()
	    if session is None: return None
	    # open service
	    svc = "//blp/refdata"
	    service = self.open_service(session, svc)
	    if service is None: return None

	    request = service.createRequest("IntradayTickRequest")

	    request.set("security",sec_id)
	    request.getElement("eventTypes").appendValue("TRADE")
	    #request.getElement("eventTypes").appendValue("AT_TRADE")
	    request.set("includeConditionCodes", False) # True

	    # all times are in GMT
	    request.set("startDateTime", start_dt_gmt)
	    request.set("endDateTime", end_dt_gmt)

	    if debug: print "Sending Request:", request
	    session.sendRequest(request)
	    if TRACK_API_COUNT: self.log_api_usage('IntradayTickRequest',1)

	    tick_arr = []
	    try:
	        # Process received events
	        while(True):
	            ev = session.nextEvent(0)
	            # Response completly received, so we could exit
	            if ev.eventType() in [blpapi.Event.RESPONSE,blpapi.Event.PARTIAL_RESPONSE]:
	                for msg in ev:
	                    if msg.hasElement(RESPONSE_ERROR):
	                        print("RESPONSE_ERROR")
	                        continue
	                    tickData = msg.getElement(TICK_DATA).getElement(TICK_DATA)
	                    for tickDatum in tickData.values():
	                        dt = tickDatum.getElement(TIME).getValueAsDatetime()
	                        size = tickDatum.getElement(TICK_SIZE).getValueAsInteger()
	                        px = tickDatum.getElement(VALUE).getValueAsFloat()
	                        typ = tickDatum.getElement(TYPE).getValueAsString()
	                        cc = tickDatum.getElementAsString(COND_CODE) if tickDatum.hasElement(COND_CODE) else ""
	                        tick_arr.append((dt,size,px,typ,cc))
	                break
	    except:
	        print('exception in intraday_tick_request ')
	    finally:
	        session.stop()
	    return tick_arr

	def process_histdata_response(self, msg, fields,float_fields):
	    sec2fld2ts = {}
	    if not msg.hasElement(SECURITY_DATA): return {}
	    sec_data_arr = msg.getElement(SECURITY_DATA)
	    if not sec_data_arr.hasElement(FIELD_DATA): return {}
	    if not sec_data_arr.hasElement(SECURITY): return {}
	    sec_id = sec_data_arr.getElementAsString(SECURITY)
	    f2dates,f2values = {f:[] for f in fields}, {f:[] for f in fields}
	    fieldDataArray = sec_data_arr.getElement(FIELD_DATA).values()
	    for histField in fieldDataArray:
	        if not histField.hasElement('date'): continue
	        for fld in fields:
	            f2dates[fld].append(pd.to_datetime(histField.getElementAsString('date')))
	            if histField.hasElement(fld):
	                f2values[fld].append(histField.getElementAsString(fld))
	            else:
	                f2values[fld].append(None)
	    sec2fld2ts[sec_id] = {fld: pd.Series(index=f2dates[fld],data=f2values[fld]).astype(float) if fld in float_fields else pd.Series(index=f2dates[fld],data=f2values[fld]) for fld in fields}
	    return sec2fld2ts

