__author__ = 'pgrimberg'

import mysql.connector
from mysql.connector import RefreshOption
import datetime
import numpy as np
import pandas as pd
from contextlib import closing
import json
import datetime


WIC_DB_HOST = '10.16.1.19'

class BDCalendar(object):
	def __init__(self):
		self.holidays = self.get_holidays()['Date'].apply(lambda x: pd.to_datetime(x))

	def is_holiday(self, dt):
		dt = pd.to_datetime(dt)
		for h in self.holidays:
			if dt == h: return True
		return False

	def is_bd(self, dt):
		return not self.is_holiday(dt) and dt.weekday() not in [5, 6]

	def prev_bd(self, dt):
		dt = dt - datetime.timedelta(days=1)
		while not self.is_bd(dt):
			dt = dt - datetime.timedelta(days=1)
		return dt

	def next_bd(self, dt):
		dt = dt + datetime.timedelta(days=1)
		while not self.is_bd(dt):
			dt = dt + datetime.timedelta(days=1)
		return dt

	def next_n_bds(self, dt, n):
		res=dt
		for i in range(n):
			res = self.next_bd(res)
		return res

	def get_holidays(self):
		cols = ['Date','Holiday']
		query = "SELECT `Date`, `Holiday` FROM wic.holidays2; "
		res = self.optimized_execute(query)
		df = pd.DataFrame(res, columns=cols)
		df['Date'] = df['Date'].apply(lambda x: pd.to_datetime(x))
		return df

	def optimized_execute(self, query, commit=False, retrieve_column_names=False):
		with closing(mysql.connector.connect(host=WIC_DB_HOST, user='root', password='Mkaymkay1', database='wic',
		                                     connection_timeout=25)) as wic_cnx:
			with closing(wic_cnx.cursor()) as wic_cursor:
				wic_cnx.cmd_refresh(RefreshOption.TABLES | RefreshOption.LOG | RefreshOption.THREADS)  # refresh cache
				wic_cursor.execute(query)
				if commit:
					wic_cnx.commit()
					return
				fetched_res = wic_cursor.fetchall()  # fetch (and discard) remaining rows

				if retrieve_column_names:
					return fetched_res, [i[0] for i in wic_cursor.description]

				return fetched_res
