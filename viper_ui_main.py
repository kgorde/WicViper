__author__ = 'pgrimberg'
import web
import json
import viper_htmlutils
from viper_indexes.index_math import IndexMath
from bd_calendar import BDCalendar
import wic_htmlutils
import wic_htmlutils.highcharts
import wic_dfutils
import datetime
import viper_dbutils
import pandas as pd
import inspect, os
import numpy as np
from bbg_api import bwrapper
render_layout = web.template.render('templates/', base='layout')
render = web.template.render("\\".join(inspect.getfile(inspect.currentframe()).split("\\")[:-1]) + "\\""templates\\")

urls = ('/(.*)', 'index')


class index:
    def __init__(self):
        pass

    def GET(self, name):
        i = web.input(name=None)
        title = ""
        content = ""

        # if web.ctx['ip'] in ['10.16.1.168', '10.16.1.183']: return

        # region high level stats
        if name == 'viper-get-high-level-stats-ajax':
            df = viper_dbutils.ViperDB.get_viper_stats_df()
            # ['NumOrdersPlaced','NumOrdersCompleted','NumFills','GrossDollarsOrdered','GrossDollarsFilled',DollarsFilledOnShareOrders,
            #   'TotalPctFill','DollarsWaitingForFills','TotCashSpent($)','ShortMktCap($)','LongMktCap($)','NetMktCap($)',
            #  'CashSeeded($)','CashBalance','PortfolioValue($)','PnL($)','CumulativePnL(bps)']
            return json.dumps({'rows': [list(row) for (idx, row) in df.iterrows()]})
        # endregion

        # region subscription status
        if name == 'viper-get-viper-subscription-status-ajax':
            vsm_df = viper_dbutils.ViperDB.get_viper_subscription_mode_df()  # cols = ['Ticker','IsSubscribe']
            vsm_df = vsm_df.rename(columns={'IsSubscribe': 'IsSubscribedInViper'})
            pfm_df = viper_dbutils.ViperDB.get_price_feed_mode_df()  # cols = ['Ticker','IsSubscribe']
            pfm_df = pfm_df.rename(columns={'IsSubscribe': 'IsSubscribedInPriceFeed'})
            df = pd.merge(vsm_df, pfm_df, how='outer', on='Ticker')
            df['IsSubscribedInViper'] = df['IsSubscribedInViper'].apply(
                lambda x: 'Yes' if x == 1 else ('No' if (x == 0 or pd.isnull(x)) else 'N.A.')).apply(
                lambda x: "<center>" + x + "</center>")
            df['IsSubscribedInPriceFeed'] = df['IsSubscribedInPriceFeed'].apply(
                lambda x: 'Yes' if x == 1 else ('No' if (x == 0 or pd.isnull(x)) else 'N.A.')).apply(
                lambda x: "<center>" + x + "</center>")
            df['Ticker'] = df['Ticker'].apply(lambda x: '<center>' + x + '</center>')
            return json.dumps({'rows': [list(row) for (idx, row) in df.iterrows()]})
        # endregion

        # region spec index charts
        if name == 'viper-get-spec-index-chart-ajax':
            SPEC_MA_INDEX_ID = 3
            now_utc = (datetime.datetime.now() - datetime.datetime(1970, 1, 1)).total_seconds() * 1000
            # ['IndexID','Date','ActionID','Cash/sh','Stock/sh','PaymentType','TgtTicker','AcqTicker',
            #    'TgtBBGID','AcqBBGID','TgtISIN','AcqISIN','ExpectedCompletion',
            #    'TGT_PX_LAST','ACQ_PX_LAST','TGT_PX_YEST_CLOSE','ACQ_PX_YEST_CLOSE',
            #    'DEAL_VALUE_LAST','SPREAD_LAST','TgtRet(%)','AcqRet(%)','TgtMktCap']
            spec_df = viper_dbutils.IndexDB.get_curr_index_constituents(SPEC_MA_INDEX_ID)
            spec_df['Date'] = spec_df['Date'].apply(lambda x: x.strftime('%m/%d/%Y'))

            spec_ma_equal_weight = spec_df['TgtRet(%)'].mean()
            spec_df['TgtRet(%)'] = [0.0 if (pd.isnull(px) or px == 0) else r for (r, px) in
                                    zip(spec_df['TgtRet(%)'], spec_df['TGT_PX_LAST'])]
            cols2include = ['Date', 'ActionID', 'TgtTicker', 'TGT_PX_LAST', 'TGT_PX_YEST_CLOSE', 'TgtRet(%)']
            spec_df = spec_df.fillna('')
            return json.dumps({
                'utc': now_utc,
                'spec_ma_equal_weight': spec_ma_equal_weight if not pd.isnull(spec_ma_equal_weight) else 0.0,
                'spec_ma_constituents': [list(row) for (idx, row) in spec_df[cols2include].iterrows()]
            })
        # endregion

        # region cash ma index chart
        if name == 'viper-get-cash-mna-index-chart-ajax':
            now = datetime.datetime.now()
            CASH_MA_INDEX_ID = 2
            now_utc = (now - datetime.datetime(1970, 1, 1)).total_seconds() * 1000
            index_memb_df = viper_dbutils.IndexDB.get_curr_index_constituents(CASH_MA_INDEX_ID)
            # ['IndexID','Date','ActionID','Cash/sh','Stock/sh','PaymentType','TgtTicker','AcqTicker',
            #   'TgtBBGID','AcqBBGID','TgtISIN','AcqISIN','ExpectedCompletion',
            #   'TGT_PX_LAST','ACQ_PX_LAST','TGT_PX_YEST_CLOSE','ACQ_PX_YEST_CLOSE',
            #   'DEAL_VALUE_LAST','SPREAD_LAST','TgtRet(%)','AcqRet(%)','TgtMktCap']

            verified_df = viper_dbutils.ViperDB.get_mna_verified_universe()
            verified_df = verified_df[verified_df['IsExcluded'] == 'false'].rename(
                columns={'Cash/sh': 'V_Cash/sh', 'Stock/sh': 'V_Stock/sh', 'ExpectedCompletion': 'V_ExpectedCompletion'}
            )[['ActionID', 'V_Cash/sh', 'V_Stock/sh', 'V_ExpectedCompletion']].copy()

            df = pd.merge(index_memb_df, verified_df, how='inner',
                          on='ActionID')  # Adds 'V_Cash/sh','V_Stock/sh','V_ExpectedCompletion'

            avg_duration = int(
                df['V_ExpectedCompletion'].apply(lambda x: None if pd.isnull(x) else (x - now).days).mean())

            df['Date'] = df['Date'].apply(lambda x: x.strftime('%m/%d/%Y'))
            df['V_ExpectedCompletion'] = df['V_ExpectedCompletion'].apply(
                lambda x: '' if pd.isnull(x) else x.strftime('%m/%d/%Y'))

            cash_ma_equal_weight_ret = df['TgtRet(%)'].mean()
            cash_ma_equal_weight_upside = df['SPREAD_LAST'].mean()

            cols2include = ['Date', 'ActionID', 'TgtTicker', 'TGT_PX_LAST', 'TGT_PX_YEST_CLOSE',
                            'V_Cash/sh', 'V_ExpectedCompletion', 'SPREAD_LAST', 'TgtRet(%)']

            df = df.fillna('')
            return json.dumps({
                'utc': now_utc,
                'cash_ma_equal_weight_ret': cash_ma_equal_weight_ret if not pd.isnull(
                    cash_ma_equal_weight_ret) else 0.0,
                'cash_ma_equal_weight_upside': cash_ma_equal_weight_upside if not pd.isnull(
                    cash_ma_equal_weight_upside) else 0.0,
                'avg_duration': avg_duration,
                'cash_ma_constituents': [list(row) for (idx, row) in df[cols2include].iterrows()]
            })
        # endregion

        # region ma hisorical chart ajax
        if name == 'mna-index-historical-chart-ajax':
            # idx_id = i['index_id'].encode('ascii','ignire') if 'index_id' in i.keys() else 3
            idx_id = 3  # todo: modular this
            # idx_name = viper_dbutils.IndexDB.get_index_name(idx_id)
            hist_const_df = viper_dbutils.IndexDB.get_historical_index_constituents(idx_id)

            ###patch###
            # hist_const_df = hist_const_df[hist_const_df['Date']<=pd.to_datetime('09/26/2017')].copy()
            ###########

            hist_exclusion_df = viper_dbutils.IndexDB.get_historical_index_exclusions(idx_id)
            verified_df = viper_dbutils.ViperDB.get_mna_verified_universe()
            hist_const_df = pd.merge(hist_const_df, verified_df[verified_df['IsExcluded'] == 'false'][['ActionID']],
                                     how='inner', on='ActionID')

            index_chain = IndexMath.calc_index_historical_ts(hist_const_df, hist_exclusion_df,
                                                             IndexMath.assign_equal_weight_by_SOD_prices)  # ['Date','AUM','NetMktVal','NumDeals'],
            eqw_df = index_chain.aum_ts_df()  # ['Date','AUM']
            eqw_df['DateUTC'] = eqw_df['Date'].apply(
                lambda x: (x - datetime.datetime(1970, 1, 1)).total_seconds() * 1000)
            eqw_df['Ret(bps)'] = 1e4 * ((eqw_df['AUM'] / eqw_df['AUM'].shift(1)) - 1.0)
            eqw_df = eqw_df.dropna().sort_values(by='Date')
            eqw_df['EqualWeight'] = 1e4 * ((1.0 + eqw_df['Ret(bps)'] / 1e4).cumprod() - 1.0)
            eqw_df['IsRebal'] = eqw_df['Date'].apply(lambda dt: index_chain[dt].is_rebal)

            index_chain = IndexMath.calc_index_historical_ts(hist_const_df, hist_exclusion_df,
                                                             IndexMath.assign_mktcap_weight_by_SOD_prices)  # ['Date','AUM','NetMktVal','NumDeals'],
            mvw_df = index_chain.aum_ts_df()
            mvw_df['DateUTC'] = mvw_df['Date'].apply(
                lambda x: (x - datetime.datetime(1970, 1, 1)).total_seconds() * 1000)
            mvw_df['Ret(bps)'] = 1e4 * ((mvw_df['AUM'] / mvw_df['AUM'].shift(1)) - 1.0)
            mvw_df = mvw_df.dropna().sort_values(by='Date')
            mvw_df['MktCapWeighted'] = 1e4 * ((1.0 + mvw_df['Ret(bps)'] / 1e4).cumprod() - 1.0)
            mvw_df['IsRebal'] = mvw_df['Date'].apply(lambda dt: index_chain[dt].is_rebal)

            start_dt = pd.to_datetime(hist_const_df['Date'].min())
            end_dt = pd.to_datetime(hist_const_df['Date'].max())
            pnl_df = viper_dbutils.Tradar.get_pnl_df_by_fund(start_dt.strftime('%Y%m%d'), end_dt.strftime('%Y%m%d'))
            fund_nav_df = viper_dbutils.Northpoint.get_NAV_df2('ARB')
            arb_pnl_df = pnl_df[pnl_df['Fund'] == 'ARB'].sort_values(by='Date')
            arb_df = wic_dfutils.perf_timeseries_df(arb_pnl_df, fund_nav_df, calc_stats=False)
            arb_df = arb_df.rename(columns={"Cumulative P&L bps (ffiled)": 'ARB'})
            arb_df['DateUTC'] = arb_df['Date'].apply(
                lambda x: (pd.to_datetime(x) - datetime.datetime(1970, 1, 1)).total_seconds() * 1000)

            return json.dumps({
                'equal_weighted_index_ts': [(row['DateUTC'], row['EqualWeight'], row['IsRebal']) for (idx, row) in
                                            eqw_df[['DateUTC', 'EqualWeight', 'IsRebal']].iterrows()],
                'mktcap_weighted_index_ts': [(row['DateUTC'], row['MktCapWeighted']) for (idx, row) in
                                             mvw_df[['DateUTC', 'MktCapWeighted']].iterrows()],
                'arbnx_ts': [list(row) for (idx, row) in arb_df[["DateUTC", "ARB"]].fillna(0).iterrows()]
            })
        # endregion

        # region ma hisorical constituents ajax
        if name == 'mna-index-historical-constituents-ajax':
            cdr = BDCalendar()
            dt = pd.to_datetime(datetime.datetime(1970, 1, 1) + datetime.timedelta(
                seconds=int(i['date_utc']) / 1000)) if 'date_utc' in i.keys() else cdr.prev_bd(datetime.datetime.now())
            dt = pd.to_datetime(dt.strftime('%m/%d/%Y'))
            idx_id = 3  # todo: modular this #idx_id = i['index_id'].encode('ascii','ignire') if 'index_id' in i.keys() else 3

            hist_const_df = viper_dbutils.IndexDB.get_historical_index_constituents(idx_id)
            hist_exclusion_df = viper_dbutils.IndexDB.get_historical_index_exclusions(idx_id)
            # retroactively remove unverifeid deals- they could have been verified when added to constituents, but now excluded
            # so exclude retroactively.
            verified_df = viper_dbutils.ViperDB.get_mna_verified_universe()
            hist_const_df = pd.merge(hist_const_df, verified_df[verified_df['IsExcluded'] == 'false'][['ActionID']],
                                     how='inner', on='ActionID')
            index_chain = IndexMath.calc_index_historical_ts(hist_const_df, hist_exclusion_df,
                                                             IndexMath.assign_equal_weight_by_SOD_prices)  # ['Date','AUM','NetMktVal','NumDeals'],
            if not index_chain.contains_dt(dt):
                return json.dumps({'res': 'no holding for ' + dt.strftime('%m/%d/%Y')})

            cols2include = ['Date', 'ActionID', 'Cash/sh', 'Stock/sh', 'TgtTicker', 'AcqTicker', 'ExpectedCompletion',
                            'TgtEODPrice', 'AcqEODPrice', 'TgtSODPrice', 'AcqSODPrice', 'TgtQty', 'AcqQty']

            eod_port = index_chain[dt].eod_portfolio_df
            if len(eod_port) == 0: eod_port = pd.DataFrame(columns=cols2include)
            for dc in ['Date', 'ExpectedCompletion']: eod_port[dc] = eod_port[dc].apply(
                lambda x: x.strftime('%m/%d/%Y'))

            aid_perf_df = index_chain.action_performance_df()  # ['Date','ActionID','PnL(bps)','CumPnL(bps)']
            aid_perf_df = aid_perf_df[aid_perf_df['Date'] <= dt].copy()
            aid_perf_df = pd.merge(aid_perf_df,
                                   aid_perf_df[['ActionID', 'Date']].groupby('ActionID').max().reset_index(),
                                   how='inner', on=['ActionID', 'Date'])
            # add some verified attributed
            aid_perf_df = pd.merge(aid_perf_df, verified_df, how='left',
                                   on='ActionID')  # ['ActionID','Announce Date','TgtTicker','AcqTicker','PaymentType', 'Cash/sh','Stock/sh','ExpectedCompletion','IsExcluded']
            aid_perf_df = aid_perf_df[
                ['Date', 'ActionID', 'Announce Date', 'TgtTicker', 'AcqTicker', 'Cash/sh', 'Stock/sh', 'PnL(bps)',
                 'CumPnL(bps)']].copy()
            for dc in ['Date', 'Announce Date']: aid_perf_df[dc] = aid_perf_df[dc].apply(
                lambda x: x.strftime('%m/%d/%Y'))

            return json.dumps({
                'eod_port': [list(row) for (idx, row) in eod_port[cols2include].fillna('').iterrows()],
                'action_performance': [list(row) for (idx, row) in aid_perf_df.fillna('').iterrows()],
                'res': 'success'
            })
        # endregion

        if name == 'viper-get-historical-index-chart':
            #Load all the data from the chart
            df_historical_index = viper_dbutils.IndexDB.get_historical_index_values()

            return df_historical_index.to_json(orient='split')




        # region ma live index chart
        if name == 'viper-get-mna-index-chart-ajax':
            now = datetime.datetime.now()
            MA_INDEX_ID = 3
            now_utc = (now - datetime.datetime(1970, 1, 1)).total_seconds() * 1000
            index_memb_df = viper_dbutils.IndexDB.get_curr_index_constituents(MA_INDEX_ID)


            index_memb_df = index_memb_df.loc[index_memb_df['DealCurrency'] == 'USD']
            # ['IndexID','Date','ActionID','Cash/sh','Stock/sh','PaymentType','TgtTicker','AcqTicker',
            #   'TgtBBGID','AcqBBGID','TgtISIN','AcqISIN','ExpectedCompletion',TgtEODDownsidePX,
            #   'TGT_PX_LAST','ACQ_PX_LAST','TGT_PX_YEST_CLOSE','ACQ_PX_YEST_CLOSE',
            #   'DEAL_VALUE_LAST','SPREAD_LAST','TgtRet(%)','AcqRet(%)','TgtMktCap']

            target_tickers = list(index_memb_df['TgtTicker'])
            acquirer_tickers = list(index_memb_df['AcqTicker'])

            # Get the Lastest PX_LAST for all Target_Tickers and Acquirer_Tickers
            bbg = bwrapper()
            target_px_last = bbg.get_refdata_fields(target_tickers, ['CRNCY_ADJ_PX_LAST', 'CRNCY_ADJ_PX_CLOSE_1D'],{'EQY_FUND_CRNCY':'USD'})
            acquirer_px_last = bbg.get_refdata_fields(acquirer_tickers, ['CRNCY_ADJ_PX_LAST', 'CRNCY_ADJ_PX_CLOSE_1D'], {'EQY_FUND_CRNCY':'USD'})

            #Replace the old values of TGT_PX_LAST and ACQ_PX_LAST
            #print(target_px_last[target_tickers[0]]['PX_LAST'])

            #index_memb_df.dropna(subset=['AcqTicker'],inplace=True)
            for every_ticker in target_tickers:
                #Update the values
                index_memb_df.loc[index_memb_df['TgtTicker'] == every_ticker, 'TGT_PX_LAST'] = float(target_px_last[every_ticker]['CRNCY_ADJ_PX_LAST'])
                index_memb_df.loc[index_memb_df['TgtTicker'] == every_ticker, 'TGT_PX_YEST_CLOSE'] = float(target_px_last[every_ticker]['CRNCY_ADJ_PX_CLOSE_1D'])
            for every_ticker in acquirer_tickers:
                #Update the values
                if acquirer_px_last.get(every_ticker) is None:
                    # print(index_memb_df[index_memb_df['AcqTicker'] == None])
                    # index_memb_df.drop(index_memb_df[index_memb_df['AcqTicker'] == np.NaN].index, inplace=True)
                    continue

                if acquirer_px_last.get(every_ticker).get('CRNCY_ADJ_PX_LAST') is None:
                    # index_memb_df.drop(index_memb_df[index_memb_df['AcqTicker'] == every_ticker].index,inplace=True)
                    continue

                else:
                    index_memb_df.loc[index_memb_df['AcqTicker'] == every_ticker, 'ACQ_PX_LAST'] = float(acquirer_px_last[every_ticker]['CRNCY_ADJ_PX_LAST'])
                    index_memb_df.loc[index_memb_df['AcqTicker'] == every_ticker, 'ACQ_PX_YEST_CLOSE'] = float(acquirer_px_last[every_ticker]['CRNCY_ADJ_PX_CLOSE_1D'])



            i_yield_srs = viper_dbutils.ViperDB.get_yield_curve_srs()

            verified_df = viper_dbutils.ViperDB.get_mna_verified_universe()

            verified_df = verified_df[verified_df['IsExcluded'] == 'false'].rename(
                columns={'Cash/sh': 'V_Cash/sh', 'Stock/sh': 'V_Stock/sh', 'ExpectedCompletion': 'V_ExpectedCompletion'}
            )[['ActionID', 'V_Cash/sh', 'V_Stock/sh', 'V_ExpectedCompletion']].copy()
            df = pd.merge(index_memb_df, verified_df, how='inner',
                          on='ActionID')  # Adds  'V_Cash/sh','V_Stock/sh','V_ExpectedCompletion'



            df['Duration'] = df['V_ExpectedCompletion'].apply(lambda x: None if pd.isnull(x) else (x - now).days)
            df['RiskFreeForDuration'] = df['Duration'].apply(lambda x: i_yield_srs[x] if x in i_yield_srs else None)
            df['Downside(%)'] = 100.0 * ((df['TgtEODDownsidePX'] / df['TGT_PX_LAST']) - 1.0)
            # apply verified terms into spread calc
            df['DEAL_VALUE_LAST'] = df['V_Cash/sh'].fillna(0) + (
                        df['V_Stock/sh'].fillna(0) * df['ACQ_PX_LAST'].fillna(0))
            df['SPREAD_LAST'] = 100.0 * ((df['DEAL_VALUE_LAST'] / df['TGT_PX_LAST']) - 1.0)

            # Only include deals with duration > 7
            df = df.loc[df['Duration'] > 7]
            # calc prob close
            def calc_prob_close(downside_px, curr_px, upside_px, duration_days):
                if pd.isnull(curr_px) or pd.isnull(downside_px) or pd.isnull(upside_px) or pd.isnull(
                    duration_days): return None
                if duration_days not in i_yield_srs: return None
                if upside_px <= downside_px: return None

                yield_pct = i_yield_srs[duration_days]
                p = 100.0 * (((1.0 + yield_pct / 100.0) * curr_px - downside_px) / (upside_px - downside_px))
                p = max(0, min(p, 100))
                return p

            df['ProbClose(%)'] = [calc_prob_close(d, p, u, dur) for (d, p, u, dur) in
                                  zip(df['TgtEODDownsidePX'], df['TGT_PX_LAST'], df['DEAL_VALUE_LAST'], df['Duration'])]
            df['SPREAD_LAST_ANN'] = [None if (pd.isnull(sprd) or pd.isnull(dtc)) else (
                None if dtc <= 0 else 100.0 * (((1.0 + sprd / 100.0) ** (365.0 / dtc)) - 1.0)) for (sprd, dtc) in
                                     zip(df['SPREAD_LAST'], df['Duration'])]
            df['TgtMktCap(bln)'] = df['TgtMktCap'].apply(lambda x: None if pd.isnull(x) else round(x / 1e9, 2))
            tot_mktcap = df['TgtMktCap(bln)'].sum()
            df['MktCapWeight'] = df['TgtMktCap(bln)'] / tot_mktcap

            # cosmetics
            df['Date'] = df['Date'].apply(lambda x: x.strftime('%m/%d/%Y'))
            df['V_ExpectedCompletion'] = df['V_ExpectedCompletion'].apply(
                lambda x: '' if pd.isnull(x) else x.strftime('%m/%d/%Y'))

            ma_equal_weight_ret = df['TgtRet(%)'].mean()
            ews = df['SPREAD_LAST'].mean()
            mvws = (df['SPREAD_LAST'] * df['MktCapWeight']).sum()
            eq_d = int(df['Duration'].mean())
            # print(df.head())
            mv_d = int((df['Duration'].fillna(0) * df['MktCapWeight'].fillna(0)).sum())
            mvws_ann_by_mvd = 100.0 * (((1.0 + mvws / 100.0) ** (365.0 / mv_d)) - 1.0) if mv_d != 0 else 0.0
            ews_ann_by_ewd = 100.0 * (((1.0 + ews / 100.0) ** (365.0 / eq_d)) - 1.0) if eq_d != 0 else 0.0

            cols2include = ['Date', 'ActionID', 'TgtTicker', 'AcqTicker','DealCurrency',
                            'TGT_PX_LAST', 'ACQ_PX_LAST', 'TGT_PX_YEST_CLOSE', 'ACQ_PX_YEST_CLOSE',
                            'V_Cash/sh', 'V_Stock/sh', 'V_ExpectedCompletion', 'Duration',
                            'TgtEODDownsidePX', 'DEAL_VALUE_LAST',
                            'Downside(%)', 'SPREAD_LAST', 'RiskFreeForDuration',
                            'SPREAD_LAST_ANN', 'ProbClose(%)',
                            'TgtRet(%)', 'TgtMktCap(bln)']

            df = df.fillna('')
            #Insert Constitutents of the Index into the Database

            viper_dbutils.IndexDB.insert_historical_ma_index_constituents(df)
            #First insert the Index into the Database and then load all the points
            viper_dbutils.IndexDB.insert_historical_index_values(now_utc, ma_equal_weight_ret if not pd.isnull(ma_equal_weight_ret) else 0.0, ews, 0, eq_d, 0, ews_ann_by_ewd,
                                                                 ews_ann_by_ewd)


            return json.dumps({
                'utc': now_utc,
                'ma_equal_weight_ret': ma_equal_weight_ret if not pd.isnull(ma_equal_weight_ret) else 0.0,
                'ma_equal_weight_upside': ews,
                # 'ma_mktcap_weight_upside':mvws,
                'ma_mktcap_weight_upside': 0,
                'avg_duration': eq_d,
                # 'mv_duration': mv_d,
                'mv_duration': 0,
                # 'mvws_ann_by_mvd': mvws_ann_by_mvd,
                'mvws_ann_by_mvd': ews_ann_by_ewd,
                'ews_ann_by_ewd': ews_ann_by_ewd,
                'ma_constituents': [list(row) for (idx, row) in df[cols2include].iterrows()]
            })
        # endregion

        # region mna universe verifier
        if name == 'viper-get-mna-universe-to-verify-ajax':
            now = datetime.datetime.now()
            ifnull = lambda a, b: b if pd.isnull(a) else a
            import universe_loader
            #universe_loader.UniverseLoader().load_mna_universe()

            df = viper_dbutils.ViperDB.get_universe()

            f1 = df['Deal Status'].str.lower() == 'pending'  # pending deal
            f2 = df['Announced Date'].apply(
                lambda x: False if pd.isnull(x) else (now - x).days <= 365)  # announced in last year
            f3 = df['TgtMktCap($)'] >= 400
            df = df[f1 & f2 & f3].copy()
            verified_df = viper_dbutils.ViperDB.get_mna_verified_universe()
            df = pd.merge(df, verified_df, how='left', left_on=['Action Id'], right_on=['ActionID'])
            df['Announced Date'] = [ifnull(v, u) for (u, v) in zip(df['Announced Date'], df['Announced Date'])]
            df['TgtTicker'] = [ifnull(v, u) for (u, v) in zip(df['Target Ticker'], df['TgtTicker'])]
            df['AcqTicker'] = [ifnull(v, u) for (u, v) in zip(df['Acquirer Ticker'], df['AcqTicker'])]

            df['PaymentType'] = [ifnull(v, u) for (u, v) in zip(df['Payment Type'], df['PaymentType'])]
            df['Cash/sh'] = [ifnull(v, u) for (u, v) in zip(df['Cash/sh_x'], df['Cash/sh_y'])]
            df['Stock/sh'] = [ifnull(v, u) for (u, v) in zip(df['Stock/sh_x'], df['Stock/sh_y'])]
            df['ExpectedCompletion'] = [ifnull(v, u) for (u, v) in
                                        zip(df['Expected Completion Date'], df['ExpectedCompletion'])]
            df['IsVerified'] = df['ActionID'].apply(lambda x: 'false' if pd.isnull(x) else 'true')
            df['IsExcluded'] = ['false' if pd.isnull(aid) else is_excl for (aid, is_excl) in
                                zip(df['ActionID'], df['IsExcluded'])]
            # region cosmetics
            df = df.sort_values(by=['Announced Date', 'Action Id'], ascending=[False, True])
            date_cols = ['Announced Date', 'ExpectedCompletion', 'Date Loaded']
            for dc in date_cols: df[dc] = df[dc].apply(lambda x: None if pd.isnull(x) else x.strftime('%m/%d/%Y'))
            del df['ActionID']
            df = df.rename(columns={'Date Loaded': 'DateLoaded', 'Action Id': 'ActionID'})
            df = df.fillna('')
            # endregion

            cols = ["DateLoaded", "ActionID", "Announced Date", "TgtTicker", "AcqTicker","LiveTgtPrice","LiveAcqPrice","DealValue",
                    "PaymentType", "Cash/sh", "Stock/sh", "Spread(%)",'Ann.ROR', "ExpectedCompletion", "IsExcluded",
                    "IsVerified"]



            #Get live Target Price
            #Add new Columns
            df['LiveTgtPrice'] = np.NaN
            df['LiveAcqPrice'] = np.NaN

            target_tickers = list(df['TgtTicker'])
            deal_currencies = list(df['Deal Currency'])
            target_tickers = [security + ' Equity' for security in target_tickers]

            bbg = bwrapper()

            target_px_last = {}

            #Adjust for Foreign Exchange

            for ticker, deal_ccy  in zip(target_tickers, deal_currencies):
                target_px_last[ticker] = bbg.get_refdata_fields([ticker], ['CRNCY_ADJ_PX_LAST'],{'EQY_FUND_CRNCY': deal_ccy})


            #target_px_last = bbg.get_refdata_fields(target_tickers, ['CRNCY_ADJ_PX_LAST'],{'EQY_FUND_CRNCY': 'USD'})


            for every_ticker in target_tickers:
                # Update the values
                df.loc[df['Target Ticker'] +" Equity" == every_ticker, 'LiveTgtPrice'] = float(
                    target_px_last[every_ticker][every_ticker]['CRNCY_ADJ_PX_LAST'])


            #Get live Acquirer Price
            acquirer_tickers = list(df['AcqTicker'])
            acquirer_tickers = [security + ' Equity' for security in acquirer_tickers]
            bbg = bwrapper()
            acquirer_px_last = {}

            for ticker, deal_ccy in zip(acquirer_tickers, deal_currencies):
                acquirer_px_last[ticker] =bbg.get_refdata_fields([ticker], ['CRNCY_ADJ_PX_LAST'], {'EQY_FUND_CRNCY': deal_ccy})


            for every_ticker in acquirer_tickers:
                # Update the values
                if acquirer_px_last.get(every_ticker) is None:
                    # print(index_memb_df[index_memb_df['AcqTicker'] == None])
                    # index_memb_df.drop(index_memb_df[index_memb_df['AcqTicker'] == np.NaN].index, inplace=True)
                    continue

                if acquirer_px_last.get(every_ticker).get(every_ticker).get('CRNCY_ADJ_PX_LAST') is None:
                    #df.drop(df[df['Acquirer Ticker'] +" Equity" == every_ticker].index,inplace=True)
                    continue

                else:
                    df.loc[df['Acquirer Ticker']+" Equity" == every_ticker, 'LiveAcqPrice'] = float(acquirer_px_last[every_ticker][every_ticker]['CRNCY_ADJ_PX_LAST'])

            #Calculate Deal Value
            df = df.astype('float',raise_on_error=False)
            df = df.fillna(0)
            df['Cash/sh'] = df['Cash/sh'].replace('',0)
            df['Stock/sh'] = df['Stock/sh'].replace('', 0)

            df['DealValue'] = df['Cash/sh'].fillna(0) + (
                    df['Stock/sh'].fillna(0) * df['LiveAcqPrice'].fillna(0))

            df['Spread(%)'] = 100.0 * ((df['DealValue'] / df['LiveTgtPrice']) - 1.0)

            #Calculate the Annulazed Rate of
            df['ExpectedCompletion'] = df['ExpectedCompletion'].apply(lambda x: '' if (pd.isnull(x) or x=='') else datetime.datetime.strptime(x,"%m/%d/%Y")) #Can be incorrect due to the Datetime and just date values. Time can cause it to be incorrect
            df['Duration'] = df['ExpectedCompletion'].apply(lambda x: None if pd.isnull(x) else (x - now).days)
            df['ExpectedCompletion'] = df['ExpectedCompletion'].apply(lambda x: '' if pd.isnull(x) else datetime.datetime.strftime(x, "%m/%d/%Y"))
            df['Ann.ROR'] = (df['Spread(%)']/df['Duration'])*365
            df = df[cols].copy()
            df = df.fillna('N/A')

            return json.dumps([{c: row[c] for c in cols} for (idx, row) in df.iterrows()])

        # endregion

        # region chart
        if name == 'viper-get-pnl-chart-ajax':
            now_utc = (datetime.datetime.now() - datetime.datetime(1970, 1, 1)).total_seconds() * 1000

            securities_mktval = viper_dbutils.ViperDB.get_curr_securities_mktval()
            cash_spent_on_securities = viper_dbutils.ViperDB.get_curr_cash_from_trading()
            cash_from_deposits_and_redemptions = viper_dbutils.ViperDB.get_curr_cash_from_deposits_and_redemptions()

            securities_pnl = securities_mktval - cash_spent_on_securities
            aum = cash_from_deposits_and_redemptions + securities_pnl

            return json.dumps({'utc': now_utc, 'portfolio': int(securities_mktval), 'aum': int(aum),
                               'cash': int(aum - securities_mktval)})

        if name == 'viper-get-mkt-snapshot-ajax':
            now_utc = (datetime.datetime.now() - datetime.datetime(1970, 1, 1)).total_seconds() * 1000
            (spx, vix) = viper_dbutils.ViperDB.get_mkt_snapshot()
            return json.dumps({'utc': now_utc, 'spx': spx, 'vix': vix})

        # endregion

        # region orders
        if name == 'viper-get-orders-ajax':
            last_update_utc = int(i['last_update_utc']) if 'last_update_utc' in i.keys() else 0
            last_creation_utc = int(i['last_creation_utc']) if 'last_creation_utc' in i.keys() else 0
            df = viper_dbutils.ViperDB.get_orders_df(last_update_utc)

            df['CreationTime'] = df['CreationTime'].apply(
                lambda x: None if pd.isnull(x) else x.strftime('%m/%d/%Y %H:%M:%S'))
            df['CompletionTime'] = df['CompletionTime'].apply(
                lambda x: None if pd.isnull(x) else x.strftime('%m/%d/%Y %H:%M:%S'))

            float_cols = ['SharesSought', 'PX', 'DollarsAllocated', 'TotalQty', 'AvgCostBasis', 'CashSpent',
                          'DollarsLeft', 'NumFills', 'MinFillPX', 'MaxFillPX', 'PctComplete', 'CashWithdrew']
            for fc in float_cols: df[fc] = df[fc].astype(float)
            new_opened_orders_df = df[df['CreationTimeUTC'] > last_creation_utc].fillna('')
            new_rows = [list(row) for (idx, row) in new_opened_orders_df.iterrows()]

            updated_orders_df = df[
                (df['LastUpdateUTC'] > last_update_utc) & (df['CreationTimeUTC'] <= last_creation_utc)].fillna('')

            order_id2edit_row = {row['OrderID']: list(row) for (idx, row) in updated_orders_df.iterrows()}
            data_row = {'new_orders': new_rows, 'modify_orders': order_id2edit_row}
            return json.dumps(data_row)
        # endregion

        # region fills
        if name == 'viper-get-fills-ajax':
            last_utc = int(i['last_utc']) if 'last_utc' in i.keys() else 0
            fills_df = viper_dbutils.ViperDB.get_fills_UI(min_utc=last_utc)
            # only returns today's fills
            fills_df = fills_df[fills_df['FillTime'] >= datetime.datetime.now().date()].copy()
            # ['Ticker', 'FillID','OrderID','FillTime','FillTimeUTC','NumShares','ExecPX']
            fills_df['FillTime'] = fills_df['FillTime'].apply(
                lambda x: None if pd.isnull(x) else x.strftime('%m/%d/%Y %H:%M:%S'))
            return json.dumps({'new_fills': [list(row) for (idx, row) in fills_df.iterrows()]})
        # endregion

        # region full spectrum attribution
        if name == 'viper-get-sleeves-table-ajax':
            pos_df = viper_dbutils.ViperDB.get_positions()
            pos_df['Deal Status'] = pos_df['Deal Status'].apply(lambda x: 'portfolio hedge' if x == '' else x)
            df = pos_df[['Deal Status', 'MktCap', 'MktCapPctOfAUM', 'PnL($)']].groupby(
                'Deal Status').sum().reset_index()
            cols2return = ['Deal Status', 'MktCap', 'MktCapPctOfAUM', 'PnL($)']
            return json.dumps({'rows': [list(row) for (idx, row) in df[cols2return].iterrows()]})
        # endregion

        # region positions
        if name == 'viper-get-positions-ajax':
            pos_df = viper_dbutils.ViperDB.get_positions()
            #    ['ActionID', 'UniverseDealStatus','UniverseProposedDate', 'Ticker', 'ISIN','Tgt/Acq','Deal Status','Payment Type','Deal Currency','AcquirerCCY',
            #    'Cash/sh','Stock/sh','Cash Value','AnncDate','ExpCompDate','AnncDealSize','DollarsUnfilled',
            #    'CashSpent','DealState','Beta','HighVixBeta','TgtWeight(bps)','CurrWeight(bps)','PctOfTgtWeight','Qty',
            #    'PX','MktCap','MktCapPctOfAUM','PnL($)']
            if len(pos_df) == 0: return json.dumps({'positions': []})

            # region Deal Expiration - Adds EffectiveStartDate, ExpirationDate, DaysToExpiration
            manual_rumors_df = viper_dbutils.ViperDB.get_analyst_input_rumors()  # ['ManualRumorID','Timestamp','TimestampUTC','Ticker','Analyst']
            rid2creation_timestamp = {('MANUAL_RUMOR_' + str(row['ManualRumorID'])): pd.to_datetime(row['Timestamp'])
                                      for (idx, row) in manual_rumors_df[['ManualRumorID', 'Timestamp']].iterrows()}

            def calc_effective_start_date(aid, deal_status, annc_dt, proposed_dt):
                if deal_status == 'pending': return annc_dt
                if deal_status == 'proposed':
                    if aid in rid2creation_timestamp:
                        return rid2creation_timestamp[aid]
                    return proposed_dt
                return None

            pos_df['RumorEffectiveStart'] = [calc_effective_start_date(aid, ds, annc_dt, proposed_dt) for
                                             (aid, ds, annc_dt, proposed_dt) in
                                             zip(pos_df['ActionID'], pos_df['Deal Status'], pos_df['AnncDate'],
                                                 pos_df['UniverseProposedDate'])]
            pos_df['RumorEffectiveStart'] = pos_df['RumorEffectiveStart'].apply(
                lambda x: None if pd.isnull(x) else x.date())
            pos_df['RumorExpiration'] = [
                None if (ds == 'pending' or pd.isnull(eff_start_dt)) else eff_start_dt + datetime.timedelta(days=14)
                for (ds, eff_start_dt) in zip(pos_df['Deal Status'], pos_df['RumorEffectiveStart'])]
            now = datetime.datetime.now().date()
            pos_df['DaysToExpiration'] = pos_df['RumorExpiration'].apply(
                lambda x: None if pd.isnull(x) else None if x < now else (x - now).days)
            # endregion
            pos_df['AvgCostBasis'] = [(px - pnl / qty) if qty != 0 else None for (px, qty, pnl) in
                                      zip(pos_df['PX'], pos_df['Qty'], pos_df['PnL($)'])]
            pos_df['ChgFromAvgCostBasis'] = 100.0 * ((pos_df['PX'] / pos_df['AvgCostBasis']) - 1.0)

            # region cosmetics
            pos_df['ActionID'] = pos_df['ActionID'].fillna('N.A.')
            dt_cols = ['AnncDate', 'ExpCompDate', 'RumorEffectiveStart', 'RumorExpiration']
            for dc in dt_cols: pos_df[dc] = pos_df[dc].apply(
                lambda x: None if pd.isnull(x) else pd.to_datetime(x).strftime('%m/%d/%Y'))
            # endregion

            # add top level data
            top_level_cols = ['DollarsUnfilled', 'CashSpent', 'TgtWeight(bps)', 'MktCap', 'MktCapPctOfAUM', 'PnL($)']
            action_level_pos_df = pos_df[['ActionID'] + top_level_cols].groupby('ActionID').sum().reset_index().rename(
                columns={c: 'Tot' + c for c in top_level_cols})
            pos_df = pd.merge(pos_df, action_level_pos_df, how='left',
                              on='ActionID')  # adds 'TotDollarsUnfilled','TotCashSpent','TotTgtWeight(bps)','TotCurrWeight(bps)','TotMktCap','TotPnL($)'
            deal_tgt_tkr_df = pos_df[pos_df['Tgt/Acq'].str.lower() == 'tgt'][['ActionID', 'Ticker']].groupby(
                'ActionID').agg(lambda x: x.iloc[0]).reset_index().rename(columns={'Ticker': 'TgtTicker'})
            pos_df = pd.merge(pos_df, deal_tgt_tkr_df, on='ActionID', how='left')  # Adds 'TgtTicker'
            pos_df = pos_df.fillna('')

            # encode expandable data as a new column
            expandable_rows = ['Ticker', 'ISIN', 'Tgt/Acq', 'DollarsUnfilled', 'CashSpent', 'Beta', 'HighVixBeta',
                               'TgtWeight(bps)', 'CurrWeight(bps)',
                               'Qty', 'PX', 'AvgCostBasis', 'ChgFromAvgCostBasis', 'MktCap', 'MktCapPctOfAUM', 'PnL($)']
            action_ids = pos_df['ActionID'].unique()
            aid2rows_json = {aid: json.dumps({'expandable_rows_json': [{er: row[er] for er in expandable_rows} for
                                                                       (idx, row) in
                                                                       pos_df[pos_df['ActionID'] == aid].iterrows()]})
                             for aid in action_ids}
            pos_df['ExpandableRowsJSON'] = pos_df['ActionID'].apply(lambda x: aid2rows_json[x])

            cols2show = ['ActionID', 'TgtTicker', 'Deal Status', 'Payment Type', 'Deal Currency', 'AcquirerCCY',
                         'Cash/sh', 'Stock/sh',
                         'Cash Value',
                         'RumorEffectiveStart', 'RumorExpiration', 'DaysToExpiration',
                         'AnncDate', 'ExpCompDate',
                         'AnncDealSize', 'DealState',
                         'TotDollarsUnfilled', 'TotCashSpent', 'TotTgtWeight(bps)',
                         'TotMktCap', 'TotMktCapPctOfAUM', 'TotPnL($)', 'ExpandableRowsJSON'
                         ]
            res = pos_df[cols2show].drop_duplicates().sort_values(by='TotMktCap', ascending=False)

            return json.dumps({'positions': [{c: row[c] for c in cols2show} for (idx, row) in res.iterrows()]})
        # endregion

        # region universe
        if name == 'viper-get-universe-ajax':
            df = viper_dbutils.ViperDB.get_universe()
            df['Date Loaded'] = df['Date Loaded'].apply(lambda x: x.strftime('%m/%d/%Y') if not pd.isnull(x) else None)
            df['Announced Date'] = df['Announced Date'].apply(
                lambda x: x.strftime('%m/%d/%Y') if not pd.isnull(x) else None)
            df['Proposed Date'] = df['Proposed Date'].apply(
                lambda x: x.strftime('%m/%d/%Y') if not pd.isnull(x) else None)
            df['Completion/Termination Date'] = df['Completion/Termination Date'].apply(
                lambda x: x.strftime('%m/%d/%Y') if not pd.isnull(x) else None)
            df['Expected Completion Date'] = df['Expected Completion Date'].apply(
                lambda x: x.strftime('%m/%d/%Y') if not pd.isnull(x) else None)
            df['Acquirer Ticker'] = df['Acquirer Ticker'].apply(
                lambda x: None if pd.isnull(x) else (x if len(x) < 16 else 'Multiple'))
            df['TgtMktCap($bln)'] = df['TgtMktCap($)'] / 1e3
            cols2include = ['Date Loaded', 'Action Id', 'Announced Date', 'Proposed Date', 'Deal Status',
                            'Target Ticker', 'Acquirer Ticker',
                            'Cash Value', 'Current Premium', 'Gross Spread', 'Announced Premium',
                            'PercentProForma', 'Expected Completion Date',
                            'Payment Type', 'Announced Total Value', 'Deal Currency',
                            'Cash/sh', 'Stock/sh', 'AcquirerCCY', 'Target ISIN', 'Acquirer ISIN',
                            'TgtMktCap($bln)',
                            'TermFee/AnncTxnValue(%)']  # 'ACQ_PREV_3D_VWAP','TGT_PREV_3D_VWAP','TGT_BETA','TGT_STRESS_BETA']

            df = df[cols2include].fillna('')
            # for c in df.columns: df[c] = df[c].apply(lambda x:'<center>'+str(x)+'</center>')

            return json.dumps({'universe_rows': [list(row) for (idx, row) in df.iterrows()]})
        # endregion

        # region task scheduler invoke times
        if name == 'viper-get-next-invoke-times':
            now = datetime.datetime.now()
            task_schedule_df = viper_dbutils.ViperDB.get_viper_tasks_schedule_df()  # ['TaskName','NextInvokeTimestamp','NextInvokeUTC']
            td_fmt = lambda secs: '%s hours %s min %s sec' % (secs / 3600, (secs % 3600) / 60, secs % 60)
            task_schedule_df['TimeToNextInvoke'] = task_schedule_df['NextInvokeTimestamp'].apply(
                lambda x: 'Not yet scheduled...' if now > x else td_fmt(int((x - now).total_seconds())))
            task_schedule_df['NextInvokeTimestamp'] = task_schedule_df['NextInvokeTimestamp'].apply(
                lambda x: x.strftime('%m/%d/%Y %H:%M:%S'))
            cols2return = ['TaskName', 'NextInvokeTimestamp', 'TimeToNextInvoke', 'NextInvokeUTC']
            return json.dumps({'rows': [list(row) for (idx, row) in task_schedule_df[cols2return].iterrows()]})
        # endregion

        # region logs
        if name == 'viper-get-logs-ajax':
            last_update_utc = int(i['last_update_utc']) if 'last_update_utc' in i.keys() else 0
            df = viper_dbutils.ViperDB.get_viper_logs_df(
                last_update_utc)  # ['Timestamp','TimestampUTC','Definiteness','Category','ActionID','IsFatal','Message','LogID']

            df = df.sort_values(by='LogID', ascending=False).head(200)  # only take last 200 logs
            df['Timestamp'] = df['Timestamp'].apply(lambda x: x.strftime('%m/%d/%Y %H:%M:%S'))
            new_rows = [list(row) for (idx, row) in df.iterrows()]
            data_row = {'new_logs': new_rows}

            return json.dumps(data_row)
        # endregion

        # region positions' stocks metrics
        if name == 'viper-get-positions-stocks-metrics':
            df = viper_dbutils.ViperDB.get_stocks_refdata_df_UI()  # cols = ['Ticker','AggQty','PX_LAST','PX_YEST_CLOSE','HIGH_52WEEK','LOW_52WEEK','MOV_AVG_30D','MOV_AVG_5D']
            df = df[df['AggQty'] != 0].copy()
            port_value = viper_dbutils.ViperDB.get_port_value_df()  # ['SecMktCap','CashSpentOnTrades','CashFromBooks','PnL($)','AUM']
            aum = port_value['AUM'].iloc[0] if len(port_value) > 0 else None
            df['MktCap($)'] = df['AggQty'] * df['PX_LAST']
            df['PX_CHG_1D(%)'] = 100.0 * ((df['PX_LAST'] / df['PX_YEST_CLOSE']) - 1.0)
            df['MktCap(%)'] = 100.0 * (df['MktCap($)'] / aum)
            df['MTM_1D_CHG(bps)'] = df['MktCap(%)'] * df['PX_CHG_1D(%)']
            cols2return = ['Ticker', 'AggQty', 'PX_CHG_1D(%)', 'PX_LAST', 'PX_YEST_CLOSE', 'MktCap($)', 'MktCap(%)',
                           'MTM_1D_CHG(bps)']

            df[['AggQty', 'MktCap($)', 'MktCap(%)', 'MTM_1D_CHG(bps)']] = df[
                ['AggQty', 'MktCap($)', 'MktCap(%)', 'MTM_1D_CHG(bps)']].fillna(0)
            df = df[cols2return].fillna('')

            return json.dumps({'rows': [list(row) for (idx, row) in df[cols2return].fillna('').iterrows()]})
        # endregion

        # region misc table
        if name == 'viper-get-misc-table':
            midnight_utc = (datetime.datetime.strptime(datetime.datetime.now().strftime('%m/%d/%Y') + ' 00:00:00',
                                                       '%m/%d/%Y %H:%M:%S') - datetime.datetime(1970, 1,
                                                                                                1)).total_seconds() * 1000
            one_hour_ago_utc = ((datetime.datetime.now() - datetime.timedelta(minutes=60)) - datetime.datetime(1970, 1,
                                                                                                               1)).total_seconds() * 1000
            num_calls_today = viper_dbutils.ViperDB.get_api_calls_used(midnight_utc)
            num_calls_last_hour = viper_dbutils.ViperDB.get_api_calls_used(one_hour_ago_utc)
            rows = [
                ('API invokes today', "{:,}".format(num_calls_today)),
                ('API invokes hour', "{:,}".format(num_calls_last_hour))
            ]
            df = pd.DataFrame(columns=['Component', 'Description'], data=rows)
            return json.dumps({'rows': [list(row) for (idx, row) in df.iterrows()]})
        # endregion

        if name == 'viper-dashboard':
            title = 'Viper'
            content = viper_htmlutils.viper_dashboard()

        if name == 'ma-index-dashboard':
            title = 'Risk Indices'
            content = viper_htmlutils.live_mna_index_page()

        if name == 'hist-ma-index-dashboard':
            title = 'Historical Index Report'
            content = viper_htmlutils.mna_index_historical_report()

        if name == 'spec-ma-index-dashboard':
            title = 'Risk Indices'
            content = viper_htmlutils.live_spec_mna_index_page()

        if name == 'mna-verifier':
            html = viper_htmlutils.mna_human_verifier_component_html()
            js = viper_htmlutils.mna_human_verifier_component_js(scrollY='90%',
                                                                 ajax_refresh_rate='1*60*60*1000')  # every 1 hr
            content = html + "<script>" + js + "</script>"

        return render.index(name, content, title=title)

    def POST(self, name):
        content = ""
        i = web.input(name=None)
        if name == 'viper-post-verified-mna-universe':
            try:
                data_str = web.data()
                data_json = json.loads(data_str)
                rows = [(r['ActionID'], r['Announced Date'],
                         r['TgtTicker'], r['AcqTicker'], r['PaymentType'],
                         r['Cash/sh'], r['Stock/sh'], r['ExpectedCompletion'],
                         r['IsExcluded'], r['IsVerified']) for r in data_json]
                cols = ['ActionID', 'Announced Date',
                        'TgtTicker', 'AcqTicker', 'PaymentType',
                        'Cash/sh', 'Stock/sh', 'ExpectedCompletion',
                        'IsExcluded', 'IsVerified']
                df = pd.DataFrame(columns=cols, data=rows)
                add_df = df[df['IsVerified'] == 'true'].copy();
                del add_df['IsVerified']

                if len(add_df[add_df['TgtTicker'] == '']) > 0: raise Exception('empty target ticker(s)')
                if len(add_df[add_df['PaymentType'] == '']) > 0: raise Exception('empty payment type(s)')
                if len(add_df[add_df['ExpectedCompletion'] == '']) > 0: raise Exception(
                    'empty expected completion date(s)')
                if len(add_df[add_df['Announced Date'] == '']) > 0: raise Exception('empty announced date(s)')

                date_cols = ['Announced Date', 'ExpectedCompletion']
                for dc in date_cols: add_df[dc] = add_df[dc].apply(lambda x: pd.to_datetime(x).strftime('%Y-%m-%d'))

                float_cols = ['Cash/sh', 'Stock/sh']
                for fc in float_cols: add_df[fc] = add_df[fc].apply(lambda x: 'NULL' if x == '' else x)

                viper_dbutils.ViperDB.replace_into_verified_mna_universe(
                    [tuple(row) for (idx, row) in add_df.iterrows()])

                db_df = viper_dbutils.ViperDB.get_mna_verified_universe()
                remove_df = df[df['IsVerified'] == 'false'].copy()
                remove_df = pd.merge(remove_df, db_df[['ActionID']], how='inner', on='ActionID')
                viper_dbutils.ViperDB.unverify_actions_mna_universe(list(remove_df['ActionID']))
                return 'success'
            except Exception as e:
                return 'error: ' + e.message


if __name__ == "__main__":
    app = web.application(urls, globals())
    app.run()
