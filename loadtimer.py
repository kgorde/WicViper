__author__ = 'pgrimberg'
import json
from viper_indexes.index_math import IndexMath

import datetime
import viper_dbutils
import pandas as pd


before = datetime.datetime.now()
##################################
from bbg_api import bwrapper
from bd_calendar import BDCalendar
bbg = bwrapper()
cdr = BDCalendar()
df = pd.read_csv("c:\\1\\query_df.csv")
df = df[['ISIN','StartDate','EndDate']].copy()
df['StartDate'] = df['StartDate'].apply(lambda x: pd.to_datetime(x))
df['EndDate'] = df['EndDate'].apply(lambda x: pd.to_datetime(x))

rows =[]
for (_,r) in df.iterrows():
    ISIN = r['ISIN'] #+ ' Equity'
    try:
        print('processing ' + ISIN)
        start_dt_yyyymmdd = r['StartDate'].strftime('%Y%m%d')
        end_dt_yyyymmdd = r['EndDate'].strftime('%Y%m%d')
        isin2fld2ts_df = bbg.get_histdata_fields([ISIN], ['PX_LAST','PX_OPEN'], {},
                                start_dt_yyyymmdd,end_dt_yyyymmdd,
                                float_fields=['PX_LAST','PX_OPEN'],
                                periodicityAdjustment=None,
                                periodicitySelection=None,
                                currency='USD',
                                override_option=None,
                                pricing_option=None,
                                non_trading_day_fill_option='NON_TRADING_WEEKDAYS',
                                non_trading_day_fill_method='PREVIOUS_VALUE',
                                max_data_points=None,
                                return_eids=None,
                                return_relative_date=None,
                                adjustment_normal=None,
                                adjustment_abnormal=None,
                                adjustment_split=None,
                                adjustment_follow_DPDF=True,
                                calendar_code_override=None,
                                calendar_code_overrides=None,
                                cdr_opt=None)
        px_last = isin2fld2ts_df[ISIN]['PX_LAST'].reset_index().rename(columns={'index':'Date',0:'PX_LAST'})
        px_open = isin2fld2ts_df[ISIN]['PX_OPEN'].reset_index().rename(columns={'index':'Date',0:'PX_OPEN'})
        isin_px_df = pd.merge(px_last, px_open, how='outer', on='Date')
        rows += [(r2['Date'],ISIN,r2['PX_LAST'],r2['PX_OPEN']) for (__,r2) in isin_px_df.iterrows()]
    except:
        print('failed ' + ISIN)
print("saving to excel file")
px_db = pd.DataFrame(columns=['Date','ISIN','PX_LAST','PX_OPEN'], data=rows)
px_db.to_csv("C:\\1\\BACKTEST_px_db.csv")
print('done')



##################################
after = datetime.datetime.now()
print((after-before).microseconds/1e6)

