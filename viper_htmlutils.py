__author__ = 'pgrimberg'

def dashboard_line(args_list,height='10%'):
	h = "<div style=\"width: 100%;\"> \r\n"
	for arg in args_list:
		h += "<div style=\"width: "+arg[1]+";height: "+height+"; float: left; border:3px solid black;\"> \r\n"
		h += arg[0]
		h += "</div> \r\n"
	h += "</div>"
	return h

class ColumnAttr(object):
	def __init__(self, is_number=True, thousand_separator=',',decimal_separator='.', rounding=2, prefix='', suffix='', is_centered=True, is_colored=False, is_visible=True):
		self.is_number = is_number
		self.thousand_separator = thousand_separator
		self.rounding = str(rounding) if rounding is not None else 2
		self.decimal_separator = decimal_separator
		self.prefix = prefix
		self.suffix = suffix
		self.is_centered = is_centered
		self.is_colored = is_colored
		self.is_visible = is_visible

def generate_expandable_formatter(cols, cln2attrs):
	js = "function format_expandable(colName, val) { \r\n"
	for col in cln2attrs:
		if col not in cols: continue
		js += "if (colName == '"+str(col)+"') { \r\n"

		if not cln2attrs[col].is_number:
			if cln2attrs[col].is_centered:
				js += "\t return '<center>'+val+'</center>' \r\n"
			else:
				js += "\t return val; \r\n"

			js += "}\r\n"
			continue

		js += "\t if (isNaN(val*1)) { return val; } \r\n"
		js += "\t if (val == '') { return val; } \r\n"
		js += "\t res = $.fn.dataTable.render.number('"+cln2attrs[col].thousand_separator+"', '"+cln2attrs[col].decimal_separator+"', "+cln2attrs[col].rounding+", '"+cln2attrs[col].prefix+"' , '"+cln2attrs[col].suffix+"' ).display(Math.round(val*Math.pow(10,"+cln2attrs[col].rounding+"))/Math.pow(10,"+cln2attrs[col].rounding+")); \r\n"
		js += (" \t if(val>=0){res = '<font color=\"green\">'+res+'</font>'} else {res = '<font color=\"red\">'+res+'</font>'} \r\n" if cln2attrs[col].is_colored else "")
		js += " \t res = '<center>'+res+'</center>' \r\n" if cln2attrs[col].is_centered else ""
		js += " \t return res; \r\n"
		js += "}\r\n"
	js += "return 'error'; \r\n"
	js += "} \r\n"
	return js
def generate_columnDefs(cols, cln2attrs):
	js = "\"columnDefs\": [   \r\n"
	for col in cln2attrs:
		if col not in cols: continue
		idx = cols.index(col)

		if not cln2attrs[col].is_visible:
			js += "\t {\"targets\": ["+str(idx)+"], \"visible\": false },\r\n"
			continue

		if not cln2attrs[col].is_number:
			if cln2attrs[col].is_centered:
				js += "{\r\n"\
					        "\t\t\t\"targets\": ["+str(idx)+"], \r\n" \
                            "\t\t\t \"render\": function ( data, type, full, meta ) { if(type != 'display') { return data } else {return '<center>'+data+'</center>'} } \r\n" \
                        "\t},\r\n"
			continue

		js += "{\r\n"
		js += "\t\t \"targets\": [" + str(idx) + "],\r\n"
		js += "\t\t \"render\": function ( data, type, full, meta ) { \r\n" \
				"\t\t\t if(type != 'display') { return data }; \r\n" \
				"\t\t\t if(isNaN(data*1)) { return data } \r\n" \
				"\t\t\t if(data == '') { return data } \r\n" \
				"\t\t\t res = $.fn.dataTable.render.number('"+cln2attrs[col].thousand_separator+"', '"+cln2attrs[col].decimal_separator+"', "+cln2attrs[col].rounding+", '"+cln2attrs[col].prefix+"' , '"+cln2attrs[col].suffix+"' ).display(Math.round(data*Math.pow(10,"+cln2attrs[col].rounding+"))/Math.pow(10,"+cln2attrs[col].rounding+")); \r\n" \
				  +("\t\t\t if(data>=0){res = '<font color=\"green\">'+res+'</font>'} else {res = '<font color=\"red\">'+res+'</font>'} \r\n" if cln2attrs[col].is_colored else "")+\
				  ("\t\t\t res = '<center>'+res+'</center>' \r\n" if cln2attrs[col].is_centered else "")+\
				"\t\t\t return res \r\n" \
			"\t\t } \r\n"
		js += "},\r\n"
	js = js[:-3] #remove last ,
	js += "] \r\n "


	return js

def high_level_stats_component_html():
	id = "high_level_stats_table"
	cols = ['NumOrdersPlaced','NumOrdersCompleted','NumFills','GrossDollarOrders','GrossShareOrders','GrossDollarsFilled',
		        'TotalPctFill','DollarsWaitingForFills','TotCashSpent($)','ShortMktCap($)','LongMktCap($)','NetMktCap($)',
		       'CashSeeded($)','CashBalance','PortfolioValue($)','PnL($)','CumulativePnL(bps)']

	thead = "\r\n".join(["<th>"+c+"</th>" for c in cols])
	html = ""
	html += "<div id=\"ViperHighLevelStatsTopDiv\"></div>"
	html += "<table id=\""+id+"\" class=\"display\" cellspacing=\"0\" width=\"100%\"> \r\n"\
			"<thead><tr>"+thead+"</tr></thead> \r\n"\
			"<tbody></tbody> \r\n"\
			"</table> "
	return html
def high_level_stats_component_js(scrollY='600px', ajax_refresh_rate='10*1000'):
	id = "high_level_stats_table"

	ajax_url = "viper-get-high-level-stats-ajax"

	cols = ['NumOrdersPlaced','NumOrdersCompleted','NumFills','GrossDollarOrders','GrossShareOrders','GrossDollarsFilled',
		        'TotalPctFill','DollarsWaitingForFills','TotCashSpent($)','ShortMktCap($)','LongMktCap($)','NetMktCap($)',
		       'CashSeeded($)','CashBalance','PortfolioValue($)','PnL($)','CumulativePnL(bps)']

	net_mktval_idx = cols.index('NetMktCap($)')
	cash_balance_idx = cols.index('CashBalance')
	aum_index = cols.index('PortfolioValue($)')

	#region col2attr
	col2attrs = {
		'NumOrdersPlaced': ColumnAttr(rounding=0,is_colored=True),
		'NumOrdersCompleted': ColumnAttr(rounding=0,is_colored=True),
		'NumFills': ColumnAttr(rounding=0,is_colored=True),
		'GrossDollarOrders': ColumnAttr(rounding=0,prefix='$',is_colored=True),
		'GrossShareOrders': ColumnAttr(rounding=0,prefix='$',is_colored=True),
		'GrossDollarsFilled': ColumnAttr(rounding=0,prefix='$',is_colored=True),
		'TotalPctFill': ColumnAttr(rounding=2,suffix='%',is_colored=True),
		'DollarsWaitingForFills': ColumnAttr(rounding=0,prefix='$',is_colored=True),
		'TotCashSpent($)': ColumnAttr(rounding=0,prefix='$',is_colored=True),
		'ShortMktCap($)': ColumnAttr(rounding=0,prefix='$',is_colored=True),
		'LongMktCap($)': ColumnAttr(rounding=0,prefix='$',is_colored=True),
		'NetMktCap($)': ColumnAttr(rounding=0,prefix='$',is_colored=True),
		'CashSeeded($)': ColumnAttr(rounding=0,prefix='$',is_colored=True),
		'CashBalance': ColumnAttr(rounding=0,prefix='$',is_colored=True),
		'PortfolioValue($)': ColumnAttr(rounding=0,prefix='$',is_colored=True),
		'PnL($)': ColumnAttr(rounding=0,prefix='$',is_colored=True),
		'CumulativePnL(bps)': ColumnAttr(rounding=2,suffix='bps',is_colored=True),

	}
	#endregion

	columnDefs = generate_columnDefs(cols, col2attrs)
	js = \
		 "var hls_t = $('#"+id+"').DataTable({ \r\n"\
					"\"scrollY\":        \""+scrollY+"\", \r\n"\
					"\"scrollCollapse\": true, \r\n"\
					"\"paging\":         false, \r\n"\
					"\"searching\": false, \r\n"\
					"\"bInfo\": false, \r\n"\
					+columnDefs+\
				  "}); \r\n"\
		"function requestHighLevelStatisticsData() { \r\n"\
			"$.ajax({ \r\n"\
					"url:'"+ajax_url+"', \r\n"\
						"success: function(response) { \r\n"\
								    "hls_t.clear().draw(false); \r\n"\
								    "resp_json = JSON.parse(response); \r\n"\
								    "var arr = resp_json['rows'] \r\n"\
								    "var rows = []; \r\n"\
									 "for (var i = 0; i < arr.length; i++){ rows.push(arr[i]); \r\n " \
	                             "} \r\n"\
							     "hls_t.rows.add(rows).draw();   \r\n"\
		                         "$('#ViperHighLevelStatsTopDiv').html('<center><b><font face=\"verdana\" size=\"5\">Portfolio: $'+Math.floor(arr[0]["+str(net_mktval_idx)+"]).toLocaleString()+' &nbsp&nbsp&nbsp Cash: $'+Math.floor(arr[0]["+str(cash_balance_idx)+"]).toLocaleString()+' &nbsp&nbsp&nbsp AUM: $'+Math.floor(arr[0]["+str(aum_index)+"]).toLocaleString()+' </font></b></center><br>'); \r\n "\
					"}, \r\n"\
					"complete: function (response) { }, \r\n"\
				   "error: function () { logs_t.row.add(['<center>'+new Date().toLocaleString().replace(', ',' ')+'</center>','<center>Failed loading high-level stats</center>']).draw( false ); } \r\n"\
			"}); \r\n"\
			"setTimeout(requestHighLevelStatisticsData, "+ajax_refresh_rate+"); \r\n"\
		"} \r\n"\
		"requestHighLevelStatisticsData(); "

	return js

def fills_component_html():
	id = "fill_table"
	cols = ['Ticker','FillID','OrderID','FillTime','FillTimeUTC','NumShares','ExecPX']
	thead = "\r\n".join(["<th>"+c+"</th>" for c in cols])

	html = "<br><center><b>Fills</b></center><hr>"
	html += "<table id=\""+id+"\" class=\"display\" cellspacing=\"0\" width=\"100%\"> \r\n"\
		"<thead><tr>"+thead+"</tr></thead> \r\n"\
		"<tbody></tbody> \r\n"\
	"</table> "

	return html
def fills_component_js(scrollY='600px', ajax_refresh_rate = '10*1000'):
	id = "fill_table"

	cols = ['Ticker','FillID','OrderID','FillTime','FillTimeUTC','NumShares','ExecPX']

	#region col2attrs
	cln2attrs = {
		'Ticker':ColumnAttr(is_number=False),
		'FillID': ColumnAttr(rounding=0),
		'OrderID': ColumnAttr(rounding=0),
		'FillTime': ColumnAttr(is_number=False),
		'FillTimeUTC': ColumnAttr(is_visible=False),
		'NumShares': ColumnAttr(is_number=False),
		'ExecPX': ColumnAttr(is_number=False)
	}
	#endregion

	columnsDef = generate_columnDefs(cols,cln2attrs)

	ajax_url = "viper-get-fills-ajax?last_utc="
	js = "var last_fill_time_utc = 0;\r\nvar ft = $('#"+id+"').DataTable({\r\n    " \
														   "\"scrollY\":        \""+scrollY+"\",\r\n    " \
															"\"scrollCollapse\": true,\r\n    " \
															"\"paging\":         true, \r\n  " \
	                                                        + columnsDef +\
                                                    " \r\n });" \
	"\r\n\r\nfunction requestFillsData() {\r\n  \t$.ajax({\r\n            " \
	"url:'"+ajax_url+"'+last_fill_time_utc,\r\n      \t\t\t      			" \
	 "success: function(response) { \r\n"\
			  "resp_json = JSON.parse(response); \r\n"\
			  "var fills_arr = resp_json['new_fills'] \r\n"\
			  "var new_fills_rows=[]; \r\n"\
			  "for (var i = 0; i < fills_arr.length; i++){ \r\n"\
				  "var fill = fills_arr[i]; \r\n"\
				  "var fill_utc = fill[4]; \r\n"\
				  "if (fill_utc > last_fill_time_utc) { last_fill_time_utc = fill_utc; } \r\n"\
				  "new_fills_rows.push(fill); \r\n"\
			  "} \r\n"\
			 "ft.rows.add(new_fills_rows).draw(false); \r\n"\
			"},\r\n            complete: function (response) {               \r\n           },\r\n           error: function () { \r\n           \t\tlogs_t.row.add(['<center>'+new Date().toLocaleString().replace(', ',' ')+'</center>','<center>Failed loading fills</center>']).draw( false ); \r\n           }\r\n    });    \r\n    setTimeout(requestFillsData, "+ajax_refresh_rate+"); \r\n}\r\n\r\nrequestFillsData()\r\n\r\n"
	return js

def orders_component_html():
	id="orders_table"
	cols = ['OrderID','BuySell','OrderType','CreationTime','CreationTimeUTC',
			'Ticker','ISIN','ActionId','Tgt/Acq','SharesSought','PX','DollarsAllocated','CashWithdrew',
			'Status','CompletionTime','CompletionTimeUTC','CompletionReason',
			'TotalQty','AvgCostBasis','CashSpent','DollarsLeft',
			'NumFills','MinFillPX','MaxFillPX',
			'PctComplete','LastUpdateUTC']

	thead = "\r\n".join(["<th>"+c+"</th>" for c in cols])
	html = "<br><center><b>Orders</b></center><hr>"
	#"<tfoot><tr>"+thead+"</tr></tfoot> \r\n"\
	html += "<table id=\""+id+"\" class=\"display\" cellspacing=\"0\" width=\"100%\"> \r\n"\
		"<thead><tr>"+thead+"</tr></thead> \r\n"\
		"<tbody></tbody> \r\n"\
	"</table> "
	return html
def orders_component_js(scrollY='600px', ajax_refresh_rate='10*1000'):
	id = "orders_table"
	cols = ['OrderID','BuySell','OrderType','CreationTime','CreationTimeUTC',
			'Ticker','ISIN','ActionId','Tgt/Acq','SharesSought','PX','DollarsAllocated','CashWithdrew',
			'Status','CompletionTime','CompletionTimeUTC','CompletionReason',
			'TotalQty','AvgCostBasis','CashSpent','DollarsLeft',
			'NumFills','MinFillPX','MaxFillPX',
			'PctComplete','LastUpdateUTC']

	#region col2attrs
	cln2attrs = {
		'OrderID':ColumnAttr(rounding=0),
		'BuySell': ColumnAttr(is_number=False),
		'OrderType': ColumnAttr(is_number=False),
		'CreationTime': ColumnAttr(is_number=False),
		'CreationTimeUTC': ColumnAttr(is_number=False,is_visible=False),
		'Ticker': ColumnAttr(is_number=False),
		'ISIN': ColumnAttr(is_number=False),
		'ActionId': ColumnAttr(is_number=False),
		'Tgt/Acq': ColumnAttr(is_number=False),
		'SharesSought': ColumnAttr(rounding=0),
		'PX': ColumnAttr(rounding=2),
		'DollarsAllocated': ColumnAttr(rounding=0,prefix='$'),
		'CashWithdrew': ColumnAttr(rounding=0,prefix='$'),
		'Status':ColumnAttr(is_number=False),
		'CompletionTime':ColumnAttr(is_number=False),
		'CompletionTimeUTC':ColumnAttr(is_number=False, is_visible=False),
		'CompletionReason':ColumnAttr(is_number=False),
		'TotalQty': ColumnAttr(rounding=0),
		'AvgCostBasis': ColumnAttr(rounding=2),
		'CashSpent': ColumnAttr(rounding=0, is_colored=True),
		'DollarsLeft': ColumnAttr(rounding=0,prefix='$'),
		'NumFills': ColumnAttr(rounding=0),
		'MinFillPX': ColumnAttr(rounding=2),
		'MaxFillPX': ColumnAttr(rounding=2),
		'PctComplete': ColumnAttr(rounding=0, suffix='%'),
		'LastUpdateUTC': ColumnAttr(is_number=False, is_visible=False)
	}
	#endregion

	ajax_url = "viper-get-orders-ajax"

	columnsDef = generate_columnDefs(cols,cln2attrs)

	js = \
		"var max_creation_utc = 0; \r\n" \
		"var max_update_utc = 0; \r\n" \
		"var orders_ids_seen = []; \r\n"\
		"var MAX_ORDER_IDS_TO_STORE = 2000; \r\n"\
		 "var ot = $('#"+id+"').DataTable({ \r\n"\
					"\"scrollY\":        \""+scrollY+"\", \r\n"\
					"\"scrollCollapse\": true, \r\n"\
					"\"paging\":         false, \r\n"\
					+ columnsDef +\
				  " }); \r\n"\
		"function requestOrdersData() { \r\n"\
			"$.ajax({ \r\n"\
					"url:'"+ajax_url+"?last_creation_utc='+max_creation_utc+'&last_update_utc='+max_update_utc, \r\n"\
						"success: function(response) { \r\n"\
								  "resp_json = JSON.parse(response); \r\n"\
								  "var new_orders_rows = [] \r\n"\
								  "var order_arr = resp_json['new_orders'] \r\n"\
								  "for (var i = 0; i < order_arr.length; i++){ \r\n"\
									  "var ord = order_arr[i]; \r\n"\
									  "var ord_id = ord[0]; \r\n"\
									  "if (orders_ids_seen.indexOf(ord_id) >= 0) { continue; } \r\n "\
									  "orders_ids_seen.push(ord_id); \r\n"\
									  "if (ord[4] > max_creation_utc) { max_creation_utc = ord[4]; } \r\n"\
									  "new_orders_rows.push(ord); \r\n"\
							  "} \r\n"\
							  "orders_ids_seen = orders_ids_seen.slice(Math.max(orders_ids_seen.length - MAX_ORDER_IDS_TO_STORE,0)); \r\n"\
							  "ot.rows.add(new_orders_rows).draw(false);   \r\n"\
							  "var modify_arr = resp_json['modify_orders'] \r\n"\
							  "ot.rows().every( function ( rowIdx, tableLoop, rowLoop ) { \r\n"\
									"var data = this.data(); \r\n"\
									"ord_id = data[0] \r\n"\
									"if (ord_id in modify_arr) { \r\n"\
										"data = modify_arr[ord_id] \r\n"\
										"this.data(data) \r\n"\
										"if (data[24] > max_update_utc) { max_update_utc = data[24] } \r\n"\
									"} \r\n"\
							"}); \r\n"\
					"}, \r\n"\
					"complete: function (response) { \r\n"\
				   "}, \r\n"\
					"error: function () { logs_t.row.add(['<center>'+new Date().toLocaleString().replace(', ',' ')+'</center>','<center>Failed loading orders</center>']).draw( false ); } \r\n"\
			"}); \r\n"\
			"setTimeout(requestOrdersData, "+ajax_refresh_rate+"); \r\n"\
		"} \r\n"\
		"setTimeout(requestOrdersData, 3000); \r\n"
	return js #"requestOrdersData(); "

def positions_component_html():
	id = "positions_table"
	cols = ['','ActionID','TgtTicker','Deal Status','Payment Type','Deal Currency','AcquirerCCY','Cash/sh','Stock/sh',
	        'Cash Value',
	        'RumorEffectiveStart','RumorExpiration','DaysToExpiration',
	        'AnncDate','ExpCompDate','AnncDealSize','DealState',
	        'TotDollarsUnfilled','TotCashSpent', 'TotTgtWeight(bps)',
	        'TotMktCap','TotMktCapPctOfAUM','TotPnL($)','ExpandableRowsJSON']

	thead = "\r\n".join(["<th>"+c+"</th>" for c in cols])
	html = "<br><center><b>Positions</b></center><hr>"
	html += "<table id=\""+id+"\" class=\"display\" cellspacing=\"0\" width=\"100%\"> \r\n"\
		"<thead><tr>"+thead+"</tr></thead> \r\n"\
		"<tbody></tbody> \r\n"\
	"</table> "
	return html
def positions_component_js(scrollY='600px', ajax_refresh_rate='100*1000'):
	ajax_url = "viper-get-positions-ajax"
	id = "positions_table"
	cols = ['','ActionID','TgtTicker','Deal Status','Payment Type','Deal Currency','AcquirerCCY','Cash/sh','Stock/sh',
	        'Cash Value','RumorEffectiveStart','RumorExpiration','DaysToExpiration',
	        'AnncDate','ExpCompDate','AnncDealSize','DealState',
	        'TotDollarsUnfilled','TotCashSpent', 'TotTgtWeight(bps)',
	        'TotMktCap','TotMktCapPctOfAUM','TotPnL($)','ExpandableRowsJSON']


	deal_status_idx = cols.index('DealState')

	#region col2attrs
	cln2attrs = {
		'' : ColumnAttr(is_number=False,is_centered=False),
		'ActionID': ColumnAttr(is_number=False),
		'TgtTicker': ColumnAttr(is_number=False),
		'Deal Status': ColumnAttr(is_number=False),
		#'Ticker': ColumnAttr(is_number=False),
		#'ISIN': ColumnAttr(is_visible=False),
		#'Tgt/Acq': ColumnAttr(is_number=False),
		'Payment Type': ColumnAttr(is_visible=False),
		'Deal Currency': ColumnAttr(is_visible=False),
		'AcquirerCCY': ColumnAttr(is_visible=False),
		'Cash/sh': ColumnAttr(rounding=3),
		'Stock/sh': ColumnAttr(rounding=4),
		'Cash Value': ColumnAttr(rounding=3),
		'RumorEffectiveStart':ColumnAttr(is_number=False),
        'RumorExpiration':ColumnAttr(is_number=False),
        'DaysToExpiration':ColumnAttr(rounding=0),
		'AnncDate': ColumnAttr(is_number=False),
		'ExpCompDate': ColumnAttr(is_number=False),
		'AnncDealSize': ColumnAttr(is_visible=False),
		'DealState': ColumnAttr(is_number=False),
		'TotDollarsUnfilled': ColumnAttr(rounding=0, prefix='$'),
		'TotCashSpent': ColumnAttr(rounding=0, prefix='$',is_colored=True),
		#'Beta': ColumnAttr(rounding=2,is_colored=True),
        #'HighVixBeta':ColumnAttr(rounding=2,is_colored=True),
		'TotTgtWeight(bps)': ColumnAttr(rounding=0,suffix='bps'),
		#'CurrWeight(bps)':ColumnAttr(rounding=0,suffix='bps'),
		#'Qty': ColumnAttr(rounding=0, is_colored=True),
		#'PX': ColumnAttr(rounding=3),
		'TotMktCap': ColumnAttr(rounding=0, is_colored=True),
		'TotMktCapPctOfAUM': ColumnAttr(rounding=2, is_colored=True,suffix='%'),
		'TotPnL($)':ColumnAttr(rounding=0,prefix='$',is_colored=True),
		'ExpandableRowsJSON': ColumnAttr(is_visible=False)
	}
	#endregion
	columnDefs = generate_columnDefs(cols, cln2attrs)

	#column definition for expandable rows
	expanded_cols = ['Ticker','ISIN','Tgt/Acq','DollarsUnfilled','CashSpent','Beta','HighVixBeta','TgtWeight(bps)',
	                 'CurrWeight(bps)','Qty',
	                 'PX','AvgCostBasis','ChgFromAvgCostBasis',
	                 'MktCap','MktCap','MktCapPctOfAUM','PnL($)']
	#region expanded_cols2attrs
	expanded_cols2attrs = {
		'Ticker': ColumnAttr(is_number=False),
		'ISIN': ColumnAttr(is_visible=False),
		'Tgt/Acq': ColumnAttr(is_number=False),
		'DollarsUnfilled': ColumnAttr(rounding=0, prefix='$'),
		'CashSpent': ColumnAttr(rounding=0, prefix='$',is_colored=True),
		'Beta': ColumnAttr(rounding=2,is_colored=True),
        'HighVixBeta':ColumnAttr(rounding=2,is_colored=True),
		'TgtWeight(bps)': ColumnAttr(rounding=0,suffix='bps'),
		'CurrWeight(bps)':ColumnAttr(rounding=0,suffix='bps'),
		'Qty': ColumnAttr(rounding=0, is_colored=True),
		'PX': ColumnAttr(rounding=3,prefix='$'),
		'AvgCostBasis': ColumnAttr(rounding=3,prefix='$'),
		'ChgFromAvgCostBasis': ColumnAttr(rounding=1,suffix='%'),
		'MktCap': ColumnAttr(rounding=0, is_colored=True),
		'MktCapPctOfAUM': ColumnAttr(rounding=2, is_colored=True,suffix='%'),
		'PnL($)':ColumnAttr(rounding=0,prefix='$',is_colored=True)
	}
	#endregion

	expandable_cols_def = " \"columns\": [ \r\n"
	expandable_cols_def += "{ \"className\": \"details-control\", \"orderable\": false, \"data\": null, \"defaultContent\": \"\" }, \r\n"
	expandable_cols_def += ", \r\n ".join(["{ \"data\": \""+c+"\" }" for c in cols if c != ''])
	expandable_cols_def += "]"

	expandable_cols_formatter = generate_expandable_formatter(expanded_cols, expanded_cols2attrs) # creates function format_expandable(colName, val)

	js = expandable_cols_formatter + \
		"function expand_row ( d ) { \r\n " \
			"var expanded_cols = " + str(expanded_cols) + " \r\n"\
			"var rj = JSON.parse(d.ExpandableRowsJSON); \r\n"\
			"var rows_arr = rj['expandable_rows_json'] \r\n"\
			"var table_html = '<table class=\"display\" >' \r\n"\
			"table_html += '<thead><tr>' \r\n"\
			"for (var i = 0; i < expanded_cols.length; i++ ) { table_html += '<th>'+expanded_cols[i]+'</th>' } \r\n"\
			"table_html += '</tr></thead>' \r\n"\
			"table_html += '<tbody>' \r\n"\
			"for (var i = 0; i < rows_arr.length; i++) { \r\n"\
				"table_html += '<tr>' \r\n"\
			  "for (var j =0; j < expanded_cols.length; j++) { table_html += '<td>'+format_expandable(expanded_cols[j],rows_arr[i][expanded_cols[j]])+'</td>' } \r\n"\
			  "table_html += '</tr>' \r\n"\
			"} \r\n"\
			"table_html += '</tbody>' \r\n"\
			"table_html += '</table>' \r\n"\
			"return table_html; \r\n"\
		"} \r\n "\
		 "var pos_t = $('#"+id+"').DataTable({ \r\n"\
						"\"scrollY\":        \""+scrollY+"\", \r\n"\
						"\"scrollCollapse\": true, \r\n"\
						"\"paging\":         false, \r\n"\
						+columnDefs+ ", \r\n"\
						+expandable_cols_def +\
				  "}); \r\n"\
		"$('#"+id+" tbody').on('click', 'td.details-control', function () { \r\n"\
		        "var tr = $(this).closest('tr'); \r\n"\
		        "var row = pos_t.row( tr ); \r\n"\
		        "if ( row.child.isShown() ) { \r\n"\
		            "row.child.hide(); \r\n"\
		            "tr.removeClass('shown'); \r\n"\
		        "} \r\n"\
		        "else {	 \r\n"\
		            "row.child( expand_row(row.data()) ).show(); \r\n"\
		            "tr.addClass('shown'); \r\n"\
		        "} \r\n"\
		    "} ); \r\n"\
		"function requestPositionsData() { \r\n"\
			"$.ajax({ \r\n"\
					"url:'"+ajax_url+"', \r\n"\
						"success: function(response) { \r\n"\
								  "pos_t.clear().draw(); \r\n"\
								  "resp_json = JSON.parse(response); \r\n"\
								  "var pos_arr = resp_json['positions'] \r\n"\
								  "var pos_rows = []; \r\n"\
								  "var exited_rows_idxs = []; \r\n"\
								  "var exiting_rows_idxs = []; \r\n"\
								  "var init_rows_idxs = []; \r\n"\
								  "for (var i = 0; i < pos_arr.length; i++){ \r\n"\
									  "var pos = pos_arr[i]; \r\n"\
									  "pos_rows.push(pos); \r\n"\
									  "if (pos['DealState'] == 'EXITED') { exited_rows_idxs.push(i); } \r\n"\
									  "if (pos['DealState'] == 'EXITING') { exiting_rows_idxs.push(i); } \r\n"\
									  "if (['INIT_STOCK_AND_CASH','INIT','INIT_LONG_LEG_DONE'].indexOf(pos['DealState']) >= 0) { init_rows_idxs.push(i); } \r\n"\
									"} \r\n"\
                                  "var posRowNodes = pos_t.rows.add(pos_rows).draw().nodes(); \r\n"\
								  "for (var j = 0; j < exited_rows_idxs.length; j++ ) { $('td', posRowNodes[exited_rows_idxs[j]]).css('background-color', '#e6e6e6' ) } \r\n"\
								  "for (var j = 0; j < exiting_rows_idxs.length; j++ ) { $('td', posRowNodes[exiting_rows_idxs[j]]).css('background-color', '#ffcccc' ) } \r\n"\
								  "for (var j = 0; j < init_rows_idxs.length; j++ ) { $('td', posRowNodes[init_rows_idxs[j]]).css('background-color', '#e6ffe6' ) } \r\n"\
					"}, \r\n"\
					"complete: function (response) { \r\n"\
				   "}, \r\n"\
				   "error: function () { logs_t.row.add(['<center>'+new Date().toLocaleString().replace(', ',' ')+'</center>','<center>Failed loading positions</center>']).draw( false ); } \r\n"\
			"}); \r\n"\
			"setTimeout(requestPositionsData, "+ajax_refresh_rate+"); \r\n"\
		"} \r\n"\
		"requestPositionsData(); "

	return js

def universe_component_html():
	id = "universe_table"
	cols = ['TimeLoaded','ActionID','AnncDate','ProposedDate','DealStatus','Tgt','Acq','CashValue','CurrPrem',
			'GrossSpread','AnncPrem','PctOwnPost','ExpectedComp','PaymentType','AnncTotalValue',
			'DealCCY','Cash/sh','Stock/sh','AcquirerCCY','TgtISIN','AcqISIN','TgtMktCap($bln)','TermFee/AnnTxnValue(%)']
			#'ACQ_PREV_3D_VWAP','TGT_PREV_3D_VWAP','TGT_BETA','TGT_STRESS_BETA']

	thead = "\r\n".join(["<th>"+c+"</th>" for c in cols])
	html = "<br><center><b>Universe</b></center><hr>"
	html += "<table id=\""+id+"\" class=\"display\" cellspacing=\"0\" width=\"100%\"> \r\n"\
		"<thead><tr>"+thead+"</tr></thead> \r\n"\
		"<tbody></tbody> \r\n"\
	"</table> "
	return html
def universe_component_js(scrollY='600px', ajax_refresh_rate='10*1000'):
	id = "universe_table"
	ajax_url = "viper-get-universe-ajax"

	cols = ['TimeLoaded','ActionID','AnncDate','ProposedDate','DealStatus','Tgt','Acq','CashValue','CurrPrem',
			'GrossSpread','AnncPrem','PctOwnPost','ExpectedComp','PaymentType','AnncTotalValue',
			'DealCCY','Cash/sh','Stock/sh','AcquirerCCY','TgtISIN','AcqISIN','TgtMktCap($bln)','TermFee/AnnTxnValue(%)']

	#region col2attrs
	cln2attrs = {
		'TimeLoaded': ColumnAttr(is_number=False),
		'ActionID': ColumnAttr(is_number=False),
		'AnncDate': ColumnAttr(is_number=False),
		'ProposedDate': ColumnAttr(is_number=False),
		'DealStatus': ColumnAttr(is_number=False),
		'Tgt': ColumnAttr(is_number=False),
		'Acq': ColumnAttr(is_number=False),
		'CashValue': ColumnAttr(rounding=3),
		'CurrPrem': ColumnAttr(rounding=2),
		'GrossSpread': ColumnAttr(rounding=2),
		'AnncPrem': ColumnAttr(rounding=2),
		'PctOwnPost': ColumnAttr(rounding=0),
		'ExpectedComp': ColumnAttr(is_number=False),
		'PaymentType': ColumnAttr(is_number=False),
		'AnncTotalValue': ColumnAttr(rounding=0),
		'DealCCY': ColumnAttr(is_number=False),
		'Cash/sh': ColumnAttr(rounding=3),
		'Stock/sh': ColumnAttr(rounding=3),
		'AcquirerCCY': ColumnAttr(is_number=False),
		'TgtISIN': ColumnAttr(is_visible=False),
		'AcqISIN': ColumnAttr(is_visible=False),
		'TgtMktCap($bln)': ColumnAttr(rounding=2),
		'TermFee/AnnTxnValue(%)': ColumnAttr(rounding=2)
	}
	#endregion

	columnDefs = generate_columnDefs(cols,cln2attrs)

	js = "var univ_table = $('#"+id+"').DataTable({ \r\n"\
				"\"scrollY\":        \""+scrollY+"\", \r\n"\
				"\"scrollCollapse\": true, \r\n"\
				"\"paging\":         false, \r\n"\
				+columnDefs+\
		" }); \r\n"\
		"function requestUniverseData() { \r\n"\
		"$.ajax({ \r\n"\
				"url:'"+ajax_url+"', \r\n"\
					"success: function(response) { \r\n"\
				  "resp_json = JSON.parse(response); \r\n"\
				  "var rows = resp_json['universe_rows']; \r\n"\
				  "univ_table.clear(); \r\n"\
				  "univ_rows_to_add = []; \r\n"\
				  "for (var i = 0; i < rows.length; i++){ univ_rows_to_add.push(rows[i]); } \r\n"\
				  "univ_table.rows.add(univ_rows_to_add).draw(); \r\n"\
				"}, \r\n"\
				"complete: function (response) {}, \r\n"\
			   "error: function () { logs_t.row.add(['<center>'+new Date().toLocaleString().replace(', ',' ')+'</center>','<center>Failed loading universe</center>']).draw( false ); } \r\n"\
		"}); \r\n"\
		"setTimeout(requestUniverseData, "+ajax_refresh_rate+"); \r\n"\
		"} \r\n"\
		"requestUniverseData() \r\n"
	return js

def pnl_chart_html():
	id = "viper_chart"
	html = "<div id=\""+id+"\" class=\"chart_container\"></div>"
	return html
def pnl_chart_js(title='Live P&L', ylabel='MktVal($)',ajax_refresh_rate='10*1000', height='30%'):
	id = "viper_chart"
	ajax_url = "viper-get-pnl-chart-ajax"
	js = \
		"var chart = new Highcharts.Chart({ \r\n"\
		"chart: { renderTo: '"+id+"' , defaultSeriesType: 'line', height: '"+height+"' }, \r\n"\
		"title: { text: '"+title+"' }, \r\n"\
		"tooltip: { crosshairs: true, shared: true }, \r\n"\
		"xAxis: { \r\n"\
				"type: 'datetime', \r\n"\
				"tickPixelInterval: 150, \r\n"\
				"maxZoom: 20 * 1000, \r\n"\
				"gridLineWidth: 1, \r\n"\
				"minPadding: 0.2, \r\n"\
				"maxPadding: 0.2, \r\n"\
				"maxZoom: 60 \r\n"\
		  "}, \r\n"\
		"yAxis: { \r\n"\
				"minPadding: 0.2, \r\n"\
				"maxPadding: 0.2, \r\n"\
				"title: { text: '"+ylabel+"', margin: 5 } \r\n"\
		"}, \r\n"\
		"exporting: { enabled: false }, \r\n"\
		"series: [{name: 'AUM', data: [] },{name: 'Portfolio', data: [], visible: false },{name: 'Cash', data: [], visible: false }] \r\n"\
		"}); \r\n"\
		"\r\n"\
		"\r\n"\
		"function requestPnlChartData() { \r\n"\
			"$.ajax({ \r\n"\
					"url:'"+ajax_url+"', \r\n"\
						"success: function(response) { \r\n"\
							 "resp_json = JSON.parse(response); \r\n"\
							 "var curr_utc = resp_json['utc']; \r\n"\
                             "chart.series[0].addPoint([curr_utc,resp_json['aum']], false, false); \r\n"\
                             "chart.series[1].addPoint([curr_utc, resp_json['portfolio']], false, false); \r\n"\
                             "chart.series[2].addPoint([curr_utc, resp_json['cash']], false, false); \r\n"\
					  "chart.redraw() \r\n"\
					"}, \r\n"\
					"complete: function (response) {}, \r\n"\
				   "error: function () { logs_t.row.add(['<center>'+new Date().toLocaleString().replace(', ',' ')+'</center>','<center>Failed updating chart</center>']).draw( false ); } \r\n"\
			"}); \r\n"\
			"setTimeout(requestPnlChartData, "+ajax_refresh_rate+"); \r\n"\
		"} \r\n"\
		"requestPnlChartData() \r\n"

	return js

def webpage_logs_component_html():
	id = "logs_table"
	cols = ['Timestamp','Message']
	thead = "\r\n".join(["<th>"+c+"</th>" for c in cols])
	html = "<br><center><b>Web Logs</b></center><hr>"
	html += "<table id=\""+id+"\" class=\"display\" cellspacing=\"0\" width=\"100%\"> \r\n"\
		"<thead><tr>"+thead+"</tr></thead> \r\n"\
		"<tbody></tbody> \r\n"\
	"</table> "
	return html
def webpage_logs_component_js(scrollY='600px'):
	id = "logs_table"
	js = "var logs_t = $('#"+id+"').DataTable({ \r\n"\
				"\"scrollY\":        \""+scrollY+"\", \r\n"\
				"\"scrollCollapse\": true, \r\n"\
				"\"paging\":         false \r\n"\
		"}); \r\n"
	return js

def viper_logs_component_html():
	id = "viper_logs_table"
	cols = ['Timestamp','TimestampUTC','Definiteness','Category','ActionID','IsFatal','Message']
	thead = "\r\n".join(["<th>"+c+"</th>" for c in cols])
	html = "" #"<br><center><b>Viper Logs</b></center><hr>"
	#"<tfoot><tr>"+thead+"</tr></tfoot> \r\n"\
	html += "<table id=\""+id+"\" class=\"display\" cellspacing=\"0\" width=\"100%\"> \r\n"\
		"<thead><tr>"+thead+"</tr></thead> \r\n"\
		"<tbody></tbody> \r\n"\
	"</table> "
	return html
def viper_logs_component_js(scrollY='600px', ajax_refresh_rate='10*1000'):
	id = "viper_logs_table"
	cols = ['Timestamp','TimestampUTC','Definiteness','Category','ActionID','IsFatal','Message']

	#region col2attrs
	cln2attrs = {
		'Timestamp':ColumnAttr(is_number=False),
		'TimestampUTC':ColumnAttr(is_number=True,is_visible=False),
		'Definiteness': ColumnAttr(is_number=False),
		'Category': ColumnAttr(is_number=False),
		'ActionID': ColumnAttr(is_number=False),
		'IsFatal': ColumnAttr(is_number=False,is_visible=False),
		'Message': ColumnAttr(is_number=False),
	}
	#endregion

	ajax_url = "viper-get-logs-ajax"

	columnsDef = generate_columnDefs(cols,cln2attrs)

	js = \
		"var hidden_categories = ['BBG_API']; \r\n"\
		"var last_log_utc = 0; \r\n" \
		"var log_ids_seen = []; \r\n"\
		"var MAX_LOG_IDS_TO_STORE = 2000; \r\n"\
		 "var vlt = $('#"+id+"').DataTable({ \r\n"\
					"\"scrollY\":        \""+scrollY+"\", \r\n"\
					"\"scrollCollapse\": true, \r\n"\
					"\"paging\":         false, \r\n"\
					"\"order\": [[1,\"desc\"]], \r\n "\
					+ columnsDef +\
				  " }); \r\n"\
		"function getLogColor(log_ctg) { \r\n"\
	        "switch(log_ctg){ \r\n"\
	            "case 'ERROR': return 'Red' \r\n"\
				"case 'TRADE_PASSED': return 'Teal' \r\n"\
				"case 'SYSTEM': return 'LightSteelBlue' \r\n"\
				"case 'RISK_MGMT': return 'Purple' \r\n"\
				"case 'NEWS_ALERT': return 'Red' \r\n"\
				"case 'NOT_IMPLEMENTED': return 'Grey' \r\n"\
				"case 'BBG_API': return '#4e1287' \r\n"\
				"case 'TRADE_LOG': return 'DarkGoldenRod' \r\n"\
				"case 'SCHEDULED_TASK': return '#4d1919' \r\n"\
				"case 'VIPER_MKTDATA_PIPE': return '#003366' \r\n"\
				"case 'MATCHING_ENGINE': return 'Purple' \r\n"\
				"return 'Black' \r\n"\
			"} \r\n"\
		"} \r\n"\
		"function requestViperLogsData() { \r\n"\
			"$.ajax({ \r\n"\
					"url:'"+ajax_url+"?last_update_utc='+last_log_utc, \r\n"\
						"success: function(response) { \r\n"\
								  "resp_json = JSON.parse(response); \r\n"\
								  "var new_logs_rows = [] \r\n"\
								  "var new_logs_colors = [] \r\n"\
								  "var logs_arr = resp_json['new_logs'] \r\n"\
								  "for (var i = 0; i < logs_arr.length; i++){ \r\n"\
									  "var lg = logs_arr[i]; \r\n"\
									  "var lg_id = lg[7]; \r\n"\
									  "var lg_utc = lg[1]; \r\n"\
									  "var lg_ctg = lg[3]; \r\n"\
									  "if (log_ids_seen.indexOf(lg_id) >= 0) { continue; } \r\n "\
									  "log_ids_seen.push(lg_id); \r\n"\
									  "if (lg_utc > last_log_utc) { last_log_utc = lg_utc; } \r\n"\
									  "if (hidden_categories.indexOf(lg_ctg) >= 0) { continue; } \r\n "\
									  "new_logs_rows.push(lg); \r\n"\
									  "new_logs_colors.push(getLogColor(lg_ctg)); \r\n"\
							  "} \r\n"\
							  "var rowNodes = vlt.rows.add(new_logs_rows).draw(false).nodes();   \r\n"\
							  "for (var i = 0; i < rowNodes.length; i++) { \r\n"\
									"$('td', rowNodes[i]).css('background-color', new_logs_colors[i] ).animate( { color: 'white' } ); \r\n"\
                              "} \r\n"\
							  "log_ids_seen = log_ids_seen.slice(Math.max(log_ids_seen.length - MAX_LOG_IDS_TO_STORE,0)); \r\n"\
							  " \r\n"\
					"}, \r\n"\
					"complete: function (response) { \r\n"\
				   "}, \r\n"\
					"error: function () { logs_t.row.add(['<center>'+new Date().toLocaleString().replace(', ',' ')+'</center>','<center>Failed loading viper logs</center>']).draw( false ); } \r\n"\
			"}); \r\n"\
			"setTimeout(requestViperLogsData, "+ajax_refresh_rate+"); \r\n"\
		"} \r\n"\
		"setTimeout(requestViperLogsData, 3000); \r\n"
	return js #"requestViperLogsData(); \r\n "

def viper_subscription_component_html():
	id = "viper_subscription_table"
	cols = ['Ticker','Viper','PriceFeed']
	thead = "\r\n".join(["<th>"+c+"</th>" for c in cols])
	html = "<br><center><b>Viper Subscription Status</b></center><hr>"
	html += "<table id=\""+id+"\" class=\"display\" cellspacing=\"0\" width=\"100%\"> \r\n"\
		"<thead><tr>"+thead+"</tr></thead> \r\n"\
		"<tbody></tbody> \r\n"\
	"</table> "
	return html
def viper_subscription_component_js(scrollY='600px', ajax_refresh_rate='10*1000'):
	id = "viper_subscription_table"
	ajax_url = "viper-get-viper-subscription-status-ajax"

	js = "var vst_t = $('#"+id+"').DataTable({ \r\n"\
				"\"scrollY\":        \""+scrollY+"\", \r\n"\
				"\"scrollCollapse\": true, \r\n"\
				"\"paging\":         false \r\n"\
		"}); \r\n"\
		"function requestViperSubscriptionStatusData() { \r\n"\
		"$.ajax({ \r\n"\
				"url:'"+ajax_url+"', \r\n"\
					"success: function(response) { \r\n"\
						"resp_json = JSON.parse(response); \r\n"\
						"var sub_rows = resp_json['rows']; \r\n"\
						"vst_t.clear(); \r\n"\
						"rows = []; \r\n"\
						"for (var i = 0; i < sub_rows.length; i++){ rows.push(sub_rows[i]); } \r\n"\
						"vst_t.rows.add(rows).draw(); \r\n"\
				"}, \r\n"\
				"complete: function (response) {}, \r\n"\
			   "error: function () { logs_t.row.add(['<center>'+new Date().toLocaleString().replace(', ',' ')+'</center>','<center>Failed loading viper subscription status</center>']).draw( false ); } \r\n"\
		"}); \r\n"\
		"setTimeout(requestViperSubscriptionStatusData, "+ajax_refresh_rate+"); \r\n"\
		"} \r\n"\
		"requestViperSubscriptionStatusData() \r\n"
	return js

def get_mkt_snapshot_component_html():
	id = "mkt_snapshot_chart"
	html = "<div id=\""+id+"\" class=\"chart_container\"></div>"
	return html
def get_mkt_snapshot_component_js(title='Market', ajax_refresh_rate='10*1000', height='30%'):
	id = "mkt_snapshot_chart"
	ajax_url = "viper-get-mkt-snapshot-ajax"
	js = \
		"var mkt_snapshot_chart = new Highcharts.Chart({ \r\n"\
		"chart: { renderTo: '"+id+"' , defaultSeriesType: 'line', height: '"+height+"' }, \r\n"\
		"title: { text: '"+title+"' }, \r\n"\
		"tooltip: { crosshairs: true, shared: true }, \r\n"\
		"xAxis: [{ \r\n"\
				"type: 'datetime', \r\n"\
				"tickPixelInterval: 150, \r\n"\
				"maxZoom: 20 * 1000, \r\n"\
				"gridLineWidth: 1, \r\n"\
				"minPadding: 0.2, \r\n"\
				"maxPadding: 0.2, \r\n"\
				"maxZoom: 60 \r\n"\
		  "}], \r\n"\
		"yAxis: [ { minPadding: 0.2, maxPadding: 0.2, title: { text: '' }, opposite:true }, { minPadding: 0.2, maxPadding: 0.2, title: { text: '' }  }], \r\n"\
		"exporting: { enabled: false }, \r\n"\
		"series: [ {name: 'SPX Index', data: []  }, {name: 'VIX Index',yAxis: 1, data: []  , visible: true } ] \r\n"\
		"}); \r\n"\
		"\r\n"\
		"\r\n"\
		"function requestMktSnapshotChartData() { \r\n"\
			"$.ajax({ \r\n"\
					"url:'"+ajax_url+"', \r\n"\
						"success: function(response) { \r\n"\
							 "resp_json = JSON.parse(response); \r\n"\
							 "var curr_utc = resp_json['utc']; \r\n"\
                             "mkt_snapshot_chart.series[0].addPoint([curr_utc,resp_json['spx']], false, false); \r\n"\
                             "mkt_snapshot_chart.series[1].addPoint([curr_utc, resp_json['vix']], false, false); \r\n"\
					  "mkt_snapshot_chart.redraw() \r\n"\
					"}, \r\n"\
					"complete: function (response) {}, \r\n"\
				   "error: function () { logs_t.row.add(['<center>'+new Date().toLocaleString().replace(', ',' ')+'</center>','<center>Failed updating market snapshot chart</center>']).draw( false ); } \r\n"\
			"}); \r\n"\
			"setTimeout(requestMktSnapshotChartData, "+ajax_refresh_rate+"); \r\n"\
		"} \r\n"\
		"requestMktSnapshotChartData() \r\n"

	return js

def scheduled_tasks_component_html():
	id = "viper_sched_tasks_table"
	cols = ['TaskName','NextInvokeTimestamp','TimeToNextInvoke','NextInvokeUTC']
	thead = "\r\n".join(["<th>"+c+"</th>" for c in cols])
	html = "<table id=\""+id+"\" class=\"display\" cellspacing=\"0\" width=\"100%\"> \r\n"\
		"<thead><tr>"+thead+"</tr></thead> \r\n"\
		"<tbody> \r\n"\
        "</tbody> \r\n"\
	"</table> "
	return html
def scheduled_tasks_component_js(scrollY='600px', ajax_refresh_rate='5*1000'):
	id = "viper_sched_tasks_table"
	cols = ['TaskName','NextInvokeTimestamp','TimeToNextInvoke','NextInvokeUTC']
	ajax_url = "viper-get-next-invoke-times"

	#region col2attr
	col2attrs = {
		'TaskName': ColumnAttr(is_number=False),
		'NextInvokeTimestamp': ColumnAttr(is_visible=False),
		'TimeToNextInvoke': ColumnAttr(is_number=False),
		'NextInvokeUTC': ColumnAttr(is_visible=False)
	}
	#endregion

	columnDefs = generate_columnDefs(cols, col2attrs)

	js = "var sched_tasks_table = $('#"+id+"').DataTable({ \r\n"\
				"\"searching\": false, \r\n"\
				"\"scrollY\":        \""+scrollY+"\", \r\n"\
				"\"scrollCollapse\": true, \r\n"\
				"\"paging\":         false, \r\n"\
				+columnDefs+\
		"}); \r\n"\
		"function requestScheduledTasksData() { \r\n"\
			"$.ajax({ \r\n"\
					"url:'"+ajax_url+"', \r\n"\
						"success: function(response) { \r\n"\
								    "sched_tasks_table.clear().draw(false); \r\n"\
								    "resp_json = JSON.parse(response); \r\n"\
								    "var arr = resp_json['rows'] \r\n"\
								    "var rows = []; \r\n"\
									"for (var i = 0; i < arr.length; i++){ rows.push(arr[i]); \r\n " \
	                             "} \r\n"\
							     "sched_tasks_table.rows.add(rows).draw();   \r\n"\
					"}, \r\n"\
					"complete: function (response) { }, \r\n"\
				   "error: function () { logs_t.row.add(['<center>'+new Date().toLocaleString().replace(', ',' ')+'</center>','<center>Failed loading scheduled tasks next invoke times</center>']).draw( false ); } \r\n"\
			"}); \r\n"\
			"setTimeout(requestScheduledTasksData, "+ajax_refresh_rate+"); \r\n"\
		"} \r\n"\
		"requestScheduledTasksData(); "
	return js

def full_spectrum_attribution_component_html():
	id = "viper_full_spetrum_attribution_table"
	cols = ['Deal Status','MktCap','MktCapPctOfAUM','PnL($)']
	thead = "\r\n".join(["<th>"+c+"</th>" for c in cols])
	html = "<table id=\""+id+"\" class=\"display\" cellspacing=\"0\" width=\"100%\"> \r\n"\
		"<thead><tr>"+thead+"</tr></thead> \r\n"\
		"<tbody> \r\n"\
        "</tbody> \r\n"\
	"</table> "
	return html
def full_spectrum_attribution_component_js(scrollY='600px', ajax_refresh_rate='5*1000'):
	id = "viper_full_spetrum_attribution_table"
	cols = ['Deal Status','MktCap','MktCapPctOfAUM','PnL($)']
	ajax_url = 'viper-get-sleeves-table-ajax'

	#region col2attr
	col2attrs = {
		'Deal Status':ColumnAttr(is_number=False),
        'MktCap':ColumnAttr(rounding=0,prefix='$',is_colored=True),
        'MktCapPctOfAUM':ColumnAttr(rounding=2,suffix='%',is_colored=True),
		'PnL($)':ColumnAttr(rounding=0,prefix='$',is_colored=True)
	}
	#endregion

	columnDefs = generate_columnDefs(cols, col2attrs)

	js = "var full_spec_attr_table = $('#"+id+"').DataTable({ \r\n"\
				"\"searching\": false, \r\n"\
				"\"scrollY\":        \""+scrollY+"\", \r\n"\
				"\"scrollCollapse\": true, \r\n"\
				"\"paging\":         false, \r\n"\
				+columnDefs+\
		"}); \r\n"\
		"function requestFullSpectrumAttributionData() { \r\n"\
			"$.ajax({ \r\n"\
					"url:'"+ajax_url+"', \r\n"\
						"success: function(response) { \r\n"\
								    "full_spec_attr_table.clear().draw(false); \r\n"\
								    "resp_json = JSON.parse(response); \r\n"\
								    "var arr = resp_json['rows'] \r\n"\
								    "var rows = []; \r\n"\
									"for (var i = 0; i < arr.length; i++){ rows.push(arr[i]); \r\n " \
	                             "} \r\n"\
							     "full_spec_attr_table.rows.add(rows).draw();   \r\n"\
					"}, \r\n"\
					"complete: function (response) { }, \r\n"\
				   "error: function () { logs_t.row.add(['<center>'+new Date().toLocaleString().replace(', ',' ')+'</center>','<center>Failed loading full spectrum attribution table</center>']).draw( false ); } \r\n"\
			"}); \r\n"\
			"setTimeout(requestFullSpectrumAttributionData, "+ajax_refresh_rate+"); \r\n"\
		"} \r\n"\
		"requestFullSpectrumAttributionData(); "
	return js

def positions_stocks_metrics_component_html():
	id = "viper_positions_stocks_metrics_table"
	cols = ['Ticker','AggQty','PX_CHG_1D(%)','PX_LAST','PX_YEST_CLOSE','MktCap($)','MktCap(%)','MTM_1D_CHG(bps)']
	thead = "\r\n".join(["<th>"+c+"</th>" for c in cols])
	html = "<table id=\""+id+"\" class=\"display\" cellspacing=\"0\" width=\"100%\"> \r\n"\
		"<thead><tr>"+thead+"</tr></thead> \r\n"\
		"<tbody> \r\n"\
        "</tbody> \r\n"\
	"</table> "
	return html
def positions_stocks_metrics_component_js(scrollY='600px', ajax_refresh_rate='20*1000'):
	id = "viper_positions_stocks_metrics_table"
	cols = ['Ticker','AggQty','PX_CHG_1D(%)','PX_LAST','PX_YEST_CLOSE','MktCap($)','MktCap(%)','MTM_1D_CHG(bps)']
	ajax_url = 'viper-get-positions-stocks-metrics'

	#region col2attr
	col2attrs = {
		'Ticker': ColumnAttr(is_number=False),
        'AggQty': ColumnAttr(rounding=0,is_colored=True),
		'PX_CHG_1D(%)':ColumnAttr(rounding=2,suffix='%',is_colored=True),
        'PX_LAST': ColumnAttr(rounding=3,prefix='$'),
		'PX_YEST_CLOSE': ColumnAttr(rounding=3,prefix='$'),
        #'HIGH_52WEEK': ColumnAttr(rounding=3,prefix='$'),
        #'LOW_52WEEK': ColumnAttr(rounding=3,prefix='$'),
        #'MOV_AVG_30D': ColumnAttr(rounding=3,prefix='$'),
        #'MOV_AVG_5D': ColumnAttr(rounding=3,prefix='$'),
        'MktCap($)': ColumnAttr(rounding=0,prefix='$',is_colored=True),
        'MktCap(%)': ColumnAttr(rounding=2,suffix='%',is_colored=True),
		'MTM_1D_CHG(bps)': ColumnAttr(rounding=0,suffix='bps',is_colored=True)
	}
	#endregion

	columnDefs = generate_columnDefs(cols, col2attrs)

	js = "var pos_stocks_metrics_t = $('#"+id+"').DataTable({ \r\n"\
				"\"scrollY\":        \""+scrollY+"\", \r\n"\
				"\"scrollCollapse\": true, \r\n"\
				"\"paging\":         false, \r\n"\
				+columnDefs+\
		"}); \r\n"\
		"function requestPositionsStocksMetricsData() { \r\n"\
			"$.ajax({ \r\n"\
					"url:'"+ajax_url+"', \r\n"\
						"success: function(response) { \r\n"\
								    "pos_stocks_metrics_t.clear().draw(false); \r\n"\
								    "resp_json = JSON.parse(response); \r\n"\
								    "var arr = resp_json['rows'] \r\n"\
								    "var rows = []; \r\n"\
									"for (var i = 0; i < arr.length; i++){ rows.push(arr[i]); \r\n " \
	                             "} \r\n"\
							     "pos_stocks_metrics_t.rows.add(rows).draw();   \r\n"\
					"}, \r\n"\
					"complete: function (response) { }, \r\n"\
				   "error: function () { logs_t.row.add(['<center>'+new Date().toLocaleString().replace(', ',' ')+'</center>','<center>Failed loading positions stocks metrics table</center>']).draw( false ); } \r\n"\
			"}); \r\n"\
			"setTimeout(requestPositionsStocksMetricsData, "+ajax_refresh_rate+"); \r\n"\
		"} \r\n"\
		"requestPositionsStocksMetricsData(); "
	return js

def misc_component_html():
	id = "viper_misc_table"
	cols = ['Component','Description']
	thead = "\r\n".join(["<th>"+c+"</th>" for c in cols])
	html = "<table id=\""+id+"\" class=\"display\" cellspacing=\"0\" width=\"100%\"> \r\n"\
		"<thead><tr>"+thead+"</tr></thead> \r\n"\
		"<tbody> \r\n"\
        "</tbody> \r\n"\
	"</table> "
	return html
def misc_component_js(scrollY='600px', ajax_refresh_rate='10*1000'):
	id = "viper_misc_table"
	cols = ['Component','Description']
	ajax_url = 'viper-get-misc-table'

	#region col2attr
	col2attrs = {
		'Component':ColumnAttr(is_number=False),
        'Description':ColumnAttr(is_number=False)
	}
	#endregion

	columnDefs = generate_columnDefs(cols, col2attrs)

	js = "var viper_misc_table = $('#"+id+"').DataTable({ \r\n"\
				"\"searching\": false, \r\n"\
				"\"scrollY\":        \""+scrollY+"\", \r\n"\
				"\"scrollCollapse\": true, \r\n"\
				"\"paging\":         false, \r\n"\
				+columnDefs+\
		"}); \r\n"\
		"function requestMiscData() { \r\n"\
			"$.ajax({ \r\n"\
					"url:'"+ajax_url+"', \r\n"\
						"success: function(response) { \r\n"\
								    "viper_misc_table.clear().draw(false); \r\n"\
								    "resp_json = JSON.parse(response); \r\n"\
								    "var arr = resp_json['rows'] \r\n"\
								    "var rows = []; \r\n"\
									"for (var i = 0; i < arr.length; i++){ rows.push(arr[i]); \r\n " \
	                             "} \r\n"\
							     "viper_misc_table.rows.add(rows).draw();   \r\n"\
					"}, \r\n"\
					"complete: function (response) { }, \r\n"\
				   "error: function () { logs_t.row.add(['<center>'+new Date().toLocaleString().replace(', ',' ')+'</center>','<center>Failed loading misc table</center>']).draw( false ); } \r\n"\
			"}); \r\n"\
			"setTimeout(requestMiscData, "+ajax_refresh_rate+"); \r\n"\
		"} \r\n"\
		"requestMiscData(); "
	return js

def spec_index_component_html(chart_width='30%',chart_height='30%'):
	chart_id = "viper_spec_index_chart"
	table_id = "viper_spec_index_table"

	chart_html = "<center><div id=\""+chart_id+"\" style=\"width:"+chart_width+"; height: "+chart_height+";\" ></div></center>"
	cols = ['Date','ActionID','Ticker','PX_LAST','PX_YEST_CLOSE','DailyChg(%)']
	thead = "\r\n".join(["<th>"+c+"</th>" for c in cols])
	table_html = "<table id=\""+table_id+"\" class=\"display\" cellspacing=\"0\" width=\"100%\"> \r\n"\
		"<thead><tr>"+thead+"</tr></thead> \r\n"\
		"<tbody> \r\n"\
        "</tbody> \r\n"\
	"</table> "

	return chart_html + "<br>" + table_html
def spec_index_component_js(scrollY='600px', ajax_refresh_rate='10*1000'):
	chart_id = "viper_spec_index_chart"
	table_id = "viper_spec_index_table"

	ajax_url = "viper-get-spec-index-chart-ajax"
	cols = ['Date','ActionID','Ticker','PX_LAST','PX_YEST_CLOSE','DailyChg(%)']

	#region col2attr
	col2attrs = {
		'Date':ColumnAttr(is_number=False),
        'Ticker':ColumnAttr(is_number=False),
        'PX_LAST':ColumnAttr(rounding=2,is_colored=True),
        'PX_YEST_CLOSE':ColumnAttr(rounding=2,is_colored=True),
        'DailyChg(%)':ColumnAttr(rounding=2,suffix='%',is_colored=True)
	}
	#endregion
	columnDefs = generate_columnDefs(cols, col2attrs)


	js = "var spec_index_table = $('#"+table_id+"').DataTable({ \r\n"\
				"\"searching\": false, \r\n"\
				"\"scrollY\":        \""+scrollY+"\", \r\n"\
				"\"scrollCollapse\": true, \r\n"\
				"\"paging\":         false, \r\n"\
				+columnDefs+\
		"}); \r\n"\
		"var spec_index_chart = new Highcharts.Chart({ \r\n"\
		"chart: { renderTo: '"+chart_id+"' , defaultSeriesType: 'line' }, \r\n"\
		"title: { text: 'Spec M&A Index' }, \r\n"\
		"tooltip: { crosshairs: true, shared: true, valueDecimals: 3 }, \r\n"\
		"xAxis: { \r\n"\
				"type: 'datetime', \r\n"\
				"tickPixelInterval: 150, \r\n"\
				"maxZoom: 20 * 1000, \r\n"\
				"gridLineWidth: 1, \r\n"\
				"minPadding: 0.2, \r\n"\
				"maxPadding: 0.2, \r\n"\
				"maxZoom: 60 \r\n"\
		  "}, \r\n"\
		"yAxis: { \r\n"\
				"minPadding: 0.2, \r\n"\
				"maxPadding: 0.2, \r\n"\
				"title: { text: 'DailyChg(%)', margin: 5 } \r\n"\
		"}, \r\n"\
		"exporting: { enabled: false }, \r\n"\
		"series: [{name: 'EqualWeight', data: [], visible: true}] \r\n"\
		"}); \r\n"\
		"\r\n"\
		"\r\n"\
		"function requestSpecIndexChartAndTableData() { \r\n"\
			"$.ajax({ \r\n"\
					"url:'"+ajax_url+"', \r\n"\
						"success: function(response) { \r\n"\
	                        "spec_index_table.clear().draw(false); \r\n"\
							"resp_json = JSON.parse(response); \r\n"\
						    "var arr = resp_json['spec_ma_constituents'] \r\n"\
						    "var rows = []; \r\n"\
							"for (var i = 0; i < arr.length; i++){ rows.push(arr[i]); } \r\n"\
							"spec_index_table.rows.add(rows).draw(); \r\n"\
							"\r\n"\
							"var curr_utc = resp_json['utc']; \r\n"\
                            "spec_index_chart.series[0].addPoint([curr_utc,resp_json['spec_ma_equal_weight']], false, false); \r\n"\
					        "spec_index_chart.redraw() \r\n"\
					"}, \r\n"\
					"complete: function (response) {}, \r\n"\
				   "error: function () { logs_t.row.add(['<center>'+new Date().toLocaleString().replace(', ',' ')+'</center>','<center>Failed updating spec index chart</center>']).draw( false ); } \r\n"\
			"}); \r\n"\
			"setTimeout(requestSpecIndexChartAndTableData, "+ajax_refresh_rate+"); \r\n"\
		"} \r\n"\
		"requestSpecIndexChartAndTableData() \r\n"

	return js

def cash_mna_index_component_html(chart_width='30%',chart_height='30%'):
	chart_id = "viper_cash_mna_index_chart"
	table_id = "viper_cash_mna_index_table"

	top_bar_html = "<div id=\"cash_mna_index_top_bar_div\"></div>"
	chart_html = "<center><div id=\""+chart_id+"\" style=\"width:"+chart_width+"; height: "+chart_height+";\" ></div></center>"

	cols = ['Date','ActionID','Ticker','PX_LAST','PX_YEST_CLOSE','Cash/sh',
	        'ExpectedCompletion','Spread(%)','DailyChg(%)']

	thead = "\r\n".join(["<th>"+c+"</th>" for c in cols])
	table_html = "<table id=\""+table_id+"\" class=\"display\" cellspacing=\"0\" width=\"100%\"> \r\n"\
		"<thead><tr>"+thead+"</tr></thead> \r\n"\
		"<tbody> \r\n"\
        "</tbody> \r\n"\
	"</table> "

	return top_bar_html+"<br>"+chart_html + "<br>" + table_html
def cash_mna_index_component_js(scrollY='600px', ajax_refresh_rate='10*1000'):
	chart_id = "viper_cash_mna_index_chart"
	table_id = "viper_cash_mna_index_table"

	ajax_url = "viper-get-cash-mna-index-chart-ajax"
	cols = ['Date','ActionID','Ticker','PX_LAST','PX_YEST_CLOSE','Cash/sh',
	        'ExpectedCompletion','Spread(%)','DailyChg(%)']

	#region col2attr
	col2attrs = {
		'Date':ColumnAttr(is_number=False),
		'ActionID':ColumnAttr(is_number=False),
        'Ticker':ColumnAttr(is_number=False),
        'PX_LAST':ColumnAttr(rounding=2,is_colored=True),
        'PX_YEST_CLOSE':ColumnAttr(rounding=2,is_colored=True),
        'Cash/sh':ColumnAttr(rounding=3,is_colored=True),
        'ExpectedCompletion':ColumnAttr(is_number=False),
        'Spread(%)':ColumnAttr(rounding=2,suffix='%',is_colored=True),
        'DailyChg(%)':ColumnAttr(rounding=2,suffix='%',is_colored=True)
	}
	#endregion
	columnDefs = generate_columnDefs(cols, col2attrs)


	js = "var cash_mna_index_table = $('#"+table_id+"').DataTable({ \r\n"\
				"\"searching\": false, \r\n"\
				"\"scrollY\":        \""+scrollY+"\", \r\n"\
				"\"scrollCollapse\": true, \r\n"\
				"\"paging\":         false, \r\n"\
				+columnDefs+\
		"}); \r\n"\
		"var cash_mna_index_chart = new Highcharts.Chart({ \r\n"\
		"chart: { renderTo: '"+chart_id+"' , defaultSeriesType: 'line' }, \r\n"\
		"title: { text: 'Cash M&A Index' }, \r\n"\
		"tooltip: { crosshairs: true, shared: true, valueDecimals: 3 }, \r\n"\
		"xAxis: { \r\n"\
				"type: 'datetime', \r\n"\
				"tickPixelInterval: 150, \r\n"\
				"maxZoom: 20 * 1000, \r\n"\
				"gridLineWidth: 1, \r\n"\
				"minPadding: 0.2, \r\n"\
				"maxPadding: 0.2, \r\n"\
				"maxZoom: 60 \r\n"\
		  "}, \r\n"\
		"yAxis: { \r\n"\
				"minPadding: 0.2, \r\n"\
				"maxPadding: 0.2, \r\n"\
				"title: { text: 'DailyChg(%)', margin: 5 } \r\n"\
		"}, \r\n"\
		"exporting: { enabled: false }, \r\n"\
		"series: [{name: 'EqualWeightIndex', data: [], visible: false},{name: 'EqualWeightSpread', data: [], visible: true}] \r\n"\
		"}); \r\n"\
		"\r\n"\
		"\r\n"\
		"function requestCashMNAIndexChartAndTableData() { \r\n"\
			"$.ajax({ \r\n"\
					"url:'"+ajax_url+"', \r\n"\
						"success: function(response) { \r\n"\
	                        "cash_mna_index_table.clear().draw(false); \r\n"\
							"resp_json = JSON.parse(response); \r\n"\
						    "var arr = resp_json['cash_ma_constituents'] \r\n"\
						    "var rows = []; \r\n"\
						    "var verified_rows_idxs = []; \r\n"\
						    "var excluded_rows_idxs = []; \r\n"\
							"for (var i = 0; i < arr.length; i++){ " \
									"if(arr[i][9] == 'true'){ verified_rows_idxs.push(i); } \r\n"\
									"if(arr[i][8] == 'true'){ excluded_rows_idxs.push(i); } \r\n"\
	                                "rows.push(arr[i]); \r\n"\
                             "} \r\n"\
							"var rowNodes = cash_mna_index_table.rows.add(rows).draw().nodes(); \r\n"\
			                "for (var j = 0; j < verified_rows_idxs.length; j++ ) { $('td', rowNodes[verified_rows_idxs[j]]).css('background-color', '#e6ffe6' ) } \r\n"\
		                    "for (var j = 0; j < excluded_rows_idxs.length; j++ ) { $('td', rowNodes[excluded_rows_idxs[j]]).css('background-color', '#ffcccc' ) } \r\n"\
							"var curr_utc = resp_json['utc']; \r\n"\
                            "cash_mna_index_chart.series[0].addPoint([curr_utc,resp_json['cash_ma_equal_weight_ret']], false, false); \r\n"\
                            "cash_mna_index_chart.series[1].addPoint([curr_utc,resp_json['cash_ma_equal_weight_upside']], false, false); \r\n"\
					        "cash_mna_index_chart.redraw() \r\n"\
							"$('#cash_mna_index_top_bar_div').html('average duration: '+ resp_json['avg_duration'] + ' days'); \r\n"\
					"}, \r\n"\
					"complete: function (response) {}, \r\n"\
				   "error: function () { logs_t.row.add(['<center>'+new Date().toLocaleString().replace(', ',' ')+'</center>','<center>Failed updating cash mna index chart</center>']).draw( false ); } \r\n"\
			"}); \r\n"\
			"setTimeout(requestCashMNAIndexChartAndTableData, "+ajax_refresh_rate+"); \r\n"\
		"} \r\n"\
		"requestCashMNAIndexChartAndTableData() \r\n"

	return js

def mna_index_component_html(chart_width='30%',chart_height='30%'):
	chart_id = "viper_mna_index_chart"
	table_id = "viper_mna_index_table"

	top_bar_html = "<div id=\"mna_index_top_bar_div\"></div>"
	chart_html = "<center><div id=\""+chart_id+"\" style=\"width:"+chart_width+"; height: "+chart_height+";\" ></div></center>"

	cols = ['Date','ActionID','TgtTicker','AcqTicker','DealCurrency',
	       'TGT_PX_LAST','ACQ_PX_LAST','TGT_PX_YEST_CLOSE','ACQ_PX_YEST_CLOSE',
	       'Cash/sh','Stock/sh','ExpectedCompletion','Duration',
	        'DownsidePX','UpsidePX',
	        'Downside(%)','Spread(%)','RiskFree',
	        'Ann.Spread(%)','ProbClose(%)',
	        'TgtDailyChg(%)','TgtMktCap($bln)']

	thead = "\r\n".join(["<th>"+c+"</th>" for c in cols])
	table_html = "<table id=\""+table_id+"\" class=\"display\" cellspacing=\"0\" width=\"100%\"> \r\n"\
		"<thead><tr>"+thead+"</tr></thead> \r\n"\
		"<tbody> \r\n"\
        "</tbody> \r\n"\
	"</table> "

	return top_bar_html+"<br>"+chart_html + "<br>" + table_html
def mna_index_component_js(scrollY='600px', ajax_refresh_rate='300*1000'):
	ajax_refresh_rate = '300*6*1000' #every half an hour
	chart_id = "viper_mna_index_chart"
	table_id = "viper_mna_index_table"

	ajax_url = "viper-get-mna-index-chart-ajax"
	mna_index_chart_url = 'viper-get-historical-index-chart'
	cols = ['Date','ActionID','TgtTicker','AcqTicker','DealCurrency',
	        'TGT_PX_LAST','ACQ_PX_LAST','TGT_PX_YEST_CLOSE','ACQ_PX_YEST_CLOSE',
	        'Cash/sh','Stock/sh','ExpectedCompletion','Duration',
	        'DownsidePX','UpsidePX',
	        'Downside(%)','Spread(%)','RiskFree',
	        'Ann.Spread(%)','ProbClose(%)',
	        'TgtDailyChg(%)','TgtMktCap($bln)']

	#region col2attr
	col2attrs = {
		'Date':ColumnAttr(is_number=False),
		'ActionID':ColumnAttr(is_number=False),
        'TgtTicker':ColumnAttr(is_number=False),
        'AcqTicker':ColumnAttr(is_number=False),
		'DealCurrency': ColumnAttr(is_number=False),
        'TGT_PX_LAST':ColumnAttr(rounding=2,is_colored=True),
        'ACQ_PX_LAST':ColumnAttr(rounding=2,is_colored=True),
        'TGT_PX_YEST_CLOSE':ColumnAttr(rounding=2,is_colored=True),
        'ACQ_PX_YEST_CLOSE':ColumnAttr(rounding=2,is_colored=True),
        'Cash/sh':ColumnAttr(rounding=3,is_colored=True),
        'Stock/sh':ColumnAttr(rounding=3,is_colored=True),
        'ExpectedCompletion':ColumnAttr(is_number=False),
        'Duration':ColumnAttr(rounding=0,suffix=' days',is_colored=True),
		'DownsidePX':ColumnAttr(rounding=3,prefix='$',is_colored=True),
		'UpsidePX':ColumnAttr(rounding=3,prefix='$',is_colored=True),
		'Downside(%)':ColumnAttr(rounding=2,suffix='%',is_colored=True),
        'Spread(%)':ColumnAttr(rounding=2,suffix='%',is_colored=True),
        'RiskFree':ColumnAttr(rounding=3,suffix='%',is_colored=True),
        'Ann.Spread(%)':ColumnAttr(rounding=2,suffix='%',is_colored=True),
        'ProbClose(%)':ColumnAttr(rounding=2,suffix='%',is_colored=True),
        'TgtDailyChg(%)':ColumnAttr(rounding=2,suffix='%',is_colored=True),
        'TgtMktCap($bln)':ColumnAttr(rounding=2,prefix='$',suffix='b',is_colored=True)
	}
	#endregion
	columnDefs = generate_columnDefs(cols, col2attrs)


	js = "var mna_index_table = $('#"+table_id+"').DataTable({ \r\n" \
			"dom: 'Bfrtip',"\
 			"buttons: ['copy', 'excel','pdf','print'],"\
				"\"searching\": true, \r\n"\
				"\"scrollY\":        \""+scrollY+"\", \r\n"\
				"\"scrollCollapse\": true, \r\n"\
				"\"paging\":         false, \r\n"\
				+columnDefs+\
		"}); \r\n"\
		"var mna_index_chart =  Highcharts.stockChart({ \r\n"\
		"chart: { renderTo: '"+chart_id+"' , defaultSeriesType: 'line' }, \r\n"\
		"title: { text: 'M&A Index' }, \r\n"\
		"tooltip: { crosshairs: true, shared: true, valueDecimals: 3 }, \r\n"\
		"xAxis: { \r\n"\
				"type: 'datetime', \r\n"\
		  "}, \r\n"\
	"rangeSelector: {"\
	" enabled:true,"\
	"           },\r\n"\
		"yAxis: { \r\n"\
				"minPadding: 0.2, \r\n"\
				"maxPadding: 0.2, \r\n"\
				"title: { text: 'DailyChgz(%)', margin: 5 } \r\n"\
		"}, \r\n"\
		"exporting: { enabled: true }, \r\n"\
		"series: [" \
	                                    "{name: 'EqualWeightIndex', data: [], visible: false}, " \
	                                    "{name: 'EqualWeightSpread', data: [], visible: false}, " \
	                                    "{name: 'MktCapWeightSpread', data: [], visible: false}, " \
	                                    "{name: 'EWS_EWD', data: [], visible: true}, " \
	                                    "{name: 'MVS_MVD', data: [], visible: false}, " \
                "] \r\n"\
		"}); \r\n"\
		"\r\n"\
		"\r\n"\
		"$.ajax({ \r\n"\
					"url:'"+mna_index_chart_url+"', \r\n"\
						"success: function(response) { \r\n"\
							"resp_json = JSON.parse(response); \r\n"\
							"my_data = [];" \
							"for(i=0;i<resp_json['data'].length;i++){" \
							"my_data.push([parseInt(resp_json['data'][i][0]), parseFloat(resp_json['data'][i][1])])" \
							"}" \
                            "mna_index_chart.series[3].setData(my_data); \r\n"\
							"$('#mna_index_top_bar_div').html(" \
	                                 "'average duration: '+ resp_json['avg_duration'] + ' days <br> ' +  "\
									 "'mktcap weighted duration: '+ resp_json['mv_duration'] + ' days <br>' +" \
	                                 "''); \r\n"\
					"}, \r\n"\
					"complete: function (response) {}, \r\n"\
				   "error: function () { logs_t.row.add(['<center>'+new Date().toLocaleString().replace(', ',' ')+'</center>','<center>Failed updating mna index chart</center>']).draw( false ); } \r\n"\
			"}); \r\n"\
		"function requestMNAIndexChartAndTableData() { \r\n"\
			"$.ajax({ \r\n"\
					"url:'"+ajax_url+"', \r\n"\
						"success: function(response) { \r\n"\
	                        "mna_index_table.clear().draw(false); \r\n"\
							"resp_json = JSON.parse(response); \r\n"\
						    "var arr = resp_json['ma_constituents'] \r\n"\
						    "var rows = []; \r\n"\
							"for (var i = 0; i < arr.length; i++){ rows.push(arr[i]); } \r\n"\
							"var rowNodes = mna_index_table.rows.add(rows).draw().nodes(); \r\n"\
							"var curr_utc = resp_json['utc']; \r\n"\
                            "mna_index_chart.series[0].addPoint([curr_utc,resp_json['ma_equal_weight_ret']], false, false); \r\n"\
                            "mna_index_chart.series[1].addPoint([curr_utc,resp_json['ma_equal_weight_upside']], false, false); \r\n"\
                            "mna_index_chart.series[2].addPoint([curr_utc,resp_json['ma_mktcap_weight_upside']], false, false); \r\n"\
                            "mna_index_chart.series[3].addPoint([curr_utc,resp_json['ews_ann_by_ewd']], false, false); \r\n"\
                            "mna_index_chart.series[4].addPoint([curr_utc,resp_json['mvws_ann_by_mvd']], false, false); \r\n"\
					        "mna_index_chart.redraw() \r\n"\
							"$('#mna_index_top_bar_div').html(" \
	                                 "'average duration: '+ resp_json['avg_duration'] + ' days <br> ' +  "\
									 "'mktcap weighted duration: '+ resp_json['mv_duration'] + ' days <br>' +" \
	                                 "''); \r\n"\
					"}, \r\n"\
					"complete: function (response) {}, \r\n"\
				   "error: function () { logs_t.row.add(['<center>'+new Date().toLocaleString().replace(', ',' ')+'</center>','<center>Failed updating mna index chart</center>']).draw( false ); } \r\n"\
			"}); \r\n"\
			"setTimeout(requestMNAIndexChartAndTableData, "+ajax_refresh_rate+"); \r\n"\
		"} \r\n"\
		"requestMNAIndexChartAndTableData() \r\n"

	return js

def historical_mna_index_component_html(chart_width='30%',chart_height='30%'):
	chart_id = "viper_mna_hist_index_chart"
	eod_pos_table_id = 'viper_mna_hist_eod_pos_table'
	action_perf_table_id = 'viper_mna_hist_action_perf_table'
	date_picker_id = "index_composition_date_picker"
	submit_btn_id = "hist_index_submit"

	top_bar_html = "<center><div id=\"mna_hist_index_top_bar_div\" style=\"color:#0000FF\"></div></center>"
	chart_html = "<center><div id=\""+chart_id+"\" style=\"width:"+chart_width+"; height: "+chart_height+";\" ></div></center>"
	date_picker_html = "Browse Index Constituents at <input id=\""+date_picker_id+"\" type=\"text\" /><input id=\""+submit_btn_id+"\" type=\"button\" value=\"Submit\" />"

	cols = ['Date','ActionID','Cash/sh','Stock/sh','TgtTicker','AcqTicker','ExpectedCompletion',
	        'TgtEODPrice','AcqEODPrice','TgtSODPrice','AcqSODPrice', 'TgtQty','AcqQty']
	thead = "\r\n".join(["<th>"+c+"</th>" for c in cols])
	eod_pos_table_html = "<table id=\""+eod_pos_table_id+"\" class=\"display\" cellspacing=\"0\" width=\"100%\"><thead><tr>"+thead+"</tr></thead><tbody></tbody></table>"

	action_perf_cols = ['Date','ActionID','Announce Date','TgtTicker','AcqTicker','Cash/sh','Stock/sh','PnL','CumPnL']

	action_perf_table_html = "<table id=\""+action_perf_table_id+"\" class=\"display\" cellspacing=\"0\" width=\"100%\"><thead><tr>"+\
	                         "\r\n".join(["<th>"+c+"</th>" for c in action_perf_cols])+\
	                         "</tr></thead><tbody></tbody></table>"

	html =  "<div id=\"vertical_tabs\"> \r\n"\
		    "<ul> \r\n"\
		        "<li><a href=\"#eod_tab\">EOD Positions</a></li> \r\n"\
		        "<li><a href=\"#action_perf_table\">Action Performance</a></li> \r\n"\
		    "</ul> \r\n"\
		    "<div id=\"eod_tab\">"+eod_pos_table_html+"</div> \r\n"\
		    "<div id=\"action_perf_table\">"+action_perf_table_html+"</div> \r\n"\
			"</div> \r\n"\

	return top_bar_html+"<br>"+chart_html+"<br><center>"+date_picker_html+"</center><br>" + html
def historical_mna_index_component_js(scrollY='600px', ajax_refresh_rate='10*1000'):
	chart_id = "viper_mna_hist_index_chart"

	eod_pos_table_id = 'viper_mna_hist_eod_pos_table'
	action_perf_table_id = 'viper_mna_hist_action_perf_table'


	date_picker_id = "index_composition_date_picker"
	submit_btn_id = "hist_index_submit"

	hist_chart_ajax_url = "mna-index-historical-chart-ajax"
	hist_constituents_table_ajax_url = "mna-index-historical-constituents-ajax"

	cols = ['Date','ActionID','Cash/sh','Stock/sh','TgtTicker','AcqTicker','ExpectedCompletion',
	        'TgtEODPrice','AcqEODPrice','TgtSODPrice','AcqSODPrice','TgtQty','AcqQty']

	action_perf_cols = ['Date','ActionID','Announce Date','TgtTicker','AcqTicker','Cash/sh','Stock/sh','PnL','CumPnL']

	#region col2attr
	col2attrs = {
		'Date':ColumnAttr(is_number=False),
		'ActionID':ColumnAttr(is_number=False),
        'Cash/sh':ColumnAttr(rounding=3,is_colored=True),
        'Stock/sh':ColumnAttr(rounding=3,is_colored=True),
        'TgtTicker':ColumnAttr(is_number=False),
        'AcqTicker':ColumnAttr(is_number=False),
        'ExpectedCompletion':ColumnAttr(is_number=False),
        'TgtEODPrice':ColumnAttr(rounding=3),
        'AcqEODPrice':ColumnAttr(rounding=3),
        'TgtSODPrice':ColumnAttr(rounding=3),
        'AcqSODPrice':ColumnAttr(rounding=3),
        'TgtQty':ColumnAttr(rounding=5,is_colored=True),
        'AcqQty':ColumnAttr(rounding=5, is_colored=True)
	}
	#endregion
	columnDefs = generate_columnDefs(cols, col2attrs)
	#region col2attr
	col2attrs = {
		'Date':ColumnAttr(is_number=False),
		'ActionID':ColumnAttr(is_number=False),
		'Announce Date':ColumnAttr(is_number=False),
	    'TgtTicker':ColumnAttr(is_number=False),
	    'AcqTicker':ColumnAttr(is_number=False),
        'Cash/sh':ColumnAttr(rounding=3,is_colored=True),
        'Stock/sh':ColumnAttr(rounding=3,is_colored=True),
		'PnL':ColumnAttr(rounding=0,suffix='bps',is_colored=True),
		'CumPnL':ColumnAttr(rounding=0,suffix='bps',is_colored=True),
	}
	#endregion
	action_perf_columnDefs = generate_columnDefs(action_perf_cols, col2attrs)

	#switch to tab 4: "$( \"#vertical_tabs\" ).tabs( \"option\", \"active\", 0 ); $( \"#vertical_tabs\" ).tabs( \"option\", \"active\", 4 ); \r\n"\
	js = "$('#vertical_tabs').tabs().addClass('ui-tabs-vertical ui-helper-clearfix'); \r\n"
	js += "$('#"+submit_btn_id+"').click(function() {" \
									"var dt_str = $('#index_composition_date_picker').val();  \r\n"\
									"if (dt_str == '') {updateStatusBar('invalid date chosen'); return; } \r\n"\
									"utc_num = Math.round((new Date(dt_str)).getTime()); \r\n"\
									"requestHistMNAIndexConstituentsData(utc_num); \r\n"\
	                         "}) \r\n"
	#region table initialization
	js += "var eod_pos_table = $('#"+eod_pos_table_id+"').DataTable({ \r\n"\
				"\"searching\": true, \r\n"\
				"\"scrollCollapse\": true, \r\n"\
				"\"paging\":         true, \r\n"\
				"\"pageLength\":         20, \r\n"\
				"\"lengthChange\": false, \r\n"\
				+columnDefs+\
		"}); \r\n"\
         "var action_performance_table = $('#"+action_perf_table_id+"').DataTable({ \r\n"\
				"\"searching\": false, \r\n"\
				"\"scrollCollapse\": true, \r\n"\
				"\"paging\":         true, \r\n"\
				"\"pageLength\":         20, \r\n"\
				"\"lengthChange\": false, \r\n"\
				+action_perf_columnDefs+\
		"}); \r\n"
	#endregion
	js += "$('#"+date_picker_id+"').datepicker({dateFormat: 'mm-dd-yy'}); \r\n"
	#region highchart initialization
	js +=  "var hist_mna_index_chart = new Highcharts.Chart({ \r\n"\
		"   chart: { " \
                        "renderTo: '"+chart_id+"' , \r\n" \
                         "defaultSeriesType: 'line', \r\n" \
                         "events: { click: function (e) { requestHistMNAIndexConstituentsData(e.xAxis[0].value); } } \r\n"\
                   "}, \r\n"\
		"   title: { text: 'M&A Index' }, \r\n"\
		"   tooltip: { crosshairs: true, shared: true, valueDecimals: 2 }, \r\n"\
		"   plotOptions: { \r\n"\
				"series: { \r\n"\
					"point: { \r\n"\
						"events: { \r\n"\
							"click: function(e) { requestHistMNAIndexConstituentsData(this.x); } \r\n"\
						"}\r\n"\
					"} \r\n"\
				"}\r\n"\
			"}, \r\n"\
		"   xAxis: { \r\n"\
				"type: 'datetime', \r\n"\
				"tickPixelInterval: 150, \r\n"\
				"maxZoom: 20 * 1000, \r\n"\
				"gridLineWidth: 1, \r\n"\
				"minPadding: 0.2, \r\n"\
				"maxPadding: 0.2, \r\n"\
				"maxZoom: 60 \r\n"\
		  "}, \r\n"\
		"   yAxis: { \r\n"\
				"minPadding: 0.2, \r\n"\
				"maxPadding: 0.2, \r\n"\
				"title: { text: 'DailyChg(%)', margin: 5 } \r\n"\
		"}, \r\n"\
		"   exporting: { enabled: false }, \r\n"\
		"   series: [" \
	                                    "{name: 'EqualWeightIndex', data: [], visible: true}, " \
	                                    "{name: 'MarketCapWeightIndex', data: [], visible: false}, " \
	                                    "{name: 'Arb', data: [], visible: true}, " \
                "] \r\n"\
		"}); \r\n"\
		"\r\n"\
		"\r\n"
	#endregion
	#region requestHistMNAIndexChart()
	js +=   "function requestHistMNAIndexChart() { \r\n"\
			"updateStatusBar('fetching chart data...'); \r\n" \
			"$.ajax({ \r\n"\
					"url:'"+hist_chart_ajax_url+"', \r\n"\
						"success: function(response) { \r\n"\
							"resp_json = JSON.parse(response); \r\n"\
						    "var eqw_arr = resp_json['equal_weighted_index_ts'] \r\n"\
							"for (var i = 0; i < eqw_arr.length; i++){ hist_mna_index_chart.series[0].addPoint([eqw_arr[i][0], eqw_arr[i][1]], false, false); } \r\n"\
							" \r\n"\
							"var mktcap_arr = resp_json['mktcap_weighted_index_ts'] \r\n"\
							"for (var i = 0; i < mktcap_arr.length; i++){ hist_mna_index_chart.series[1].addPoint(mktcap_arr[i], false, false); } \r\n"\
							" \r\n"\
							"var arb_arr = resp_json['arbnx_ts'] \r\n"\
							"for (var i = 0; i < arb_arr.length; i++){ hist_mna_index_chart.series[2].addPoint(arb_arr[i], false, false); } \r\n"\
							" \r\n"\
							"hist_mna_index_chart.redraw(); \r\n"\
							"updateStatusBar('loaded.'); \r\n"\
					"}, \r\n"\
					"complete: function (response) {}, \r\n"\
				   "error: function () { alert('error in client-side js'); } \r\n"\
			"}); \r\n"\
		"} \r\n"
	#endregion
	#region requestHistMNAIndexConstituentsData(data_utc)
	js += "function requestHistMNAIndexConstituentsData(date_utc) { \r\n"\
				"updateStatusBar('fetching constituents data for ' + (new Date(date_utc)).toISOString().split('T')[0]+'...' ); \r\n"\
				"eod_pos_table.clear().draw(false); \r\n"\
				"action_performance_table.clear().draw(false); \r\n"\
				"$.ajax({ \r\n"\
					"url:'"+hist_constituents_table_ajax_url+"?date_utc='+Math.round(date_utc), \r\n"\
						"success: function(response) { \r\n"\
							"resp_json = JSON.parse(response); \r\n"\
							"if (resp_json['res'] != 'success') {updateStatusBar(resp_json['res']); return; } \r\n "\
							"\r\n"\
						    "var eod_arr = resp_json['eod_port'] \r\n"\
						    "var rows = []; \r\n"\
							"for (var i = 0; i < eod_arr.length; i++){ rows.push(eod_arr[i]) }; \r\n " \
					        "eod_pos_table.rows.add(rows).draw();   \r\n"\
							"\r\n"\
							"var aid_perf_arr = resp_json['action_performance'] \r\n"\
						    "var rows = []; \r\n"\
							"for (var i = 0; i < aid_perf_arr.length; i++){ rows.push(aid_perf_arr[i]) }; \r\n " \
					        "action_performance_table.rows.add(rows).draw();   \r\n"\
							"\r\n"\
							"updateStatusBar('loaded constituents data for ' + (new Date(date_utc)).toISOString().split('T')[0]+'.' ); \r\n"\
					"}, \r\n"\
					"complete: function (response) {}, \r\n"\
				   "error: function () { updateStatusBar('error populating constituents data in table'); } \r\n"\
			"}); \r\n"\
		"}\r\n"
	#endregion
	js += "function updateStatusBar(msg) { \r\n"\
			"$('#mna_hist_index_top_bar_div').html(msg); \r\n"\
		"}\r\n"\
		"requestHistMNAIndexChart() \r\n"
	return js

def mna_human_verifier_component_html():
	table_id = "mna_verifier_table"

	cols = ['Date Loaded','ActionID','Announced Date','TgtTicker','AcqTicker','LiveTgtPrice','LiveAcqPrice','DealValue','PaymentType','Cash/sh','Stock/sh','Spread(%)','Ann.ROR',
	        'ExpectedCompletion','IsExcluded','IsVerified']
	thead = "\r\n".join(["<th>"+c+"</th>" for c in cols])

	html = "<center><div id=\"mna_verifier_status_div\"></div></center>"
	html += "<br><center><input type=\"button\" id=\"submit_all_button\" value=\"Submit\" style=\"height:30px;width:200px\" /></center><br>"
	html += "<table id=\""+table_id+"\" class=\"display\" cellspacing=\"0\" width=\"100%\"> \r\n"\
		"<thead><tr>"+thead+"</tr></thead> \r\n"\
		"<tbody> \r\n"\
        "</tbody> \r\n"\
	"</table> "
	return html
def mna_human_verifier_component_js(scrollY='600px', ajax_refresh_rate='60*1000'):
	ajax_url = "viper-get-mna-universe-to-verify-ajax"
	post_url = "viper-post-verified-mna-universe"
	cols = ['Date Loaded','ActionID','Announced Date','TgtTicker','AcqTicker','LiveTgtPrice','LiveAcqPrice','DealValue','PaymentType','Cash/sh','Stock/sh','Spread(%)','Ann.ROR',
	        'ExpectedCompletion','IsExcluded','IsVerified']

	col2attrs = { 'LiveTgtPrice':ColumnAttr(is_number=True) ,
				  'LiveAcqPrice': ColumnAttr(is_number=True),
				  'DealValue': ColumnAttr(is_number=True),
				  'Spread(%)': ColumnAttr(is_number=True),
				  'Ann.ROR':ColumnAttr(is_number=True),
				  }
	columnDefs = generate_columnDefs(cols, col2attrs)

	js = \
		"var editable_table = $('#mna_verifier_table').DataTable( { \r\n" \
        "dom: 'Bfrtip'," \
        "buttons: ['copy', 'excel','pdf','print']," \
        "\"searching\": true, \r\n"\
				"\"scrollY\":        \""+scrollY+"\", \r\n"\
				"\"scrollCollapse\": true, \r\n"\
				"\"paging\":         false, \r\n"\
				+columnDefs+\
		"} ); \r\n"\
		"$('body').on('focus',\".datepicker_recurring_start\", function(){ $(this).datepicker(); }); \r\n"\
		"$('#submit_all_button').click(function() { \r\n"\
			"num_rows =  editable_table.data().length \r\n"\
		    "var data = editable_table.rows().data(); \r\n"\
		    "res=[] \r\n"\
		    "data.each(function (value, index) { \r\n"\
			    "console.log(value) \r\n"\
			    "date_loaded = value[0] \r\n"\
			    "aid = value[1] \r\n"\
			    "annc_dt = document.getElementById('annc_date_'+aid).value; \r\n"\
			    "tgt_ticker = value[3] \r\n"\
			    "acq_ticker = value[4] \r\n"\
			    "payment_type = document.getElementById(\"payment_type_\"+aid).value \r\n"\
			    "cash_per_sh = document.getElementById(\"cash_per_sh_\"+aid).value \r\n"\
			    "stock_per_sh = document.getElementById(\"stock_per_sh_\"+aid).value \r\n"\
			    "exp_comp_dt = document.getElementById(\"expected_completion_\"+aid).value \r\n"\
			    "is_excluded = document.getElementById(\"is_excluded_\"+aid).value \r\n"\
			    "is_verified = document.getElementById(\"is_verified_\"+aid).value \r\n"\
			    "row_item = { \r\n"\
	              "'DateLoaded':date_loaded, \r\n"\
			      "'ActionID':aid, \r\n"\
			      "'Announced Date': annc_dt, \r\n"\
			      "'TgtTicker':tgt_ticker, \r\n"\
			      "'AcqTicker':acq_ticker, \r\n"\
			      "'PaymentType':payment_type, \r\n"\
			      "'Cash/sh':cash_per_sh, \r\n"\
			      "'Stock/sh':stock_per_sh, \r\n"\
			      "'ExpectedCompletion':exp_comp_dt, \r\n"\
			      "'IsExcluded':is_excluded, \r\n"\
			      "'IsVerified':is_verified \r\n"\
			    "} \r\n"\
			    "res.push(row_item) \r\n"\
		  "}); \r\n"\
		 "$('#mna_verifier_status_div').html('submitting...'); \r\n"\
		 "$.ajax({ \r\n"\
					"type: \"POST\", \r\n"\
				    "url: '"+post_url+"', \r\n"\
					"data: JSON.stringify(res), \r\n"\
                    "success: function(response) { $('#mna_verifier_status_div').html(response); requestMNAVerifyTable(); }, \r\n"\
                    "complete: function(response) { }, \r\n"\
                    "error: function() { $('#mna_verifier_status_div').html(response); } \r\n"\
                "}) \r\n"\
		"}); \r\n"\
		"function requestMNAVerifyTable() { \r\n"\
			"$.ajax({ \r\n"\
		        "url: '"+ajax_url+"', \r\n"\
		        "success: function(response) { parseAJAXintoDatatable(response); }, \r\n"\
		        "complete: function(response) {}, \r\n"\
		        "error: function() {} \r\n"\
		    "}) \r\n"\
		"} \r\n"\
		"function populateMNAVerifyTableLOOP() { \r\n"\
			"$('#mna_verifier_status_div').html('loading...'); \r\n"\
			"requestMNAVerifyTable(); \r\n"\
	        "$('#mna_verifier_status_div').html(''); \r\n"\
		    "setTimeout(populateMNAVerifyTable, "+ajax_refresh_rate+"); \r\n"\
		"} \r\n"\
		"function parseAJAXintoDatatable(response) \r\n"\
		"{ \r\n"\
			"editable_table.clear().draw(false); \r\n"\
	        "resp_json = JSON.parse(response) \r\n"\
		    "var rows = [] \r\n"\
		    "var verified_rows_idxs = [] \r\n"\
		    "var excluded_rows_idxs = [] \r\n"\
		    "for (i=0;i<resp_json.length;i++) \r\n"\
		    "{ \r\n"\
		      "var aid = resp_json[i]['ActionID'] \r\n"\
		      "exp_comp =  resp_json[i]['ExpectedCompletion'] \r\n"\
		      "date_loaded = resp_json[i]['DateLoaded'] \r\n"\
		      "annc_dt = resp_json[i]['Announced Date'] \r\n"\
		      "tgt_ticker = resp_json[i]['TgtTicker'] \r\n"\
		      "acq_ticker = resp_json[i]['AcqTicker'] \r\n" \
		      "target_last_price = resp_json[i]['LiveTgtPrice'] \r\n" \
			  "acquirer_last_price = resp_json[i]['LiveAcqPrice'] \r\n" \
			  "deal_value = resp_json[i]['DealValue'] \r\n" \
			  "payment_type = resp_json[i]['PaymentType'] \r\n"\
		      "cash_per_sh = resp_json[i]['Cash/sh'] \r\n"\
		      "stock_per_sh = resp_json[i]['Stock/sh'] \r\n" \
			  "spread_percentage = resp_json[i]['Spread(%)'] \r\n" \
		      "ann_ror = resp_json[i]['Ann.ROR'] \r\n" \
			  "is_excluded = resp_json[i]['IsExcluded'] \r\n"\
		      "is_verified = resp_json[i]['IsVerified'] \r\n"\
		      "exp_comp_id = \"expected_completion_\"+aid \r\n"\
		      "annc_date_id = \"annc_date_\"+aid \r\n"\
		      "payment_type_id = \"payment_type_\"+aid \r\n"\
		      "cash_per_sh_id = \"cash_per_sh_\"+aid \r\n"\
		      "stock_per_sh_id = \"stock_per_sh_\"+aid \r\n"\
		      "is_excluded_id = 'is_excluded_'+aid \r\n"\
		      "is_verified_id = 'is_verified_'+aid \r\n"\
		      "exp_comp_datepicker = '<input id=\"'+exp_comp_id+'\" class=\"datepicker_recurring_start\" type=\"text\" value='+exp_comp+'>' \r\n"\
		      "annc_date_datepicker = '<input id=\"'+annc_date_id+'\" class=\"datepicker_recurring_start\" type=\"text\" value='+annc_dt+'>' \r\n"\
		      "payment_type_dropdown = '<select id=\"'+payment_type_id+'\"><option>---SELECT---</option>' \r\n"\
		      "if (payment_type == 'Cash') { payment_type_dropdown += '<option selected=\"selected\" value=\"Cash\">Cash</option>' } else { payment_type_dropdown += '<option value=\"Cash\">Cash</option>' } \r\n"\
		      "if (payment_type == 'Stock') { payment_type_dropdown += '<option selected=\"selected\" value=\"Stock\">Stock</option>' } else { payment_type_dropdown += '<option value=\"Stock\">Stock</option>' } \r\n"\
		      "if (payment_type == 'Cash and Stock') { payment_type_dropdown += '<option selected=\"selected\" value=\"Cash and Stock\">Cash and Stock</option>' } else { payment_type_dropdown += '<option value=\"Cash and Stock\">Cash and Stock</option>' } \r\n" \
              "if (payment_type == 'Cash or Stock') { payment_type_dropdown += '<option selected=\"selected\" value=\"Cash or Stock\">Cash or Stock</option>' } else { payment_type_dropdown += '<option value=\"Cash or Stock\">Cash or Stock</option>' } \r\n" \
              "payment_type_dropdown += '</select>' \r\n"\
		      "cash_per_sh_textbox = '<input id=\"'+cash_per_sh_id+'\" type=\"text\" value='+cash_per_sh+' >' \r\n"\
		      "stock_per_sh_textbox = '<input id=\"'+stock_per_sh_id+'\" type=\"text\" value='+stock_per_sh+' >' \r\n"\
		      "is_excluded_dropdown = '<select id=\"'+is_excluded_id+'\"><option>---SELECT---</option>' \r\n"\
		      "if (is_excluded == 'true') { is_excluded_dropdown += '<option selected=\"selected\" value=\"true\">Yes</option>' } else { is_excluded_dropdown += '<option value=\"true\">Yes</option>' } \r\n"\
		      "if (is_excluded == 'false') { is_excluded_dropdown += '<option selected=\"selected\" value=\"false\">No</option>' } else { is_excluded_dropdown += '<option value=\"false\">No</option>' } \r\n"\
              "is_verified_dropdown = '<select id=\"'+is_verified_id+'\"><option>---SELECT---</option>' \r\n"\
		      "if (is_verified == 'true') { is_verified_dropdown += '<option selected=\"selected\" value=\"true\">Yes</option>' } else { is_verified_dropdown += '<option value=\"true\">Yes</option>' } \r\n"\
		      "if (is_verified == 'false') { is_verified_dropdown += '<option selected=\"selected\" value=\"false\">No</option>' } else { is_verified_dropdown += '<option value=\"false\">No</option>' } \r\n"\
		      "if (is_verified == 'true') {verified_rows_idxs.push(i); } \r\n"\
		      "if (is_excluded == 'true') {excluded_rows_idxs.push(i); } \r\n"\
		      "rows.push([ \r\n"\
		          "date_loaded, \r\n"\
		          "aid, \r\n"\
		          "annc_date_datepicker, \r\n"\
		          "tgt_ticker, \r\n"\
		          "acq_ticker, \r\n" \
				  "target_last_price, \r\n" \
				  "acquirer_last_price, \r\n" \
				  "deal_value, \r\n" \
				  "payment_type_dropdown, \r\n"\
		          "cash_per_sh_textbox, \r\n"\
		          "stock_per_sh_textbox, \r\n" \
				  "spread_percentage, \r\n" \
			      "ann_ror, \r\n" \
				  "exp_comp_datepicker, \r\n"\
		          "is_excluded_dropdown, \r\n"\
		          "is_verified_dropdown \r\n"\
		        "]) \r\n"\
		    "} \r\n"\
		    "var rowNodes = editable_table.rows.add(rows).draw().nodes(); \r\n"\
		    "for (var j = 0; j < verified_rows_idxs.length; j++ ) { $('td', rowNodes[verified_rows_idxs[j]]).css('background-color', '#e6ffe6' ) } \r\n"\
		    "for (var j = 0; j < excluded_rows_idxs.length; j++ ) { $('td', rowNodes[excluded_rows_idxs[j]]).css('background-color', '#ffcccc' ) } \r\n"\
		"} \r\n"\
		"populateMNAVerifyTableLOOP() \r\n"

	return js


def mna_index_historical_report():
	mna_html = historical_mna_index_component_html(chart_width='50%',chart_height='30%')
	html = dashboard_line([(mna_html,'98%')],height='95%')
	js = historical_mna_index_component_js(scrollY='50%', ajax_refresh_rate='30*1000') # every 25 seconds

	return html + "<script>"+js+"</script>"

def live_mna_index_page():
	#spec = spec_index_component_html(chart_width='80%',chart_height='30%')
	#cash_mna = cash_mna_index_component_html(chart_width='80%',chart_height='30%')
	mna = mna_index_component_html(chart_width='50%',chart_height='30%')


	html = ''
	#html += dashboard_line([(spec,'48%'),(cash_mna,'48%')],height='85%')
	html += dashboard_line([(mna,'98%')],height='85%')
	#html += "<br>"
	#html += dashboard_line([(spec,'48%')],height='55%')


	js = ''
	#js += spec_index_component_js(scrollY='60%', ajax_refresh_rate='30*1000') # every 25 seconds
	#js += cash_mna_index_component_js(scrollY='60%', ajax_refresh_rate='30*1000') # every 25 seconds
	js += mna_index_component_js(scrollY='60%', ajax_refresh_rate='300*1000') # every 300 seconds

	return html + "<script>"+js+"</script>"

def live_spec_mna_index_page():
	#spec = spec_index_component_html(chart_width='80%',chart_height='30%')
	#cash_mna = cash_mna_index_component_html(chart_width='80%',chart_height='30%')
	mna = spec_index_component_html(chart_width='50%',chart_height='30%')


	html = ''
	#html += dashboard_line([(spec,'48%'),(cash_mna,'48%')],height='85%')
	html += dashboard_line([(mna,'98%')],height='85%')
	#html += "<br>"
	#html += dashboard_line([(spec,'48%')],height='55%')


	js = ''
	#js += spec_index_component_js(scrollY='60%', ajax_refresh_rate='30*1000') # every 25 seconds
	#js += cash_mna_index_component_js(scrollY='60%', ajax_refresh_rate='30*1000') # every 25 seconds
	js += spec_index_component_js(scrollY='60%', ajax_refresh_rate='30*1000') # every 25 seconds

	return html + "<script>"+js+"</script>"

def viper_dashboard():
	high_lvl_stats = high_level_stats_component_html()
	orders = orders_component_html()
	fills = fills_component_html()
	viper_subscription = viper_subscription_component_html()
	positions = positions_component_html()
	universe = universe_component_html()
	chart = pnl_chart_html()

	mkt_snapshot = get_mkt_snapshot_component_html()
	webpage_logs = webpage_logs_component_html()
	viper_logs = viper_logs_component_html()
	sched_tasks = scheduled_tasks_component_html()
	full_spectrum_attr = full_spectrum_attribution_component_html()
	misc = misc_component_html()
	pos_stocks_metrics = positions_stocks_metrics_component_html()

	html = ''
	html += dashboard_line([(high_lvl_stats,'100%')],height='10%')
	html += "<br>"
	html += dashboard_line([(chart,'28%'),(viper_logs,'28.5%'),(pos_stocks_metrics,'42%')],height='27%')
	html += "<br>"
	html += dashboard_line([(positions,'100%')],height='50%')
	html += "<br>"
	html += dashboard_line([(orders,'100%')],height='30%')
	html += "<br>"
	html += dashboard_line([(universe,'100%')],height='40%')
	html += "<br>"
	html += dashboard_line([(webpage_logs,'12%'),(viper_subscription,'13%'),(fills,'25%'),(mkt_snapshot,'25%'),(misc,'10%'),(sched_tasks,'13%')],height='30%')
	html += "<br>"
	html += dashboard_line([(full_spectrum_attr,'17%')],height='30%')

	js = ''
	js += webpage_logs_component_js(scrollY='70%') # MUST BE FIRST
	js += pnl_chart_js(height='40%',ajax_refresh_rate='60*1000') # every 60 seconds
	js += positions_component_js(scrollY='75%',ajax_refresh_rate='20*1000') # every 20 seconds
	js += orders_component_js(scrollY='63%',ajax_refresh_rate='10*1000') # every 10 seconds
	js += fills_component_js(scrollY='50%',ajax_refresh_rate='30*1000') # every 30 seconds
	js += universe_component_js(scrollY='70%',ajax_refresh_rate='3600*1000') # every hour
	js += viper_subscription_component_js(scrollY='50%',ajax_refresh_rate='10*1000') # every 10 seconds
	js += high_level_stats_component_js(scrollY='100%',ajax_refresh_rate='10*1000') # every 10 seconds
	js += get_mkt_snapshot_component_js(title='SPX/VIX',ajax_refresh_rate='60*1000',height='50%') # every 60 seconds
	js += viper_logs_component_js(scrollY='70%',ajax_refresh_rate='10*1000') # every 10 seconds
	js += scheduled_tasks_component_js(scrollY='70%',ajax_refresh_rate='5*1000') # every 5 seconds
	js += full_spectrum_attribution_component_js(scrollY='70%',ajax_refresh_rate='20*1000') # every 20 seconds
	js += misc_component_js(scrollY='70%',ajax_refresh_rate='20*1000') # every 20 seconds
	js += positions_stocks_metrics_component_js(scrollY='75%',ajax_refresh_rate='20*1000') # every 20 seconds

	#js += scheduled_tasks_component_js() #AJAX refresh in js code


	return html + "<script>"+js+"</script>"

def foo():
	import datetime
	import viper_dbutils
	import pandas as pd

	now = datetime.datetime.now()
	MA_INDEX_ID = 3
	now_utc = (now - datetime.datetime(1970, 1,1)).total_seconds() * 1000
	index_memb_df = viper_dbutils.IndexDB.get_curr_index_constituents(MA_INDEX_ID)
	# ['IndexID','Date','ActionID','Cash/sh','Stock/sh','PaymentType','TgtTicker','AcqTicker',
	#   'TgtBBGID','AcqBBGID','TgtISIN','AcqISIN','ExpectedCompletion',TgtEODDownsidePX,
	#   'TGT_PX_LAST','ACQ_PX_LAST','TGT_PX_YEST_CLOSE','ACQ_PX_YEST_CLOSE',
	#   'DEAL_VALUE_LAST','SPREAD_LAST','TgtRet(%)','AcqRet(%)','TgtMktCap']

	i_yield_srs = viper_dbutils.ViperDB.get_yield_curve_srs()

	verified_df = viper_dbutils.ViperDB.get_mna_verified_universe()
	verified_df = verified_df[verified_df['IsExcluded']=='false'].rename(
		columns={'Cash/sh':'V_Cash/sh','Stock/sh':'V_Stock/sh', 'ExpectedCompletion':'V_ExpectedCompletion'}
	)[['ActionID','V_Cash/sh','V_Stock/sh','V_ExpectedCompletion']].copy()
	df = pd.merge(index_memb_df, verified_df, how='inner', on='ActionID') # Adds  'V_Cash/sh','V_Stock/sh','V_ExpectedCompletion'

	df['Duration'] = df['V_ExpectedCompletion'].apply(lambda x: None if pd.isnull(x) else (x-now).days)
	df['RiskFreeForDuration'] = df['Duration'].apply(lambda x: i_yield_srs[x] if x in i_yield_srs else None)
	df['Downside(%)'] = 100.0*((df['TgtEODDownsidePX']/df['TGT_PX_LAST'])-1.0)
	#apply verified terms into spread calc
	df['DEAL_VALUE_LAST'] = df['V_Cash/sh'].fillna(0) + (df['V_Stock/sh'].fillna(0)*df['ACQ_PX_LAST'].fillna(0))
	df['SPREAD_LAST'] = 100.0*((df['DEAL_VALUE_LAST']/df['TGT_PX_LAST'])-1.0)
	#calc prob close
	def calc_prob_close(downside_px, curr_px, upside_px, duration_days):
		if pd.isnull(curr_px) or pd.isnull(downside_px) or pd.isnull(upside_px) or pd.isnull(duration_days): return None
		if duration_days not in i_yield_srs: return None
		if upside_px <= downside_px: return None

		yield_pct = i_yield_srs[duration_days]
		p = 100.0*(((1.0+yield_pct/100.0)*curr_px - downside_px)/(upside_px-downside_px))
		p = max(0,min(p,100))
		return p
	df['ProbClose(%)'] = [calc_prob_close(d,p,u,dur) for (d,p,u,dur) in zip(df['TgtEODDownsidePX'],df['TGT_PX_LAST'],df['DEAL_VALUE_LAST'], df['Duration'])]
	df['SPREAD_LAST_ANN'] = [None if (pd.isnull(sprd) or pd.isnull(dtc)) else (None if dtc <=0 else 100.0*(((1.0+sprd/100.0)**(365.0/dtc))-1.0)) for (sprd,dtc) in zip(df['SPREAD_LAST'],df['Duration'])]
	df['TgtMktCap(bln)'] = df['TgtMktCap'].apply(lambda x: None if pd.isnull(x) else round(x/1e9,2))
	tot_mktcap = df['TgtMktCap(bln)'].sum()
	df['MktCapWeight'] = df['TgtMktCap(bln)']/tot_mktcap

	#cosmetics
	df['Date'] = df['Date'].apply(lambda x: x.strftime('%m/%d/%Y'))
	df['V_ExpectedCompletion'] = df['V_ExpectedCompletion'].apply(lambda x: '' if pd.isnull(x) else x.strftime('%m/%d/%Y'))

	ma_equal_weight_ret = df['TgtRet(%)'].mean()
	ews = df['SPREAD_LAST'].mean()
	mvws = (df['SPREAD_LAST']*df['MktCapWeight']).sum()
	eq_d = int(df['Duration'].mean())
	#print(df.head())
	mv_d = int((df['Duration'].fillna(0)*df['MktCapWeight'].fillna(0)).sum())
	mvws_ann_by_mvd = 100.0*(((1.0+mvws/100.0)**(365.0/mv_d))-1.0) if mv_d != 0 else 0.0
	ews_ann_by_ewd = 100.0*(((1.0+ews/100.0)**(365.0/eq_d))-1.0) if eq_d != 0 else 0.0

	cols2include = ['Date','ActionID','TgtTicker','AcqTicker',
	                'TGT_PX_LAST','ACQ_PX_LAST','TGT_PX_YEST_CLOSE','ACQ_PX_YEST_CLOSE',
	                'V_Cash/sh','V_Stock/sh','V_ExpectedCompletion','Duration',
	                'TgtEODDownsidePX','DEAL_VALUE_LAST',
	                'Downside(%)','SPREAD_LAST','RiskFreeForDuration',
	                'SPREAD_LAST_ANN','ProbClose(%)',
	                'TgtRet(%)','TgtMktCap(bln)']

	df = df.fillna('')

#foo()